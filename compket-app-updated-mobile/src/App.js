import './App.css';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { Defenitions, UsedLanguage } from './base/Defenitions';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  BrowserRouter,
  Navigate,
  HashRouter
} from "react-router-dom";
import { useState, useEffect } from "react";
import React from "react";
import { DefaultLandPage } from './pages/DefaultLandPage';
import CssBaseline from "@mui/material/CssBaseline";
import { ToastContainer, toast } from 'react-toastify';
import { injectStyle } from "react-toastify/dist/inject-style";
import BodyDirectionHandler from './components/BodyDirectionHandler';
import { UserLandingPage } from './pages/UserLandingPage';
import { MarketLandingPage } from './pages/MarketLandingPage';
import { Helper } from './base/Helper';

if (typeof window !== "undefined") {
  injectStyle();
}

export default function App() {
  //console.log("here");
  UsedLanguage.setLang(Helper.getSelectedLanguageFromStorage());
  const [size, setSize] = useState({
    x: window.innerWidth,
    y: window.innerHeight
  });
  Defenitions.setWidthHeight(size.x, size.y);
  //UsedLanguage.setLangHE();
  const updateSize = () =>
    setSize({
      x: window.innerWidth,
      y: window.innerHeight
    });
  useEffect(() => (window.onresize = updateSize), []);
  const theme = Defenitions.getTheme()
  console.log(theme.direction)
  
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <HashRouter>
        <div key="mainAppDir" className="App" style={{overflow:'hidden'}}>
          <Routes>
            <Route key="defaultAppReDirect" path="/" element={<Navigate to={Defenitions.routes.default.link
            }></Navigate>}>
            </Route>
            <Route key="defaultAppRoute" path={Defenitions.routes.default.link + "/*"}
              element={<DefaultLandPage key="defaultLandPage" />} />
            <Route key="userAppRoute" path={Defenitions.routes.user.link + "/*"}
              element={<UserLandingPage key="userLandPage" />} />
            <Route key="marketAppRoute" path={Defenitions.routes.market.link + "/*"}
              element={<MarketLandingPage key="marketLandPage" />} />
          </Routes>
        </div>
        <ToastContainer bodyStyle={{direction:theme.direction}} position="bottom-center" />
      </HashRouter>
    </ThemeProvider>
  );
}
