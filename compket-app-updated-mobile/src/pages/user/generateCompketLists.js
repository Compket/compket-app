import { useState, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import { Helper } from '../../base/Helper';
import { Defenitions, UsedLanguage } from '../../base/Defenitions';
import ShoppingCart from '../../base/ShoppingCart';
import { List, ListItem } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { Paper } from '@mui/material';
import LocationsAutoComplete from '../../components/LocationsAutoComplete';
import { UserRoutes, MarketRoutes } from '../../base/Communication';
import { Typography } from '@mui/material';
import GeneratedItemsList from "../../components/GeneratedItemsList";

const generateCompketLists = () => {
    const [country, setCountry] = useState(null);
    const [city, setCity] = useState(null);
    const [showLoading, setShowLoading] = useState(false);
    const [data, setData] = useState(null);
    const [sortedData, setSortedData] = useState(null);
    const [categoryLst, setCategoryLst] = useState([]);
    let cartIsEmpty = ShoppingCart.isCartEmpty();
    if (cartIsEmpty) {
        Helper.createToast(UsedLanguage.languageRef.youMustAddItemsToCartFirstTxt);
    }
    let width = Defenitions.width * 0.7;
    let widthLst = Defenitions.width * 0.9;


    useEffect(async () => {
        //console.log(country, city);
        if(cartIsEmpty)
            return;
        let cateRes = await MarketRoutes.GetCategories(Helper.getAccessTokenFromStorage());
        if (categoryLst.length === 0 && cateRes.Success) {
            setCategoryLst(cateRes.Data);
        }
        if (country !== null && country !== "" && city !== null && city !== "") {
            setSortedData(null);
            setData(null);
            //console.log("Fetching data");
            let lst = ShoppingCart.getListOfItemsAsBarcodes();
            //console.log(lst);
            setShowLoading(true);
            await UserRoutes.GenerateComparedLists(Helper.getAccessTokenFromStorage(),
                country, city, lst).then((res) => {
                    setData(res.Data)
                    setShowLoading(false);
                }, (err) => {
                    console.log(err);
                    setShowLoading(false);
                });
        }
    }, [country, city]);

    useEffect(() => {
        if(cartIsEmpty)
            return;
        handleDataChange();
    }, [data]);

    const handleDataChange = () => {
        let resultsLists = [];
        if (data !== null) {
            //console.log("data", data);
            for (const [index, info] of Object.entries(data)) {
                //console.log(index, info);
                let market = info.market;
                let resItems = info.founditems;
                //console.log(market, resItems);

                let currListStruct = {};
                currListStruct.market = market;
                currListStruct.items = resItems;
                currListStruct.totalPrice = 0;
                currListStruct.allItemsFound = true;
                currListStruct.foundItems = [];
                currListStruct.missingItems = [];


                for (const [key, value] of Object.entries(currListStruct.items)) {
                    let requestedItem = value.requesteditem;
                    let foundItem = value.founditem;
                    //console.log(requestedItem, foundItem);

                    if (foundItem !== null) {
                        let currSum = foundItem.price * requestedItem.amount;
                        currListStruct.totalPrice += currSum;
                        currListStruct.foundItems.push({ item: foundItem, amount: requestedItem.amount });
                    }
                    else {
                        currListStruct.allItemsFound = false;
                        currListStruct.missingItems.push(
                            ShoppingCart.getCartItemDataByBarcode(requestedItem.barcode));
                    }
                }
                //console.log(currListStruct);
                resultsLists.push(currListStruct);
            }
            resultsLists.sort((a, b) => { // sorting to have lowest price first
                let resA = b.totalPrice - a.totalPrice;
                let resB = a.totalPrice - b.totalPrice;
                //console.log("h",a,b, resA, resB);
                if(a.totalPrice === 0) return resB;
                if(b.totalPrice === 0) return resA;
                return resB;
            });
            //console.log(resultsLists);
            setSortedData(resultsLists);
        }
    };
//
    const dataToViews = () => {
        if (sortedData !== null) {
            console.log(sortedData)
            return (
                <List>
                    <ListItem style={{ alignContent: 'center', justifyContent: 'center' }}>
                        <Typography variant="h6" textAlign={'center'} style={{ color: Defenitions.palette.primary.main }}>
                            {UsedLanguage.languageRef.amountofItems + " " +
                                ShoppingCart.getCartSize()}
                        </Typography>
                    </ListItem>
                    {
                        sortedData.map((currData, index) => {
                            //console.log(currData);
                            let lstItem = (
                                <ListItem key={'lstItem' + index}
                                    style={{ justifyContent: "center", maxWidth: Defenitions.width }}>
                                    <GeneratedItemsList listData={currData}
                                        width={widthLst}
                                        categoryLst={categoryLst}
                                    />
                                </ListItem>);
                            return lstItem;
                        })
                    }
                </List>);
        }
        return (<Typography variant="h5" textAlign={'center'} style={{ color: Defenitions.palette.primary.main }}>
            {UsedLanguage.languageRef.noItemsFoundInThisSearchTxt}
        </Typography>);
    };

    return cartIsEmpty ?
        (<Navigate to={Defenitions.routes.user.link +
            Defenitions.routes.userAddItemsToShoppingCartRelative.link} />)
        :
        (
            <Paper elevation={0} style={{ height: '90vh', overflow: 'auto', overflowX: 'hidden' }}>
                <List style={{ justifyContent: 'center', alignContent: 'center' }}>
                    <ListItem key={"locationSelection"} style={{ justifyContent: "center" }}>
                        <LocationsAutoComplete width={width}
                            onChange={(data) => {
                                if (data !== null) {
                                    setCountry(data[0]);
                                    setCity(data[1]);
                                }
                            }} />
                    </ListItem>
                    <ListItem key="results" style={{ justifyContent: "center" }}>
                        {showLoading ?
                            (<Box style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}
                                sx={{ display: 'flex', height: Defenitions.height / 2 }}>
                                <CircularProgress />
                            </Box>)
                            :
                            dataToViews()
                        }
                    </ListItem>
                </List>
            </Paper>
        );
};

export default generateCompketLists;
