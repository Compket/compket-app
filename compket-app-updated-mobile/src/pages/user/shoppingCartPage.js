// component for generic items serach and add to cart.
import { useState, useEffect } from 'react';
import SearchBar from '../../components/SearchBar';
import CategoryAutoComplete from '../../components/CategoryAutoComplete';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { Defenitions } from '../../base/Defenitions';
import { Paper } from '@mui/material';
import { UsedLanguage } from '../../base/Defenitions';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { UserRoutes, MarketRoutes } from '../../base/Communication';
import { Helper } from '../../base/Helper';
import Item from '../../models/Item';
import { IconButton } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { Typography } from '@mui/material';
import ItemExpandableComponent from '../../components/ItemExpandableComponent'
import ShoppingCart from '../../base/ShoppingCart';
import PropTypes from 'prop-types'
import RemoveIcon from '@mui/icons-material/Remove';
import { Button } from '@mui/material';
import YesNoDialog from '../../components/YesNoDialog';


const shoppingCartPage = (props) => {
  const [nameQuery, setNameQuery] = useState("");
  const [barcodeQuery, setBarcodeQuery] = useState("");
  const [categoryQuery, setCategoryQuery] = useState("");
  const [data, setData] = useState([]);
  const [showLoading, setShowLoading] = useState(false);
  const [showDialog, setShowDialog] = useState(false);
  const [categoryLst, setCategoryLst] = useState([]);
  let width = Defenitions.width * 0.78;
  let borderSpacingPx = 10;

  useEffect(
    async () => {
      let cateRes = await MarketRoutes.GetCategories(Helper.getAccessTokenFromStorage());
      if (categoryLst.length === 0 && cateRes.Success) {
        setCategoryLst(cateRes.Data);
      }
      if (!props.viewCartMode) {
        if (categoryQuery !== null && categoryQuery !== "" &&
          categoryQuery > Defenitions.undefinedId) {
          //console.log("fetching: ", nameQuery, barcodeQuery, categoryQuery);
          setShowLoading(true);
          await UserRoutes.GetGenericItems(Helper.getAccessTokenFromStorage(), nameQuery,
            barcodeQuery, categoryQuery).then((res) => {
              //console.log(res.Data);
              if (res.Success && res.Data.length > 0) {
                let tempLst = [];
                res.Data.forEach((item) => {
                  tempLst.push(new Item(item));
                });
                setData(tempLst);
              }
              else
                setData([]);
              setShowLoading(false);
            }, (err) => {
              console.log(err);
              setShowLoading(false);
            });
        }
        else if (categoryQuery === "") {
          setData([]);
          if (nameQuery !== null || nameQuery !== "" || barcodeQuery !== null
            || barcodeQuery !== "") {
            Helper.createToast(UsedLanguage.languageRef.chooseCategoryFirstTxt,
              "categoryNotify");
          }

        }
      }
      else {
        fetchDefaultCart();
      }

    }, [nameQuery, barcodeQuery, categoryQuery]);


  const fetchDefaultCart = () => {
    if (categoryQuery === "" && (nameQuery === null || nameQuery === "")
      && (barcodeQuery === null
        || barcodeQuery === "")) {
      setShowLoading(true);
      let data = ShoppingCart.getCartItemsAsArray();
      //console.log(data);
      setData(data);
      setShowLoading(false);
    }
    else {
      setShowLoading(true);
      let data = ShoppingCart.getCartItemsFiltered(categoryQuery, nameQuery,
        barcodeQuery);
      //console.log(data);
      setData(data);
      setShowLoading(false);
    }
  }
  const dataToViews = () => {
    return (data !== null && data.length > 0 ?
      <List>
        <ListItem style={{ alignContent: 'center', justifyContent: 'center' }}>
        {props.viewCartMode ?
          <Typography variant="h6" textAlign={'center'} style={{ color: Defenitions.palette.primary.main }}>
            {UsedLanguage.languageRef.amountofItems + " " +
              ShoppingCart.getCartSize()}
          </Typography> : <></>}
        </ListItem>
        <ListItem style={{ alignContent: 'center', justifyContent: 'center' }}>
          {props.viewCartMode ?
            <Button color="primary" key="clearCartBtn" variant="contained" onClick={() => {
              setShowDialog(true);
            }}>{UsedLanguage.languageRef.emptyCartTxt}</Button> : <></>}
        </ListItem>
        {//
          data.map((item, index) => {

            let categoryName = Helper.getCategoryNameFromId(item.CategoryId, categoryLst);
            let itemElement = (<ListItem key={'item' + item.ID}
              style={{ justifyContent: "center", maxWidth: Defenitions.width }}>
              <div style={{ display: 'table', borderSpacing: borderSpacingPx, verticalAlign: 'middle' }}>
                {props.viewCartMode ?
                  <div style={{ display: 'table-cell', verticalAlign: 'middle' }}>
                    <Typography variant="h6" textAlign={'center'} style={{ color: Defenitions.palette.primary.main }}>
                      {"X" + ShoppingCart.countItemInShoppingCart(item.BarcodeNum)}
                    </Typography>
                  </div>
                  :
                  <></>
                }

                <div style={{ display: 'table-cell', verticalAlign: 'middle' }}>
                  <ItemExpandableComponent itemId={item.ID} price={item.Price} name={item.Name}
                    onDelete={() => { }}
                    width={width}
                    onEdit={() => { }}
                    isGeneric={true}
                    isMarketOwner={false}
                    categoryName={categoryName}
                    barcodeNumber={item.BarcodeNum}
                    description={item.Description} categoryId={item.CategoryId} currency={item.Currency}
                    marketId={item.MarketId} />
                </div>
                <div style={{ display: 'table-cell', verticalAlign: 'middle' }}>
                  <IconButton aria-label="add"
                    style={{ justifyContent: 'center', alignContent: 'center', padding: 0 }}
                    onClick={() => {
                      if (!props.viewCartMode) {
                        //console.log(item);
                        ShoppingCart.addItemToShoppingCart(item);
                        //console.log(ShoppingCart.getShoppingCart());
                        Helper.createToast(UsedLanguage.languageRef.itemAddedSuccessfully,
                          null, 'success');
                      }
                      else {// view mode need to delete on click
                        //console.log(item);
                        ShoppingCart.removeItemFromShoppingCart(item.BarcodeNum);
                        //console.log(ShoppingCart.getShoppingCart());
                        Helper.createToast(UsedLanguage.languageRef.itemRemovedSuccessfulyTxt,
                          "remove_item_toast", 'success');
                        fetchDefaultCart();
                      }
                    }}>
                    {
                      props.viewCartMode
                        ?
                        <RemoveIcon color='primary' />
                        :
                        <AddIcon color='primary' />
                    }
                  </IconButton>
                </div>
              </div>
            </ListItem>
            )
            return itemElement;
          })
        }
      </List >
      :
      <Box style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}
        sx={{ display: 'flex', height: Defenitions.height / 2 }}>
        <Typography variant="h5" textAlign={'center'} style={{ color: Defenitions.palette.primary.main }}>
          {UsedLanguage.languageRef.noItemsFoundInThisSearchTxt}
        </Typography>
      </Box>);
  }
  return (
    <Paper elevation={0} style={{ height: '90vh', overflow: 'auto', overflowX: 'hidden' }}>
      <List style={{ justifyContent: 'center', alignContent: 'center' }}>
        <ListItem key={"categorySelection"} style={{ justifyContent: "center" }}>
          <CategoryAutoComplete
            clearable={true}
            onChange={(data) => { setCategoryQuery(data !== null ? data.id : "") }}
            isAdmin={false}
            width={width}
          />
        </ListItem>
        <ListItem key={"clearCartPopup"} style={{ justifyContent: "center" }}>
          <YesNoDialog key={"clearCartDialog"} open={showDialog}
            onResult={(res) => {
              if (res) {
                ShoppingCart.emptyCart();
                fetchDefaultCart();
              }
              setShowDialog(false);
            }}
            title={UsedLanguage.languageRef.emptyCartTxt + "?"}
            body={null}
          />
        </ListItem>
        <ListItem key={"searchDiv"} style={{ justifyContent: "center" }}>
          <div style={{ display: 'table', borderSpacing: borderSpacingPx }}>
            <div style={{ display: 'table-cell' }}>
              <SearchBar
                value={nameQuery}
                onChange={(val) => {
                  setNameQuery(val);
                  setBarcodeQuery("");
                }}
                label={UsedLanguage.languageRef.openSearchTxt}
                placeholder={UsedLanguage.languageRef.itemNameTxt}
                width={width / 2 - borderSpacingPx / 2}
              />
            </div>
            <div style={{ display: 'table-cell' }}>
              <SearchBar
                value={barcodeQuery}
                type={'number'}
                onChange={(val) => {
                  setBarcodeQuery(val);
                  setNameQuery("");
                }}
                label=" "
                placeholder={UsedLanguage.languageRef.barcodeNumTxt}
                width={width / 2 - borderSpacingPx / 2}
              />
            </div>
          </div>
        </ListItem>
        <ListItem key="fetchContent" style={{ justifyContent: "center", maxWidth: Defenitions.width }}>
          {showLoading ?
            (<Box style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}
              sx={{ display: 'flex', height: Defenitions.height / 2 }}>
              <CircularProgress />
            </Box>)
            :
            dataToViews()
          }
        </ListItem>
      </List>
    </Paper >
  );
};

shoppingCartPage.propTypes = {
  viewCartMode: PropTypes.bool,

}
shoppingCartPage.defaultProps = {
  viewCartMode: false
};

export default shoppingCartPage;

