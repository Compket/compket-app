import { React, useState } from 'react'
import { TextField, Button } from '@mui/material'
import { Defenitions, UsedLanguage } from '../../base/Defenitions';
import Grid from '@mui/material/Grid';
import CssBaseline from "@mui/material/CssBaseline";
import { Helper } from '../../base/Helper';
import { toast } from 'react-toastify';
import { DefaultRoutes } from '../../base/Communication';
import { Navigate } from "react-router-dom";

const UserHomePage = () => {
  return (
    <div style={{
      display: "flex",
      justifyContent: "center",
      alignContent: "center"
    }}>
      <CssBaseline />
      <Grid component='main'
        container
        overflow="hidden"
        columns={1}
        spacing={0}
        mt={1}
        justifyContent="space-evenly"
        alignItems="top"
      >
        <Grid item xs={4} container
          mt={1}
          direction="column"
          alignItems="center"
          justifyContent="center">
          <p>Compket is a platform which will provide comparison between stores according to provided shopping list, user’s location, user’s preferences</p>
        </Grid>
       

      </Grid>
    </div>
  )
}

export default UserHomePage
