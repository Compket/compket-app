import { Defenitions} from '../../base/Defenitions';
import { Helper } from '../../base/Helper';
import { Navigate } from "react-router-dom";
import ShoppingCart from "../../base/ShoppingCart";

const Logout = () => {
    Helper.saveAccessTokenToStorage(null);
    Helper.saveSelectedMarketToStorage(null);
    Helper.saveSAccountTypeToStorage(null);
    Helper.saveUserIdToStorage(null);
    ShoppingCart.emptyCart();
    return (
        <Navigate to={Defenitions.routes.default.link} />
    )
}

export default Logout