import { useEffect, useState } from "react";
import { AdminRoutes } from "../../base/Communication";
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { Helper } from "../../base/Helper";
import { Defenitions } from "../../base/Defenitions";
import UserCollapsed from "../../components/UserCollapsed";
import { Typography } from "@mui/material";
import { UsedLanguage } from "../../base/Defenitions";
import { List, ListItem } from "@mui/material";
import { Navigate } from "react-router-dom";
import { Paper } from "@mui/material";

const AdminPanelPage = () => {
    const [showLoading, setShowLoading] = useState(false);
    const [fetched, setFetched] = useState(false);
    const [dataLst, setDataLst] = useState([]);

    let width = Defenitions.width * 0.9;

    useEffect(async () => {
        if (fetched)
            return;
        reload()
    }, []);

    const reload = async () => {
        //console.log("getting");
        setShowLoading(true);
        await AdminRoutes.GetUsers(Helper.getAccessTokenFromStorage()).then((res) => {
            //console.log(res);
            setDataLst(res.Data);
            setShowLoading(false);
            setFetched(true);
        }, (err) => {
            console.log(err);
            setShowLoading(false);
            setFetched(true);
        });
    };

    const dataToViews = () => {
        return (dataLst.length > 0 ?
            <List>
                {dataLst.map((item, index) => (
                    <ListItem key={"item" + item.id} style={{ justifyContent: "center" }}>
                        <UserCollapsed
                            width={width}
                            key={"itemCollapsed" + item.id}
                            userEmail={item.email}
                            userFirstName={item.firstname}
                            userLastName={item.lastname}
                            userPhoneNumber={item.phonenum}
                            userAccountType={item.accounttype}
                            userId={item.id}
                            onAction={()=>{reload()}}
                            adminView={true} />
                    </ListItem>
                ))}
            </List>
            :
            <Typography variant="h5" style={{ color: Defenitions.palette.primary.main }}>
                {UsedLanguage.languageRef.noResultsAtmTxt}
            </Typography>);
    }
    let isAdmin = Helper.isSystemAdmin();
    if (!isAdmin)
        Helper.createToast(UsedLanguage.languageRef.mustBeAnAdminToAccessTxt, null, "error");
    return (!isAdmin ? (<Navigate to={Defenitions.routes.user.link} />)
        : (<div>
            {showLoading ?
                <Box style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}
                    sx={{ display: 'flex', height: Defenitions.height / 2 }}>
                    <CircularProgress />
                </Box> :
                <Paper elevation={0} style={{ height: '90vh', overflow: 'auto' }}>
                    {dataToViews()}
                </Paper>}
        </div>));
};

export default AdminPanelPage;
