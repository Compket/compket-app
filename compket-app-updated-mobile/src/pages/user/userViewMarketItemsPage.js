import { useState, useEffect } from 'react';
import { Helper } from '../../base/Helper';
import { UserRoutes, MarketRoutes } from '../../base/Communication';
import Item from '../../models/Item';
import { Navigate } from 'react-router-dom'
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import CategoryAutoComplete from '../../components/CategoryAutoComplete'
import ItemExpandableComponent from '../../components/ItemExpandableComponent'
import { Typography } from '@mui/material';
import { Paper } from '@mui/material';
import { TextField } from '@mui/material';
import { Defenitions, UsedLanguage } from '../../base/Defenitions';
import SearchBar from '../../components/SearchBar';

const userViewMarketItemsPage = () => {
  const [dataLst, setDataLst] = useState([]);
  const [filterdLst, setFilterdLst] = useState([]);
  const [fetchedData, setFetchedData] = useState(false);
  const [categoryLst, setCategoryLst] = useState([]);

  const isMarketIdValid = (mid) => {
    return mid !== null && mid !== "" && mid !== undefined;
  }

  let query = Helper.useQuery();
  let marketId = query.get("mid");
  //console.log(marketId);

  useEffect(async () => {
    //console.log(await UserRoutes.GetMarketItems(Helper.getAccessTokenFromStorage(), marketId));
    if (fetchedData)
      return;
    setFetchedData(true);
    await reloadData();
  }, []);

  const reloadData = async () => {
    setDataLst([]);
    setFilterdLst([]);
    if (!isMarketIdValid(marketId)) {
      return;
    }
    let cateRes = await MarketRoutes.GetCategories(Helper.getAccessTokenFromStorage());
    if (cateRes.Success) {
      setCategoryLst(cateRes.Data);
    }
    await Helper.createToastPromise(UserRoutes.GetMarketItems(Helper.getAccessTokenFromStorage(), marketId))
      .then((result) => {
        //console.log(result);
        if (result.Success && result.Data.length > 0) {
          let tempLst = [];
          result.Data.forEach((item) => {
            tempLst.push(new Item(item));
          });
          setDataLst(tempLst);
          setFilterdLst(tempLst);
        }
      }
      ).catch((err) => {
        try {
          console.log(err);
        }
        catch { }
        setDataLst([]);
        setFilterdLst([]);
      });
  }
  const filterLst = (category) => {
    if (category === null) {
      setFilterdLst(dataLst);
    }
    else {
      setFilterdLst(
        dataLst.filter((item, index) => {
          return (item.CategoryId === category.id);
        }
        ));
    }

  };
  const dataToViews = () => {
    return (filterdLst.length > 0 ?
      <List>
        {filterdLst.map((item, index) => {

          let categoryName = Helper.getCategoryNameFromId(item.CategoryId, categoryLst);
          let itemElement = (<ListItem key={'item' + item.ID} style={{ justifyContent: "center" }}>
            <ItemExpandableComponent itemId={item.ID} price={item.Price} name={item.Name}
              onDelete={() => { }}
              onEdit={() => { }}
              isMarketOwner={false}
              categoryName={categoryName}
              barcodeNumber={item.BarcodeNum}
              description={item.Description} categoryId={item.CategoryId} currency={item.Currency}
              marketId={item.MarketId} />
          </ListItem>
          )
          return itemElement;
        })}
      </List>
      :
      <Typography variant="h5" textAlign={'center'} style={{ color: Defenitions.palette.primary.main }}>
        {UsedLanguage.languageRef.noItemsFoundInThisSearchTxt}
      </Typography>);
  }
  const filterLstFreeSearch = (keyword) => {
    if (keyword === null || keyword === undefined || keyword === '')
      setFilterdLst(dataLst);
    else {
      setFilterdLst(
        dataLst.filter((item, index) => {
          return (item.Name.includes(keyword) || item.BarcodeNum.includes(keyword));
        }
        ));
    }
  }


  return (
    <div>
      {isMarketIdValid(marketId) ?
        (<Paper elevation={0} style={{ height: '90vh', overflow: 'auto' }}>
          <List style={{ justifyContent: 'center', alignContent: 'center' }}>
            <ListItem key={'autoCompleteCate'} style={{ justifyContent: "center" }}>
              <CategoryAutoComplete onChange={(data) => { filterLst(data) }} isAdmin={false} clearable={true} width={Defenitions.width * 0.5} />
            </ListItem>
            <ListItem key={'freeSearch'} style={{ justifyContent: "center" }}>
              <SearchBar width={Defenitions.width * 0.5} onChange={async (value) => {
                filterLstFreeSearch(value)
              }}
                label={UsedLanguage.languageRef.openSearchTxt}
                placeholder={UsedLanguage.languageRef.barcodeOrNameTxt}
              />
            </ListItem>
            <ListItem key={'acutalLstItems'} style={{ justifyContent: "center" }}>
              {dataToViews()}
            </ListItem>
          </List>
        </Paper>)
        :
        <Navigate to={Defenitions.routes.user.link} />}
    </div>);
};

export default userViewMarketItemsPage;
