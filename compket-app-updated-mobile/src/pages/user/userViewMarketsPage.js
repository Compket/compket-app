import { useEffect, useState } from "react"
import { UserRoutes } from "../../base/Communication";
import { Helper } from "../../base/Helper";
import MarketCollapsedView from '../../components/MarketCollapsedView'
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import LocationsAutoComplete from "../../components/LocationsAutoComplete";
import Market from '../../models/Market';
import { Typography } from '@mui/material';
import { UsedLanguage, Defenitions } from '../../base/Defenitions';
import { Paper } from "@mui/material";
import SearchBar from "../../components/SearchBar";
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const userViewMarketsPage = () => {
    const [dataLst, setDataLst] = useState([]);
    const [fetchedData, setFetchedData] = useState(false);
    const [name, setName] = useState(null);
    const [phone, setPhone] = useState(null);
    const [country, setCountry] = useState(null);
    const [city, setCity] = useState(null);
    const [showLoading, setShowLoading] = useState(false);
    const refreshData = async (name = null, phone = null, country = null, city = null) => {
        setShowLoading(true);
        setDataLst([]);
        if(country === "")
        {
            country = null;
        }
        if(city === "")
        {
            city = null;
        }
        await UserRoutes.ViewMarkets(Helper.getAccessTokenFromStorage(), name, phone,
            country, city).then(
                (result) => {
                    //console.log(result);

                    if (result.Success && result.Data.length > 0) {
                        let tempLst = [];
                        result.Data.forEach((item) => {
                            tempLst.push(new Market(item));
                        });
                        setDataLst(tempLst);
                        //console.log(tempLst);
                    }
                    setShowLoading(false);
                },
                (err) => {
                    console.log(err);
                    setShowLoading(false);
                }
            );
    };




    const dataToViews = () => {
        return (dataLst.length > 0 ?
            <List>
                {dataLst.map((item, index) => (
                    <ListItem key={"item" + item.ID} style={{ justifyContent: "center" }}>
                        <MarketCollapsedView
                            description={item.Description}
                            onDeleteOrEdit={refreshData}
                            marketOwnerMode={false}
                            linkTo={Defenitions.routes.user.link + Defenitions.routes.userViewMarketsItemsRelative.link +
                                "?mid=" + item.ID}
                            mid={item.ID} key={item.ID} addressCity={item.AddressCity} addressCountry={item.AddressCountry}
                            addressStreet={item.AddressStreet} name={item.Name} phoneNumber={item.PhoneNum} />
                    </ListItem>
                ))}
            </List>
            :
            (showLoading ?
                <Box sx={{ display: 'flex' }}>
                    <CircularProgress />
                </Box> :
                <Typography variant="h5" style={{ color: Defenitions.palette.primary.main }}>
                    {UsedLanguage.languageRef.noStoresAtThisMomentTxt}
                </Typography>)
        );
    }
    useEffect(async () => {
        await Helper.createToastPromise(refreshData(name, phone, country, city));
        //console.log(await (await UserRoutes.GetAllLocations(Helper.getAccessTokenFromStorage())).Data);
    }, []);
    /*useEffect(async () => {
        console.log("refresh")
        //await refreshData(name, phone, country, city);
    }, [name, phone, country, city]);*/
    let width = Defenitions.width * 0.7;
    return (

        <Paper elevation={0} style={{ height: '90vh', overflow: 'auto' }}>
            <List style={{ justifyContent: 'center', alignContent: 'center' }}>
                <ListItem key={'serachbar'} style={{ justifyContent: "center" }}>
                    <SearchBar width={width} onChange={async (value) => {
                        //console.log(value);
                        await refreshData(value, value, null, null);
                    }}
                        placeholder={UsedLanguage.languageRef.marketNameOrPhoneTxt}
                    />
                </ListItem>
                <ListItem key={'autoCompleteloc'} style={{ justifyContent: "center" }}>
                    <LocationsAutoComplete key={"userViewMarketsLocationAutoComplete"} width={width}
                        onChange={async (data) => {
                            //console.log('here', data);
                            if (data !== null) {
                                setCountry(data[0]);
                                setCity(data[1]);
                                await refreshData(null, null, data[0], data[1]);
                            }
                        }} />
                </ListItem>
                <ListItem key={'autoCompleteViews'} style={{ justifyContent: "center" }}>
                    {dataToViews()}
                </ListItem>
            </List>
        </Paper>
    )
}

export default userViewMarketsPage
