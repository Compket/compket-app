import React, { useState } from "react"
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import PersonIcon from '@mui/icons-material/Person';
import Register from './default/Register';
import Login from './default/Login';
import BodyDirectionHandler from '../components/BodyDirectionHandler';
import CompketAppDrawer from '../components/CompketAppDrawer'
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  BrowserRouter,
  Navigate
} from "react-router-dom";
import { Defenitions, UsedLanguage } from '../base/Defenitions';
import { Button } from "@mui/material";

const RegisterPageMemo = React.memo(Register);
const LoginPageMemo = React.memo(Login);


export const DefaultLandPage = () => {
  const theme = Defenitions.getTheme()
  const styleOptions = ({
    backgroundColor: theme.palette.secondary.main, alignItems: 'center',
    height: '100vh'
  });
  const list = (<div>
    <Divider />
    <List>
      {[UsedLanguage.languageRef.LoginTxt].map((text, index) => (
        <Link to={Defenitions.routes.default.link
          + Defenitions.routes.loginRelative.link} style={{ textDecoration: 'none', color: "inherit" }} key={"defaultLandPageMenu" + index}>
          <ListItem>
            <ListItemIcon>
              <PersonIcon />
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        </Link>
      ))}
    </List>
    <Divider />
  </div >)

  return (
    <div
      key="defaultLandPageDiv"
      style={styleOptions}>
      <CompketAppDrawer key="defaultLandPageDrawer" buttonEnabled={true} buttonText={
        UsedLanguage.languageRef.RegisterTxt}
        buttonLink={Defenitions.routes.default.link
          + Defenitions.routes.registerRelative.link}
        drawerItems={<BodyDirectionHandler key="defaultPageDrawerItems" fromKey="defaultPageDrawerItems" bodyContent={list} />} />
      <Routes key="defaultLandPageRoutes" >
        <Route path="/"
          key="defaultLandPageRouteDefault"
          element={<Navigate key="defaultLandPageRouteNavigateDefault" to={Defenitions.routes.default.link
            + Defenitions.routes.loginRelative.link} />} />
        <Route
          key="defaultLandPageRouteLogin"
          path={Defenitions.routes.loginRelative.link}
          element={<BodyDirectionHandler
            fromKey="defaultLandPageRouteLoginBodyDirectionHandler"
            key="defaultLandPageRouteLoginBodyDirectionHandler" bodyContent={<LoginPageMemo key="memoComLogin" />} />} />
        <Route path={Defenitions.routes.registerRelative.link}
          key="defaultLandPageRouteRegister"
          element={<BodyDirectionHandler key="defaultLandPageRouteRegisterBodyDirectionHandler"
            fromKey="defaultLandPageRouteRegisterBodyDirectionHandler"
            bodyContent={<RegisterPageMemo key="memoCompRegister" />} />} />
      </Routes>
    </div>
  )
}
