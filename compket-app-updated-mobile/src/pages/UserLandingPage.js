import React, { useEffect, useState } from "react"
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import StoreIcon from '@mui/icons-material/Store';
import HomeIcon from '@mui/icons-material/Home';
import UserHomePage from "./user/userHomePage";
import UserViewMarketsPage from "./user/userViewMarketsPage";
import BodyDirectionHandler from '../components/BodyDirectionHandler';
import CompketAppDrawer from '../components/CompketAppDrawer'
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import {
  Routes,
  Route,
  Link,
  Navigate
} from "react-router-dom";
import { Defenitions, UsedLanguage } from '../base/Defenitions';
import Logout from "./user/Logout";
import { Helper } from "../base/Helper";
import { Button } from "@mui/material";
import StorefrontIcon from '@mui/icons-material/Storefront';
import userViewMarketItemsPage from "./user/userViewMarketItemsPage";
import shoppingCartPage from "./user/shoppingCartPage";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';
import generateCompketLists from "./user/generateCompketLists";
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import AdminPanelPage from "./user/AdminPanelPage";

const HomePageMemo = React.memo(UserHomePage);
const ViewMarketsPageMemo = React.memo(UserViewMarketsPage);
const ViewMarketItemsPageMemo = React.memo(userViewMarketItemsPage);
const AddItemsToShppingCartMemo = React.memo(shoppingCartPage);
const ViewShoppingCartMemo = React.memo(shoppingCartPage);
const GenerateCompketListsMemo = React.memo(generateCompketLists);
const AdminPanelPageMemo = React.memo(AdminPanelPage);
const LogoutpageMemo = React.memo(Logout);


export const UserLandingPage = () => {
  const theme = Defenitions.getTheme()
  const styleOptions = ({
    backgroundColor: theme.palette.secondary.main, alignItems: 'center',
    height: '100vh'
  });
  const list = (<div>
    <Divider />
    <List>
      {[[UsedLanguage.languageRef.HomeTxt, Defenitions.routes.user.link + Defenitions.routes.homeRelative.link, <HomeIcon />],
      [UsedLanguage.languageRef.viewMarketsTxt, Defenitions.routes.user.link + Defenitions.routes.viewMarketsRelative.link, <StorefrontIcon />],
      [UsedLanguage.languageRef.viewCartTxt, Defenitions.routes.user.link + Defenitions.routes.userViewShoppingCartRelative.link, <ShoppingCartIcon />],
      [UsedLanguage.languageRef.addItemToShoppingCartTxt, Defenitions.routes.user.link + Defenitions.routes.userAddItemsToShoppingCartRelative.link, <AddShoppingCartIcon />],
      [UsedLanguage.languageRef.generateCompketListTxt, Defenitions.routes.user.link + Defenitions.routes.userGenerateCopmaredListRelative.link, <ShoppingCartCheckoutIcon />],
      [UsedLanguage.languageRef.MarketModeTxt, Defenitions.routes.market.link + Defenitions.routes.marketSelectionRelative.link,
      <StoreIcon />],
      [UsedLanguage.languageRef.adminPanelTxt, Defenitions.routes.user.link + Defenitions.routes.adminPanelRelative.link,
      <AdminPanelSettingsIcon />]].map((item, index) => (
        <Button key={"userLandPageMenu" + index} color='inherit' style={{
          display: 'flex', justifyContent: 'start', width: '100%',
          textTransform:'none'
        }}>
          <Link to={item[1]} style={{ textDecoration: 'none', color: "inherit" }}>
            <ListItem>
              <ListItemIcon>
                {item[2]}
              </ListItemIcon>
              <ListItemText primary={item[0]} />
            </ListItem>
          </Link>
        </Button>
      ))}
    </List>
    <Divider />
  </div >)
  const token = Helper.getAccessTokenFromStorage();
  return (
    token !== null ?//checking if loggeed in
      <div
        key="userLandPageDiv"
        style={styleOptions}>
        <CompketAppDrawer key="userLandPageDrawer" buttonEnabled={true} buttonText={
          UsedLanguage.languageRef.LogOutTxt}
          buttonLink={Defenitions.routes.user.link
            + Defenitions.routes.logoutRelative.link}
          drawerItems={<BodyDirectionHandler key="userPageDrawerItems" fromKey="userPageDrawerItems" bodyContent={list} />} />
        <Routes key="userLandPageRoutes" >
          <Route path="/"
            key="userLandPageRouteDefault"
            element={<Navigate key="userLandPageRouteNavigateDefault" to={Defenitions.routes.user.link
              + Defenitions.routes.homeRelative.link} />} />
          <Route
            key="userLandPageRouteHome"
            path={Defenitions.routes.homeRelative.link}
            element={<BodyDirectionHandler
              fromKey="userLandPageRouteHomeBodyDirectionHandler"
              key="userLandPageRouteHomeBodyDirectionHandler" bodyContent={<HomePageMemo key="memoComWelcome" />} />} />
          <Route
            key="userLandPageRouteViewMarkets"
            path={Defenitions.routes.viewMarketsRelative.link}
            element={<BodyDirectionHandler
              fromKey="userLandPageRouteViewMarketsBodyDirectionHandler"
              key="userLandPageRouteViewMarketsBodyDirectionHandler" bodyContent={<ViewMarketsPageMemo key="memoComViewMarkets" />} />} />
          <Route
            key="userLandPageRouteViewMarketsItems"
            path={Defenitions.routes.userViewMarketsItemsRelative.link}
            element={<BodyDirectionHandler
              fromKey="userLandPageRouteViewMarketsItemsBodyDirectionHandler"
              key="userLandPageRouteViewMarketsItemsBodyDirectionHandler" bodyContent={<ViewMarketItemsPageMemo key="memoComViewMarketsItems" />} />} />
          <Route
            key="userLandPageRouteAddIemsToShoppingCart"
            path={Defenitions.routes.userAddItemsToShoppingCartRelative.link}
            element={<BodyDirectionHandler
              fromKey="userLandPageRouteAddIemsToShoppingCartBodyDirectionHandler"
              key="userLandPageRouteAddIemsToShoppingCartBodyDirectionHandler" bodyContent={<AddItemsToShppingCartMemo key="memoComAddIemsToShoppingCart" />} />} />
          <Route
            key="userLandPageRouteViewShoppingCart"
            path={Defenitions.routes.userViewShoppingCartRelative.link}
            element={<BodyDirectionHandler
              fromKey="userLandPageRouteViewShoppingCartBodyDirectionHandler"
              key="userLandPageRouteAddIemsToShoppingCartBodyDirectionHandler" bodyContent={<ViewShoppingCartMemo key="memoUserLandPageRouteViewShoppingCart" viewCartMode={true} />} />} />
          <Route
            key="userLandPageRouteViewGenerateLists"
            path={Defenitions.routes.userGenerateCopmaredListRelative.link}
            element={<BodyDirectionHandler
              fromKey="userLandPageRouteViewGenerateLists"
              key="userLandPageRouteViewGenerateListsMemo" bodyContent={<GenerateCompketListsMemo key="memoComComparePage"/>} />} />
          <Route
            key="adminPanelPageRoute"
            path={Defenitions.routes.adminPanelRelative.link}
            element={<BodyDirectionHandler
              fromKey="adminPanelPageRoute"
              key="adminPanelPageMemoBodyDirHandler" bodyContent={<AdminPanelPageMemo key="adminPanelPageMemoComp"/>} />} />
          <Route
            key="userLandPageRouteLogout"
            path={Defenitions.routes.logoutRelative.link}
            element={<BodyDirectionHandler
              fromKey="userLandPageRouteLogoutBodyDirectionHandler"
              key="userLandPageRouteLogoutBodyDirectionHandler" bodyContent={<LogoutpageMemo key="memoComLogout" />} />} />
        </Routes>
      </div>
      :
      <Navigate to={Defenitions.routes.default.link} />
  )
}
