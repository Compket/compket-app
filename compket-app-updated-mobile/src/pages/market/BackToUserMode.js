import React from 'react'
import { Navigate } from 'react-router-dom'
import { Defenitions } from '../../base/Defenitions'
import { Helper } from '../../base/Helper'

export const BackToUserMode = () => {
    Helper.saveSelectedMarketToStorage(null);
    return (
        <Navigate to={Defenitions.routes.user.link}></Navigate>
    )
}
