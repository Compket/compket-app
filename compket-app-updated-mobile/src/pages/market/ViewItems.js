import React, { useState, useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import { Helper } from '../../base/Helper'
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Item from '../../models/Item';
import { Defenitions, UsedLanguage } from '../../base/Defenitions'
import { MarketRoutes } from '../../base/Communication'
import CategoryAutoComplete from '../../components/CategoryAutoComplete'
import ItemExpandableComponent from '../../components/ItemExpandableComponent'
import { Typography } from '@mui/material';
import { Paper } from '@mui/material';
import { TextField } from '@mui/material';

const ViewItems = () => {
    const [dataLst, setDataLst] = useState([]);
    const [filterdLst, setFilterdLst] = useState([]);
    const [fetchedData, setFetchedData] = useState(false);
    const[categoryLst, setCategoryLst] = useState([]);
    useEffect(async () => {
        if (fetchedData)
            return;
        setFetchedData(true);
        await reloadData();
        
    }, []);
    const reloadData = async ()=>
    {
        setDataLst([]);
        setFilterdLst([]);
        let cateRes = await MarketRoutes.GetCategories(Helper.getAccessTokenFromStorage());
        if(cateRes.Success)
        {
            setCategoryLst(cateRes.Data);
        }
        await Helper.createToastPromise(MarketRoutes.GetMarketItems(Helper.getAccessTokenFromStorage(), Helper.getSelectedMarketFromStorage()))
        .then((result) => {
            if (result.Success && result.Data.length > 0) {
                let tempLst = [];
                result.Data.forEach((item) => {
                    tempLst.push(new Item(item));
                });
                setDataLst(tempLst);
                setFilterdLst(tempLst);
            }
        }
        ).catch((err) => { 
            console.log(err);
            setDataLst([]);
            setFilterdLst([]);
        });
    }
    const filterLst = (category) =>{
        if(category === null)
        {
            setFilterdLst(dataLst);
        }
        else
        {
            setFilterdLst(
                dataLst.filter((item, index) => {
                    return (item.CategoryId === category.id);
                }
            ));
        }

    };

    const filterLstFreeSearch = (keyword) =>{
        if(keyword === null || keyword === undefined || keyword === '')
            setFilterdLst(dataLst);
        else
        {
            setFilterdLst(
                dataLst.filter((item, index) => {
                    return (item.Name.includes(keyword) || item.BarcodeNum.includes(keyword));
                }
            ));
        }
    }
    const dataToViews = () => {
        return (filterdLst.length > 0 ?
            <List>
                {filterdLst.map((item, index) => {
                    
                    let categoryName = Helper.getCategoryNameFromId(item.CategoryId, categoryLst);
                    let itemElement = (<ListItem key={'item' + item.ID} style={{ justifyContent: "center" }}>
                        <ItemExpandableComponent itemId={item.ID} price={item.Price} name={item.Name}
                            onDelete={reloadData}
                            onEdit={reloadData}
                            categoryName={categoryName}
                            barcodeNumber={item.BarcodeNum}
                            description={item.Description} categoryId={item.CategoryId} currency={item.Currency}
                            marketId={item.MarketId} />
                    </ListItem>
                )
                return itemElement;
                })}
            </List>
            :
            <Typography variant="h5" textAlign={'center'} style={{ color: Defenitions.palette.primary.main }}>
                {UsedLanguage.languageRef.noItemsFoundInThisSearchTxt}
            </Typography>);
    }
    let selectedMarket = Helper.getSelectedMarketFromStorage();
    return (
        selectedMarket !== null ?
            <Paper elevation={0} style={{ height: '90vh', overflow: 'auto' }}>
                <List style={{ justifyContent: 'center', alignContent: 'center' }}>
                    <ListItem key={'autoCompleteCate'} style={{ justifyContent: "center" }}>
                        <CategoryAutoComplete onChange={(data)=>{filterLst(data)}} isAdmin={false} clearable={true} width={Defenitions.width * 0.5} />
                    </ListItem>
                    <ListItem key={'freeSearch'} style={{ justifyContent: "center" }}>
                        <TextField id="outlined-search" focused variant='standard' 
                            placeholder={UsedLanguage.languageRef.barcodeOrNameTxt}
                            label={UsedLanguage.languageRef.openSearchTxt} 
                            style={{width:Defenitions.width * 0.5}}
                            onChange={(e)=>{filterLstFreeSearch(e.target.value)}}
                            type="search" />
                    </ListItem>
                    <ListItem key={'acutalLstItems'} style={{ justifyContent: "center" }}>
                        {dataToViews()}
                    </ListItem>
                </List>
            </Paper>
            :
            <Navigate to={Defenitions.routes.market.link} />
    )
}

export default ViewItems
