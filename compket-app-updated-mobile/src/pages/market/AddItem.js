import React, { useState, useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import { Helper } from '../../base/Helper'
import { Defenitions, UsedLanguage } from '../../base/Defenitions'
import CategoryAutoComplete from '../../components/CategoryAutoComplete'
import { Grid } from '@mui/material'
import { TextField, Button } from '@mui/material'
import CurrencySelection from '../../components/CurrencySelection'
import BarcodeScannerComponent from "react-qr-barcode-scanner";
import { toast } from 'react-toastify';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import FileSelection from '../../components/FileSelection';
import { MarketRoutes } from '../../base/Communication'
import PropTypes from 'prop-types';

const AddItem = (props) => {
    const [data, setData] = useState(props.editItemMode ? props.itemModel.BarcodeNum : '');
    const [name, setName] = useState(props.editItemMode ? props.itemModel.Name : '');
    const [description, setDescription] = useState(props.editItemMode ? props.itemModel.Description : '');
    const [price, setPrice] = useState(props.editItemMode ? props.itemModel.Price : 0);
    const [priceFixed, setPriceFixed] = useState(props.editItemMode ? props.itemModel.Price.toFixed(2) : 0);
    const [result, setresult] = useState('');
    const [torch, setTorch] = useState(false);
    const [currency, setCurrency] = useState(props.editItemMode ? props.itemModel.Currency : {});
    const [category, setCategory] = useState('');
    const [scannerDenied, setScannerDenied] = useState(false);
    const [navigate, setNavigate] = useState(false);
    const [selectedFile, setSelectedFile] = useState("");
    let selectedMarket = Helper.getSelectedMarketFromStorage();
    const handleScan = (data) => {
        this.setState({
            result: data,
        })
    }
    const handleError = (err) => {
        console.error(err)
    }
    const handleBtnClick = async (e) => {
        if (name === "" || priceFixed <= 0 || currency === null || category === '' || data.toString() === '')
            toast.error(UsedLanguage.languageRef.FillOutTheFormTxt);
        else {
            if (!props.editItemMode) {
                Helper.createToastPromise(MarketRoutes.CreateItem(Helper.getAccessTokenFromStorage(), Helper.getSelectedMarketFromStorage(), name,
                    description, data.toString(), priceFixed, currency, category)).then((response) => {
                        if (response.Success && Helper.isFileTypeAnImage(selectedFile)) {
                            // uploadPhoto
                            const formData = new FormData();
                            formData.append('File', selectedFile);
                            Helper.createToastPromise(MarketRoutes.SetItemImage(Helper.getAccessTokenFromStorage(), response.Data,
                                formData)).then(async (resp) => {
                                    setNavigate(response.Success);
                                },
                                    (e) => {
                                        console.log(e);
                                        setNavigate(response.Success);
                                    })
                        }
                        else
                            setNavigate(response.Success);
                    },
                        (e) => {
                            console.log(e);
                            //handled by the toast
                        });
            }
            else {
                //edit mode
                Helper.createToastPromise(MarketRoutes.EditItem(Helper.getAccessTokenFromStorage(), Helper.getSelectedMarketFromStorage(), props.itemModel.ID, name,
                    description, data.toString(), priceFixed, currency, category)).then((response) => {
                        if (response.Success && Helper.isFileTypeAnImage(selectedFile)) {
                            // uploadPhoto
                            const formData = new FormData();
                            formData.append('File', selectedFile);
                            Helper.createToastPromise(MarketRoutes.SetItemImage(Helper.getAccessTokenFromStorage(), props.itemModel.ID,
                                formData)).then(async (resp) => {
                                    props.editDoneFunc();        
                                },
                                    (e) => {
                                        console.log(e);
                                        props.editDoneFunc();
                                    })
                        }
                        else
                            props.editDoneFunc();
                    },
                        (e) => {
                            console.log(e);
                            //handled by the toast
                        });
            }
        }
    }
    window.onresize = function () {
        //console.log("resizing");
        //this is required for the mobile resize support of keyboard
    };
    useEffect(() => {
        if (window !== undefined && window.cordova !== undefined && window.cordova.plugins !== undefined) {
            console.log('here!');
            window.cordova.plugins.permissions.hasPermission(window.cordova.plugins.permissions.CAMERA, function (status) {
                if (status.hasPermission) {
                    console.log("Yes :D ");
                }
                else {
                    console.warn("No :( ");
                    const err = () => { Helper.createToast(UsedLanguage.languageRef.youCantUseThisFeatureTxt); console.warn('Camera permission is not turned on'); };
                    cordova.plugins.permissions.requestPermission(cordova.plugins.permissions.CAMERA, (status) => { !status.hasPermission ? err() : toast(UsedLanguage.languageRef.restartTheAppTxt); }, err);
                }
            });
            //cordova.plugins.permissions.requestPermission(cordova.plugins.permissions.CAMERA, success, error);
        }
    }, []);
    const compWidth = Defenitions.width * 0.5;
    const compHeight = Defenitions.height * 0.4;
    const marginDiff = 0
    return (
        navigate ? (<Navigate to={Defenitions.routes.market.link +
            Defenitions.routes.viewItemsRelative.link} />) :
            (selectedMarket !== null ?
                <Grid component='main'
                    key="addItemGrid"
                    container
                    overflow="auto"
                    maxHeight={0.85 * Defenitions.height}
                    columns={1}
                    spacing={0}
                    justifyContent="space-evenly"

                    alignItems="top"
                >
                    <Grid item xs={4} container
                        mt={1}
                        key="addItemGridName"
                        direction="column"
                        alignItems="center"
                        justifyContent="center">
                        <TextField
                            key="itemName"
                            required
                            sx={{ width: compWidth }}
                            onChange={(e) => { setName(e.target.value); }}
                            value={name}
                            label={UsedLanguage.languageRef.itemNameTxt} variant="standard" color="primary" focused />
                    </Grid>
                    <Grid item xs={4} container
                        mt={1}
                        key="addItemGridCategory"
                        direction="column"
                        alignItems="center"
                        justifyContent="center">
                        <CategoryAutoComplete isAdmin={false} width={compWidth} key='addItemCategoryComp' onChange={(data) => { setCategory(data !== null ? data.id : null); }} />
                    </Grid>
                    <Grid item xs={4} container
                        mt={1}
                        direction="column"
                        alignItems="center"
                        key="addItemGridDesc"
                        justifyContent="center">
                        <TextField
                            key="description"
                            label={UsedLanguage.languageRef.itemDescriptionTxt}
                            multiline
                            onChange={(e) => { setDescription(e.target.value); }}
                            variant="standard"
                            value={description}
                            sx={{ width: compWidth }}
                            focused
                            inputProps={{ maxLength: Defenitions.maxTextFieldLength }}
                            rows={3}
                        />
                    </Grid>
                    <Grid
                        key="addItemGridPrice"
                        container
                        mt={1}
                        overflow="hidden"
                        spacing={0}
                        maxWidth={compWidth}
                        justifyContent="center"
                        alignContent="center"
                        direction={"row"}
                        alignItems="center"
                    >
                        <Grid item
                            key="price"
                            width={((compWidth - marginDiff) * (2 / 3))}
                            alignItems="center"
                            alignContent="center"
                            justifyContent="center">
                            <TextField
                                key="price"
                                required
                                label={UsedLanguage.languageRef.itemPriceTxt}
                                variant="standard"
                                value={price}
                                sx={{ width: ((compWidth - marginDiff) * (2 / 3)) }}
                                inputProps={{
                                    maxLength: 13,
                                    step: 0.5
                                }}
                                onChange={(e) => {
                                    setPrice(Math.abs(parseFloat(e.target.value)));
                                    setPriceFixed(Math.abs(parseFloat(e.target.value).toFixed(2)));
                                }}
                                type="number"
                                focused
                            />
                        </Grid>
                        <Grid item
                            key="currency"
                            width={((compWidth - marginDiff) / 3)}
                            alignItems="center"
                            alignContent="center"
                            justifyContent="center">
                            <CurrencySelection defaultVal={Defenitions.currency[currency]} width={((compWidth - marginDiff) / 3)} onChange={(data) => { setCurrency(data.index); }} />
                        </Grid>
                    </Grid>
                    <Grid item xs={4} container
                        mt={1}
                        key="addItemGridBarcode"
                        direction="column"
                        alignItems="center"
                        justifyContent="center">
                        <TextField
                            key="itemBarcode"
                            required
                            type='number'
                            inputProps={{ maxLength: Defenitions.maxTextFieldLength }}
                            dir='ltr'
                            sx={{ width: compWidth }}
                            onChange={(e) => { setData(Math.abs(e.target.value)); }}
                            value={data}
                            label={UsedLanguage.languageRef.barcodeNumTxt} variant="standard" color="primary" focused />
                    </Grid>
                    <Grid item xs={4} container
                        mt={1}
                        direction="column"
                        alignItems="center"
                        key="scanGrid"
                        justifyContent="center">
                        {!scannerDenied ?
                            (<div style={{ display: 'contents' }}>
                                <BarcodeScannerComponent
                                    width={compWidth}
                                    height={compHeight}
                                    torch={torch}
                                    onError={(e) => { setScannerDenied(true); }}
                                    onUpdate={(err, result) => {
                                        if (result) {
                                            console.log(result);
                                            setData(result.text);
                                        }
                                    }}
                                />
                                <br />
                                <Button style={{marginTop:1}} color="primary" key="createMarketBtn" variant="contained" onClick={() => { setTorch(!torch) }}>{UsedLanguage.languageRef.torchTxt}</Button>
                            </div>)
                            :
                            (<div style={{ display: 'contents' }}>
                                <QrCodeScannerIcon fontSize='large' />
                                <br />
                                <p>{UsedLanguage.languageRef.scannerUnavailableTxt}</p>
                            </div>)
                        }
                    </Grid>
                    <Grid item xs={4} container
                        mt={1}
                        key="addItemPicture"
                        direction="column"
                        alignItems="center"
                        justifyContent="center">
                        <FileSelection existingSrc={props.itemImgSrc} handlerParentFunc={(selectedFileChanged) => {
                            setSelectedFile(selectedFileChanged);
                        }}
                            componentMentForImage={true}
                        />
                    </Grid>
                    <Grid item xs={4} container
                        mt={1}
                        direction="column"
                        key="createItemBtnGrid"
                        alignItems="center"
                        justifyContent="center">
                        <Button color="primary" key="createitemBtn" variant="contained" onClick={handleBtnClick}>{UsedLanguage.languageRef.SubmitTxt}</Button>
                    </Grid>
                </Grid>
                :
                <Navigate to={Defenitions.routes.market.link} />)
    )
}

export default AddItem

AddItem.propTypes = {
    editItemMode: PropTypes.bool.isRequired,//specify if we are creating new item or editing
    itemModel: PropTypes.object,
    itemImgSrc: PropTypes.string,
    editDoneFunc: PropTypes.func,
}
AddItem.defaultProps = {
    itemModel: null,
    itemImgSrc: null
};