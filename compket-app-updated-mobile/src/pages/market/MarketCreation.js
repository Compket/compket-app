import { TextField, Button } from '@mui/material'
import { Defenitions, UsedLanguage } from '../../base/Defenitions';
import Grid from '@mui/material/Grid';
import React, { useState, useEffect, useRef } from 'react';
import 'react-phone-number-input/style.css'
import PhoneNumberInput from '../../components/PhoneNumberInput';
import { Helper } from '../../base/Helper';
import { MarketRoutes } from '../../base/Communication';
import { Navigate } from "react-router-dom";
import { toast } from 'react-toastify';
import MyGoogleMap from '../../components/MyGoogleMap';
import { Typography } from '@mui/material';
import FileSelection from '../../components/FileSelection';
import PropTypes from 'prop-types';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

const MarketCreation = (props) => {
    const [name, setMarketName] = useState(props.marketEditMode ? props.name : "");
    const [addressCountry, setMarketAddressCountry] = useState(props.marketEditMode ? props.addressCountry : "");
    const [addressCity, setMarketAddressCity] = useState(props.marketEditMode ? props.addressCity : "");
    const [addressStreet, setMarketAddressStreet] = useState(props.marketEditMode ? props.addressStreet : "");
    const [description, setMarketDescription] = useState(props.marketEditMode ? props.description : "");
    const [phoneNumValue, setPhoneNumValue] = useState(props.marketEditMode && 
        props.phoneNumber !== null ? props.phoneNumber : "");
    const [phoneNumFilled, setPhoneNumFilled] = useState(props.marketEditMode && props.phoneNumber !== null);
    const [selectedFile, setSelectedFile] = useState("");
    const [navigateToMarketGateway, setNavigate] = useState(false);
    const [setNewLocation, setSetNewLocation] = useState(false);

    //const [navigateToLogin, setNavigateToLogin] = useState(false);

    const locationChanged = (lat, lng, country, city, street) => {
        setMarketAddressCountry(country);
        setMarketAddressCity(city);
        setMarketAddressStreet(street);
    };

    const getCurrentLocationTxt = () => {
        return addressCountry === "" ? UsedLanguage.languageRef.LocationNotSetTxt : UsedLanguage.languageRef.TheSelectedLocationTxt
            + addressCountry + " " + addressCity + " " + addressStreet;
    }

    const phoneNumChanged = (e) => {
        setPhoneNumFilled(e.numValue !== "");
        setPhoneNumValue(e.value);
    }

    const handleNameChange = async (e) => {
        setMarketName(e.target.value);
    };
    const handleDescriptionChange = async (e) => {
        setMarketDescription(e.target.value);
    };
    const handleBtnClick = async (e) => {

        if (name === "" || !phoneNumFilled || addressCountry === "")
            toast.error(UsedLanguage.languageRef.FillOutTheFormTxt);
        else {
            if (!props.marketEditMode) {
                Helper.createToastPromise(MarketRoutes.CreateMarket(Helper.getAccessTokenFromStorage(), name,
                    addressCountry, addressCity, addressStreet, phoneNumValue, description)).then((response) => {
                        //console.log(response);
                        //console.log(response.Data);
                        if (response.Success && Helper.isFileTypeAnImage(selectedFile)) {
                            // uploadPhoto
                            const formData = new FormData();
                            formData.append('File', selectedFile);
                            Helper.createToastPromise(MarketRoutes.SetMarketImage(Helper.getAccessTokenFromStorage(), response.Data,
                                formData)).then((resp) => {
                                    //console.log(resp);
                                    setNavigate(response.Success);
                                },
                                    (e) => {
                                        console.log(e);
                                        setNavigate(response.Success);
                                    })
                        }
                        else
                            setNavigate(response.Success);
                        //setNavigateToLogin(response.Success);
                    },
                        (e) => {
                            console.log(e);
                            //handled by the toast
                        });
            }
            else {
                //edit mode
                Helper.createToastPromise(MarketRoutes.EditMarket(Helper.getAccessTokenFromStorage(), props.mid, name,
                    addressCountry, addressCity, addressStreet, phoneNumValue, description)).then((response) => {
                        //console.log(response);
                        //console.log(response.Data);
                        if (response.Success && Helper.isFileTypeAnImage(selectedFile)) {
                            // uploadPhoto
                            const formData = new FormData();
                            formData.append('File', selectedFile);
                            Helper.createToastPromise(MarketRoutes.SetMarketImage(Helper.getAccessTokenFromStorage(), props.mid,
                                formData)).then((resp) => {
                                    //console.log(resp);
                                    props.onDeleteOrEdit();
                                },
                                (e) => {
                                    console.log(e);
                                    props.onDeleteOrEdit();
                                })
                        }
                        else {
                            props.onDeleteOrEdit();
                        }
                    },
                        (e) => {
                            console.log(e);
                            //handled by the toast
                        });
            }
        }
    }

    window.onresize = function () {
        //console.log("resizing");
        //this is required for the mobile resize support of keyboard
    };
    const mapHeight = props.marketEditMode && !setNewLocation ? 'auto' : '100%';
    const gridMapHeight = props.marketEditMode && !setNewLocation ? 'auto' : '50vh';
    Helper.saveSelectedMarketToStorage(null);
    return (
        navigateToMarketGateway ? <Navigate to={Defenitions.routes.market.link
            + Defenitions.routes.marketSelectionRelative.link} /> :
            (<Grid component='main'
                key="createMarketGrid"
                container
                overflow="auto"
                maxHeight={0.85 * Defenitions.height}
                columns={1}
                spacing={0}
                justifyContent="space-evenly"

                alignItems="top"
            >
                <Grid item xs={4} container
                    mt={1}
                    key="createMarketGridFieldGrid"
                    direction="column"
                    alignItems="center"
                    justifyContent="center">
                    <TextField
                        key="createMarketNameField"
                        required
                        onChange={handleNameChange}
                        value={name}
                        label={UsedLanguage.languageRef.MarketNameTxt} variant="standard" color="primary" focused />
                </Grid>
                <Grid item xs={4} container
                    mt={1}
                    mr={1}
                    ml={1}
                    direction="column"
                    alignItems="center"
                    key="createMarketPhoneGrid"
                    justifyContent="center">
                    <PhoneNumberInput defaultPhoneNumber={props.phoneNumber} specialKey="createMarketPhoneField" key="createMarketPhoneField" directionLTR={Defenitions.ltrScreenDirection}
                        onChangeEvent={phoneNumChanged} />
                </Grid>
                <Grid item xs={4} container
                    mt={1}
                    direction="column"
                    alignItems="center"
                    key="createMarketDescriptionGrid"
                    justifyContent="center">
                    <TextField
                        key="description"
                        label={UsedLanguage.languageRef.MarketDescriptionTxt}
                        multiline
                        onChange={handleDescriptionChange}
                        variant="standard"
                        value={description}
                        focused
                        inputProps={{ maxLength: Defenitions.maxTextFieldLength }}
                        rows={3}
                    />
                </Grid>
                <Grid item xs={4} container
                    mt={1}
                    ml={1}
                    mr={1}
                    direction="column"
                    key="createMarketLocationDetailsGrid"
                    alignItems="center"
                    justifyContent="center">
                    <Typography variant="h6" component="h6" style={
                        {
                            color: Defenitions.palette.primary.main,
                            textAlign: 'center'
                        }
                    }>
                        {getCurrentLocationTxt()}
                    </Typography>
                </Grid>
                <Grid item /*xs={5}*/ container
                    mt={1}
                    mb={10}
                    height={gridMapHeight}
                    direction="column"
                    alignItems="center"
                    key="mapGrid"
                    justifyContent="center">
                    <div style={{ height: mapHeight, width: "80%", display: 'flow' }}>
                        {props.marketEditMode ?
                            <FormGroup>
                                <FormControlLabel style={{ justifyContent: 'center', alignContent: 'center', color: Defenitions.palette.primary.main }} control={<Checkbox
                                    style={{
                                        color: Defenitions.palette.primary.main,
                                    }}
                                    onChange={(e) => { setSetNewLocation(e.target.checked); }} />}
                                    label={UsedLanguage.languageRef.setNewLocationTxt} />
                            </FormGroup> : <></>}
                        {(props.marketEditMode && setNewLocation) || (!props.marketEditMode) ?
                            <MyGoogleMap onLocationSelected={locationChanged} />
                            : <></>}
                    </div>
                    {/**https://www.freakyjolly.com/google-maps-in-react-example-application/#Show_Address_using_Geocode_Service_API */}
                </Grid>
                <Grid item xs={4} container
                    mt={10}
                    direction="column"
                    key="selectPicGrid"
                    alignItems="center"
                    justifyContent="center">
                    <FileSelection existingSrc={props.imgSrc} handlerParentFunc={(selectedFileChanged) => {
                        setSelectedFile(selectedFileChanged);
                    }}
                        componentMentForImage={true}
                    />
                </Grid>
                <Grid item xs={4} container
                    mt={1}
                    mb={1}
                    direction="column"
                    key="createMarketBtnGrid"
                    alignItems="center"
                    justifyContent="center">
                    <Button color="primary" key="createMarketBtn" variant="contained" onClick={handleBtnClick}>{UsedLanguage.languageRef.SubmitTxt}</Button>
                </Grid>
            </Grid>))
}

export default MarketCreation;

MarketCreation.propTypes = {
    mid: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    addressCountry: PropTypes.string,
    addressCity: PropTypes.string,
    addressStreet: PropTypes.string,
    phoneNumber: PropTypes.string,
    marketEditMode: PropTypes.bool,
    onDeleteOrEdit: PropTypes.func,
    imgSrc: PropTypes.string,
}
MarketCreation.defaultProps = {
    mid: Defenitions.undefinedId,
    name: null,
    description: null,
    addressCountry: null,
    addressCity: null,
    addressStreet: null,
    phoneNumber: null,
    onDeleteOrEdit: (() => { }),
    marketEditMode: false,
    imgSrc: null,
};