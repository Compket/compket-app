import { useState, useEffect } from 'react'
import MarketCollapsedView from '../../components/MarketCollapsedView'
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { MarketRoutes } from '../../base/Communication';
import { Helper } from '../../base/Helper';
import Market from '../../models/Market';
import { Typography } from '@mui/material';
import { UsedLanguage, Defenitions } from '../../base/Defenitions';
import { Paper } from '@mui/material';

const MarketSelection = () => {
    const [dataLst, setDataLst] = useState([]);
    const [fetchedData, setFetchedData] = useState(false);
    /*let data = [
        {
            addressCity: 'קרית גת',
            addressCountry: 'ישראל',
            addressStreet: 'שד גת',
            name: 'שופרסל',
            phoneNumber: '+972546663375',
            id: 1,
        },
        {
            addressCity: 'קרית גת',
            addressCountry: 'ישראל',
            addressStreet: 'מרכז ביג',
            name: 'רמי לוי',
            phoneNumber: '546663375',
            id: 2,
        },

    ];*/
    const dataToViews = () => {
        return (dataLst.length > 0 ?
            <List>
                {dataLst.map((item, index) => (
                    <ListItem key={"item" + item.ID} style={{ justifyContent: "center" }}>
                        <MarketCollapsedView
                            onClick={() => {
                                Helper.saveSelectedMarketToStorage(item.ID);
                            }}
                            description={item.Description}
                            onDeleteOrEdit={refreshData}
                            marketOwnerMode={true}
                            linkTo={Defenitions.routes.market.link + Defenitions.routes.viewItemsRelative.link}
                            mid={item.ID} key={item.ID} addressCity={item.AddressCity} addressCountry={item.AddressCountry}
                            addressStreet={item.AddressStreet} name={item.Name} phoneNumber={item.PhoneNum} />
                    </ListItem>
                ))}
            </List>
            :
            <Typography variant="h5" style={{ color: Defenitions.palette.primary.main }}>
                {UsedLanguage.languageRef.noOwnedMarketsTxt}
            </Typography>);
    }

    const refreshData = async () =>{
        setDataLst([]);
        Helper.saveSelectedMarketToStorage(null);
        await Helper.createToastPromise(MarketRoutes.ViewOwnedMarkets(Helper.getAccessTokenFromStorage())).then(
            (result) => {
                if (result.Success && result.Data.length > 0) {
                    let tempLst = [];
                    result.Data.forEach((item) => {
                        tempLst.push(new Market(item));
                    });
                    setDataLst(tempLst);
                }
            }
        ).catch(() => { });
    };

    useEffect(async () => {
        if (fetchedData)
            return;
        setFetchedData(true);
        await refreshData();
    }, []);
    return (
        <Paper elevation={0} style={{ height: '85vh', overflow: 'auto' }}>
            {dataToViews()}
        </Paper>
    )
}

export default MarketSelection
