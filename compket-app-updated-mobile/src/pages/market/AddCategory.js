import { TextField, Button } from '@mui/material'
import { Defenitions, UsedLanguage } from '../../base/Defenitions';
import Grid from '@mui/material/Grid';
import React, { useState, useEffect, useRef } from 'react';
import 'react-phone-number-input/style.css'
import { Helper } from '../../base/Helper';
import { Communication, MarketRoutes } from '../../base/Communication';
import { Navigate } from "react-router-dom";
import { toast } from 'react-toastify';
import FileSelection from '../../components/FileSelection';
import Autocomplete from '@mui/material/Autocomplete';

const AddCategory = () => {
    const [options, setOptions] = useState([]);
    const [value, setValue] = useState('');
    const [inputValue, setInputValue] = useState('');

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [selectedFile, setSelectedFile] = useState("");
    const [navigateToItemsGateway, setNavigate] = useState(false);

    const handleDescriptionChange = async (e) => {
        setDescription(e.target.value);
    };

    const handleBtnClick = async (e) => {
        if (name === "")
            toast.error(UsedLanguage.languageRef.FillOutTheFormTxt);
        else {
            Helper.createToastPromise(MarketRoutes.CreateCategory(Helper.getAccessTokenFromStorage(), name,
                description)).then((response) => {
                    if (response.Success && Helper.isFileTypeAnImage(selectedFile)) {
                        // uploadPhoto
                        const formData = new FormData();
                        formData.append('File', selectedFile);
                        Helper.createToastPromise(MarketRoutes.SetCategoryImage(Helper.getAccessTokenFromStorage(), response.Data,
                            formData)).then((resp) => {
                                //console.log(resp);
                                setNavigate(response.Success);
                            },
                                (e) => {
                                    console.log(e);
                                    setNavigate(response.Success);
                                })
                    }
                    else
                        setNavigate(response.Success);
                    //setNavigateToLogin(response.Success);
                },
                    (e) => {
                        console.log(e);
                        //handled by the toast
                    });
        }
    }

    window.onresize = function () {
        //console.log("resizing");
        //this is required for the mobile resize support of keyboard
    };

    useEffect(async () => {
        MarketRoutes.GetCategories(Helper.getAccessTokenFromStorage()).then((res)=>{
            if (res.Success) {
                setOptions(Helper.categoriesToCategoriesNamesArray(res.Data));
            }
        }).catch((err)=>{
            console.log(err);
        });
    }, []);

    let isAdmin = Helper.isSystemAdmin();
    if (!isAdmin)
        toast.error(UsedLanguage.languageRef.mustBeAnAdminToAccessTxt);
    const width = Defenitions.width * 0.4;
    return (
        isAdmin ?
            (navigateToItemsGateway ? <Navigate to={Defenitions.routes.market.link
                + Defenitions.routes.viewItemsRelative.link} /> :
                (<Grid component='main'
                    key="createCategoryGrid"
                    container
                    overflow="auto"
                    maxHeight={0.85 * Defenitions.height}
                    columns={1}
                    spacing={0}
                    justifyContent="space-evenly"
                    alignItems="top"
                >
                    <Grid item xs={4} container
                        mt={1}
                        key="createCategoryGridFieldGrid"
                        direction="column"
                        alignItems="center"
                        justifyContent="center">
                        <Autocomplete
                        freeSolo
                        id="nameAuto"
                        options={options}
                        disableClearable
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                        sx={{ width: width }}
                        onInputChange={(event, newInputValue) => {
                            setInputValue(newInputValue);//the relevent value
                            setName(newInputValue);
                        }}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                label={UsedLanguage.languageRef.categoryName}
                                variant="standard"
                                focused
                                required
                                InputProps={{
                                    ...params.InputProps,
                                    type: 'search',
                                }}
                            />
                        )}
                    />
                    </Grid>
                    <Grid item xs={4} container
                        mt={1}
                        direction="column"
                        alignItems="center"
                        key="createCategoryDescriptionGrid"
                        justifyContent="center">
                        <TextField
                            key="description"
                            label={UsedLanguage.languageRef.categoryDescription}
                            multiline
                            onChange={handleDescriptionChange}
                            variant="standard"
                            value={description}
                            sx={{ width: width }}
                            focused
                            inputProps={{ maxLength: Defenitions.maxTextFieldLength }}
                            rows={3}
                        />
                    </Grid>
                    <Grid item xs={4} container
                        mt={1}
                        direction="column"
                        key="selectPicGrid"
                        alignItems="center"
                        justifyContent="center">
                        <FileSelection handlerParentFunc={(selectedFileChanged) => {
                            setSelectedFile(selectedFileChanged);
                        }}
                            componentMentForImage={true}
                        />
                    </Grid>
                    <Grid item xs={4} container
                        mt={1}
                        direction="column"
                        key="createCategoryBtnGrid"
                        alignItems="center"
                        justifyContent="center">
                        <Button color="primary" key="createMarketBtn" variant="contained" onClick={handleBtnClick}>{UsedLanguage.languageRef.SubmitTxt}</Button>
                    </Grid>
                </Grid>))
            :
            <Navigate to={Defenitions.routes.market.link} />
    );
}

export default AddCategory
