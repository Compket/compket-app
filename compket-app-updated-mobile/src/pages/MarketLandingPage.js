import React from "react"
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import StoreIcon from '@mui/icons-material/Store';
import CategoryIcon from '@mui/icons-material/Category';
import AddBoxIcon from '@mui/icons-material/AddBox';
import ViewCompactAltIcon from '@mui/icons-material/ViewCompactAlt';
import AddBusinessIcon from '@mui/icons-material/AddBusiness';
import BodyDirectionHandler from '../components/BodyDirectionHandler';
import CompketAppDrawer from '../components/CompketAppDrawer'
import {
  Routes,
  Route,
  Link,
  Navigate
} from "react-router-dom";
import { Defenitions, UsedLanguage } from '../base/Defenitions';
import { Helper } from "../base/Helper";
import { BackToUserMode } from "./market/BackToUserMode";
import MarketCreation from "./market/MarketCreation";
import MarketSelection from "./market/MarketSelection";
import ViewItems from "./market/ViewItems";
import AddCategory from "./market/AddCategory";
import AddItem from "./market/AddItem";
import { Button } from "@mui/material";

const BackToUserPageMemo = React.memo(BackToUserMode);
const MarketCreationPageMemo = React.memo(MarketCreation);
const MarketSelectionPageMemo = React.memo(MarketSelection);
const ViewItemsPageMemo = React.memo(ViewItems);
const AddCategoryPageMemo = React.memo(AddCategory);
const AddItemPageMemo = React.memo(AddItem);


export const MarketLandingPage = () => {
  const theme = Defenitions.getTheme()
  const styleOptions = ({
    backgroundColor: theme.palette.secondary.main, alignItems: 'center',
    height: '100vh'
  });
  const list = (<div>
    <Divider />
    <List>
      {[[UsedLanguage.languageRef.MarketSelectionTxt, Defenitions.routes.market.link + Defenitions.routes.marketSelectionRelative.link, <StoreIcon />],
      [UsedLanguage.languageRef.MarketCreationTxt, Defenitions.routes.market.link + Defenitions.routes.marketCreateionRelative.link,
      <AddBusinessIcon />],
      [UsedLanguage.languageRef.viewItemsTxt, Defenitions.routes.market.link + Defenitions.routes.viewItemsRelative.link,
      <ViewCompactAltIcon />],
      [UsedLanguage.languageRef.addItemTxt, Defenitions.routes.market.link + Defenitions.routes.addItemRelative.link,
      <AddBoxIcon />],
      [UsedLanguage.languageRef.addCategoryTxt, Defenitions.routes.market.link + Defenitions.routes.addCategoryRelative.link,
      <CategoryIcon />]].map((item, index) => (
        <Button key={"marketLandPageMenu" + index} color='inherit' style={{
          display: 'flex', justifyContent: 'start', width: '100%',
          textTransform: 'none'
        }}>
          <Link to={item[1]} style={{ textDecoration: 'none', color: "inherit" }} >
            <ListItem>
              <ListItemIcon>
                {item[2]}
              </ListItemIcon>
              <ListItemText primary={item[0]} />
            </ListItem>
          </Link>
        </Button>
      ))}
    </List>
    <Divider />
  </div >)
  const token = Helper.getAccessTokenFromStorage();
  return (
    token !== null ?//checking if loggeed in
      <div
        key="marketLandPageDiv"
        style={styleOptions}>
        <CompketAppDrawer key="marketLandPageDrawer" buttonEnabled={true} buttonText={
          UsedLanguage.languageRef.BackToUserModeTxt}
          buttonLink={Defenitions.routes.market.link
            + Defenitions.routes.backToUserRelative.link}
          drawerItems={<BodyDirectionHandler key="marketPageDrawerItems" fromKey="marketPageDrawerItems" bodyContent={list} />} />
        <Routes key="marketLandPageRoutes" >
          <Route path="/"
            key="marketLandPageRouteDefault"
            element={<Navigate key="marketLandPageRouteNavigateDefault" to={Defenitions.routes.market.link
              + Defenitions.routes.marketSelectionRelative.link} />} />
          <Route
            key="marketLandPageRouteSelection"
            path={Defenitions.routes.marketSelectionRelative.link}
            element={<BodyDirectionHandler
              fromKey="marketLandPageSelection"
              key="marketLandPageSelection" bodyContent={<MarketSelectionPageMemo />} />} />
          <Route
            key="marketLandPageRouteCreation"
            path={Defenitions.routes.marketCreateionRelative.link}
            element={<BodyDirectionHandler
              fromKey="marketLandPageCreation"
              key="marketLandPageCreation" bodyContent={<MarketCreationPageMemo />} />} />
          <Route
            key="marketLandPageRouteBackToUser"
            path={Defenitions.routes.backToUserRelative.link}
            element={<BodyDirectionHandler
              fromKey="marketLandPageBackToUser"
              key="marketLandPageBackToUser" bodyContent={<BackToUserPageMemo />} />} />
          <Route
            key="marketLandPageRouteViewItems"
            path={Defenitions.routes.viewItemsRelative.link}
            element={<BodyDirectionHandler
              fromKey="marketLandPageRouteViewItems"
              key="marketLandPageRouteViewItems" bodyContent={<ViewItemsPageMemo />} />} />
          <Route
            key="marketLandPageRouteAddCategory"
            path={Defenitions.routes.addCategoryRelative.link}
            element={<BodyDirectionHandler
              fromKey="marketLandPageRouteAddCategory"
              key="marketLandPageRouteAddCategory" bodyContent={<AddCategoryPageMemo />} />} />
          <Route
            key="marketLandPageRouteAddItem"
            path={Defenitions.routes.addItemRelative.link}
            element={<BodyDirectionHandler
              fromKey="marketLandPageRouteAddItem"
              key="marketLandPageRouteAddItem" bodyContent={<AddItemPageMemo editItemMode={false} />} />} />
        </Routes>
      </div>
      :
      <Navigate to={Defenitions.routes.default.link} />
  )
}
