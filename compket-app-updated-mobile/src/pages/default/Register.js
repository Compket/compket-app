import { TextField, Button } from '@mui/material'
import { Defenitions, UsedLanguage } from '../../base/Defenitions';
import Grid from '@mui/material/Grid';
import React, { useState } from 'react';
import 'react-phone-number-input/style.css'
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import CloseIcon from '@mui/icons-material/Close';
import PhoneNumberInput from '../../components/PhoneNumberInput';
import { Helper } from '../../base/Helper';
import { DefaultRoutes } from '../../base/Communication';
import { Navigate } from "react-router-dom";
import { toast } from 'react-toastify';
import ServerMsg from '../../models/ServerMsg';

const Register = () => {
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [password, setPassword] = useState("");
    const [errShow, setErrShow] = useState(false);
    const [phoneNumValue, setPhoneNumValue] = useState("");
    const [phoneNumFilled, setPhoneNumFilled] = useState(false);
    const [navigateToLogin, setNavigateToLogin] = useState(false);
    const phoneNumChanged = (e) => {
        setPhoneNumFilled(e.numValue !== "");
        setPhoneNumValue(e.value);
    }
    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    }
    const handleFirstNameChange = (e) => {
        setFirstName(e.target.value);
    }
    const handleLastNameChange = (e) => {
        setLastName(e.target.value);
    }
    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    }
    const handleBtnClick = async (e) => {
        setErrShow(false);
        /*
        console.log(email, firstName, lastName, password, phoneNumValue);
        console.log(Defenitions.serverAddress + DefaultRoutes.registerUserRelativeAddress);
        */
        if (!Helper.validateEmail(email) || !Helper.isPasswordValid(password)
            || firstName === "" || lastName === "" || !phoneNumFilled)
            setErrShow(true);
        else {
            Helper.createToastPromise(DefaultRoutes.RegisterUser(email, firstName, lastName, password, phoneNumValue)).then((response) => 
            {
                setNavigateToLogin(response.Success);
            },
            (e) => {
                //handled by the toast
            });
        }
    }
    return (
        <div
            key="registerDiv"
            style={{
                display: "flex",
                justifyContent: "center",
                alignContent: "center"
            }}>
            {navigateToLogin ? <Navigate key="defaultLandPageRouteNavigateDefault" to={Defenitions.routes.default.link
                + Defenitions.routes.loginRelative.link} /> : <></>}
            <Grid component='main'
                key="registerGrid"
                container
                overflow="hidden"
                columns={1}
                spacing={0}
                justifyContent="space-evenly"
                alignItems="top"
            >
                <Grid item xs={4} container
                    mt={1}
                    key="registerWarningGrid"
                    direction="column"
                    alignItems="center"
                    justifyContent="center">
                    <Collapse key="collapseTop" in={errShow}>
                        <Alert
                            severity="error"
                            action={
                                <IconButton
                                    aria-label="close"
                                    color="inherit"
                                    size="small"
                                    onClick={() => {
                                        setErrShow(false);
                                    }}
                                >
                                    <CloseIcon fontSize="inherit" />
                                </IconButton>
                            }
                            sx={{ mb: 2 }}
                        >
                            {UsedLanguage.languageRef.RegisterFieldsEmptyErrTxt}
                        </Alert>
                    </Collapse>
                </Grid>
                <Grid item xs={4} container
                    mt={1}
                    key="registerGridFieldGrid"
                    direction="column"
                    alignItems="center"
                    justifyContent="center">
                    <TextField
                        key="registerFirstNameField"
                        required
                        onChange={handleFirstNameChange}
                        value={firstName}
                        label={UsedLanguage.languageRef.FirstNameTxt} variant="standard" color="primary" focused />
                </Grid>

                <Grid item xs={4} container
                    mt={1}
                    key="registerLastNameGrid"
                    direction="column"
                    alignItems="center"
                    justifyContent="center">
                    <TextField
                        key="registerLastNameField"
                        required
                        onChange={handleLastNameChange}
                        label={UsedLanguage.languageRef.LastNameTxt} variant="standard" color="primary" focused />
                </Grid>
                <Grid item xs={4} container
                    mt={1}
                    key="registerEmailGrid"
                    direction="column"
                    alignItems="center"
                    justifyContent="center">
                    <TextField
                        key="registerEmailField"
                        dir="ltr"
                        required
                        onChange={handleEmailChange}
                        label={UsedLanguage.languageRef.EmailTxt} variant="standard" color="primary" focused />
                </Grid>
                <Grid item xs={4} container
                    mt={1}
                    direction="column"
                    alignItems="center"
                    key="registerPhoneGrid"
                    justifyContent="center">
                    <PhoneNumberInput specialKey="registerPhoneField" key="registerPhoneField" directionLTR={Defenitions.ltrScreenDirection}
                        onChangeEvent={phoneNumChanged} />
                </Grid>
                <Grid item xs={4} container
                    mt={1}
                    direction="column"
                    alignItems="center"
                    key="registerPasswordGrid"
                    justifyContent="center">
                    <TextField
                        required
                        key="registerPasswordField"
                        dir="ltr"
                        type="password"
                        onChange={handlePasswordChange}
                        label={UsedLanguage.languageRef.PasswordTxt} color="primary" variant="standard" focused />
                </Grid>
                <Grid item xs={4} container
                    mt={1}
                    direction="column"
                    key="registerBtnGrid"
                    alignItems="center"
                    justifyContent="center">
                    <Button color="primary" key="registerBtn" variant="contained" onClick={handleBtnClick}>{UsedLanguage.languageRef.SubmitTxt}</Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default Register;