import { React, useState, useEffect } from 'react'
import { TextField, Button } from '@mui/material'
import { Defenitions, UsedLanguage } from '../../base/Defenitions';
import Grid from '@mui/material/Grid';
import CssBaseline from "@mui/material/CssBaseline";
import { Helper } from '../../base/Helper';
import { toast } from 'react-toastify';
import { DefaultRoutes } from '../../base/Communication';
import { Navigate } from "react-router-dom";
import LanguageSelector from "../../components/LanguageSelector";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [navigateToUserGateway, setNavigate] = useState(false);
  const [checkedLoginToken, setCheckLoginToken] = useState(false);
  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  }
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  }
  const handleBtnClick = async (e) => {
    if (!Helper.validateEmail(email) || !Helper.isPasswordValid(password)) {
      Helper.createToast(UsedLanguage.languageRef.RegisterFieldsEmptyErrTxt);
    }
    else {
      Helper.createToastPromise(DefaultRoutes.LoginUser(email, password)).then((response) => {
        Helper.saveAccessTokenToStorage(response.Data[0]);
        Helper.saveSAccountTypeToStorage(response.Data[1]);
        Helper.saveUserIdToStorage(response.Data[2]);
        setNavigate(true);
      },
        (e) => {
          //handled by the toast
          console.log(e);
        });
    }
  }
  const handleLoginWithToken = async () => {
    if (!navigateToUserGateway && Helper.getAccessTokenFromStorage() != null) {
      Helper.createToastPromise(DefaultRoutes.LoginUserToken(Helper.getAccessTokenFromStorage())).then((response) => {
        //saving updated token
        Helper.saveAccessTokenToStorage(response.Data[0]);
        Helper.saveSAccountTypeToStorage(response.Data[1]);
        Helper.saveUserIdToStorage(response.Data[2]);
        setNavigate(true);
      },
        (e) => {
          //handled by the toast
          console.log(e);
          Helper.saveAccessTokenToStorage(null);
        });
    }
  }

  useEffect(() => {
    if(!checkedLoginToken)
    {
      setCheckLoginToken(true);
      handleLoginWithToken();
    }
  });

  return (
    <div style={{
      display: "flex",
      justifyContent: "center",
      alignContent: "center"
    }}>

      {navigateToUserGateway ? <Navigate to={Defenitions.routes.user.link
        + Defenitions.routes.homeRelative.link} /> : <></>}
      <CssBaseline />
      <Grid component='main'
        container
        overflow="hidden"
        columns={1}
        spacing={0}
        mt={1}
        justifyContent="space-evenly"
        alignItems="top"
      >
        <Grid item xs={4} container
          mt={1}
          direction="column"
          alignItems="center"
          justifyContent="center">
          <TextField dir="ltr" label={UsedLanguage.languageRef.EmailTxt} variant="standard" color="primary" focused onChange={handleEmailChange} />
        </Grid>
        <Grid item xs={4} container
          mt={1}
          direction="column"
          alignItems="center"
          justifyContent="center">
          <TextField dir="ltr"
            type="password" label={UsedLanguage.languageRef.PasswordTxt} color="primary" variant="standard" focused onChange={handlePasswordChange} />
        </Grid>
        <Grid item xs={4} container
          mt={1}
          direction="column"
          alignItems="center"
          justifyContent="center">
          <Button color="primary" variant="contained" onClick={handleBtnClick}>{UsedLanguage.languageRef.LoginTxt}</Button>
        </Grid>
        
        <Grid item xs={4} container
          mt={1}
          direction="column"
          alignItems="center"
          justifyContent="center">
          <LanguageSelector/>
        </Grid>

      </Grid>
    </div>
  )
}

export default Login
