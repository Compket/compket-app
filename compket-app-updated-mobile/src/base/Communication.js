import axios from "axios"
import { Defenitions } from "./Defenitions"
import User from "../models/User"
import Market from "../models/Market"
import ServerMsg from "../models/ServerMsg"
import { Helper } from "./Helper"
import Category from "../models/Category"
import Item from '../models/Item'

export class Communication {
    static Success200Code = 200;
    static NotFound404Code = 404;
    static async requestToServer(relativeRoute, body, method, shouldAlterData = true, headers = {}) {
        try {
            let res = await axios({
                method: method,
                baseURL: Defenitions.serverAddress + relativeRoute,
                headers: headers,
                data: shouldAlterData && body !== null ? Helper.objectToJsonMapWithLowerCaseKeys(body) : body
            });
            return res;
        }
        catch (error) {
            return error.response !== undefined ? error.response : error;
        }
    }
    static async getFromServerBlob(relativeRoute, headers = {}) {
        try {
            let res = await axios({
                method: 'get',
                responseType: 'blob',
                baseURL: Defenitions.serverAddress + relativeRoute,
                headers: headers
            });
            return res;
        }
        catch (error) {
            return error.response !== undefined ? error.response : error;
        }
    }

    static async postToServer(relativeRoute, body, shouldAlterData = true, headers = {}) {
        return await Communication.requestToServer(relativeRoute, body, "post", shouldAlterData, headers);
    }
    static async getFromServer(relativeRoute, headers = {}) {
        return await Communication.requestToServer(relativeRoute, null, "get", true, headers);
    }
    static async putToServer(relativeRoute, body, shouldAlterData = true, headers = {}) {
        return await Communication.requestToServer(relativeRoute, body, "put", shouldAlterData, headers);
    }
    static async deleteFromServer(relativeRoute, headers = {}) {
        return await Communication.requestToServer(relativeRoute, null, "delete", true, headers);
    }
}

export class DefaultRoutes {
    static relativeAddress = "/Default"
    static registerUserRelativeAddress = DefaultRoutes.relativeAddress + "/Register";
    static loginUserRelativeAddress = DefaultRoutes.relativeAddress + "/Login";
    static loginUserTokenRelativeAddress = DefaultRoutes.relativeAddress + "/LoginToken";

    static RegisterUserSuccessCode = Communication.Success200Code;
    /**
     * 
     * @param {*} email 
     * @param {*} firstName 
     * @param {*} lastName 
     * @param {*} password 
     * @param {*} phoneNum 
     * @returns promise success or error with [ServerMsg]
     */
    static async RegisterUser(email, firstName, lastName, password, phoneNum) {
        let user = new User({
            email: email, fisrtname: firstName, lastname: lastName,
            password: await Helper.strToSha512(password), phonenum: phoneNum
        });
        let res = await Communication.postToServer(DefaultRoutes.registerUserRelativeAddress, user);
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === DefaultRoutes.RegisterUserSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static LoginUserSuccessCode = Communication.Success200Code;
    /**
     * 
     * @param {*} email 
     * @param {*} firstName 
     * @param {*} lastName 
     * @param {*} password 
     * @param {*} phoneNum 
     * @returns promise success or error with [ServerMsg]
     */
    static async LoginUser(email, password) {
        let user = new User({
            email: email, password: await Helper.strToSha512(password)
        });
        let res = await Communication.postToServer(DefaultRoutes.loginUserRelativeAddress, user);
        const ret = new ServerMsg(res.status, res.data.item1, JSON.parse(res.data.item2), res.status === DefaultRoutes.LoginUserSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static LoginUserTokenSuccessCode = Communication.Success200Code;
    /**
     * 
     * @param {*} email 
     * @param {*} firstName 
     * @param {*} lastName 
     * @param {*} password 
     * @param {*} phoneNum 
     * @returns promise success or error with [ServerMsg]
     */
    static async LoginUserToken(token) {
        let res = await Communication.getFromServer(DefaultRoutes.loginUserTokenRelativeAddress, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, JSON.parse(res.data.item2), res.status === DefaultRoutes.LoginUserTokenSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }


}

export class AdminRoutes {
    static relativeAddress = "/Admin";
    static getUsersRelativeAddress = AdminRoutes.relativeAddress + "/GetUsers";
    static setUserAccessLevelRelativeAddress = AdminRoutes.relativeAddress + "/SetUserAccountType?userId={userId}&newType={newType}";
    static deleteUserRelativeAddress = AdminRoutes.relativeAddress + "/DeleteUser?userId={userId}";

    static getUsersSuccessCode = Communication.Success200Code;
    static setUserAccessLevelSuccessCode = Communication.Success200Code;
    static deleteUserSuccessCode = Communication.Success200Code;

    static async GetUsers(token) {
        let res = await Communication.getFromServer(AdminRoutes.getUsersRelativeAddress, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, JSON.parse(res.data.item2), res.status === AdminRoutes.getUsersSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async SetUserAccessLevel(token, userId, accessLevel) {
        let res = await Communication.putToServer(Helper.stringWithParamToFullString(AdminRoutes.setUserAccessLevelRelativeAddress, { userId: userId, newType: accessLevel }),
            null,false,Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === AdminRoutes.setUserAccessLevelSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async DeleteUser(token, userId)
    {
        let res = await Communication.deleteFromServer(Helper.stringWithParamToFullString(AdminRoutes.deleteUserRelativeAddress, { userId: userId}),
            Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === AdminRoutes.deleteUserSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }
}

export class UserRoutes {

    static relativeAddress = "/User";
    static viewMarketsRelativeAddress = UserRoutes.relativeAddress + "/ViewMarkets";
    static getAllLocationsRelativeAddress = UserRoutes.relativeAddress + "/GetAllLocations";
    static getMarketItemsRelativeAddress = UserRoutes.relativeAddress + "/GetMarketItems?mid={mid}";
    static getGenericItemsRelativeAddress = UserRoutes.relativeAddress + "/GetGenericItems";
    static generateComparedListsRelativeAddress = UserRoutes.relativeAddress
        + "/GenerateComparedLists";


    static viewMarketsSuccessCode = Communication.Success200Code;
    static getAllLocationsSuccessCode = Communication.Success200Code;
    static getMarketItemsSuccessCode = Communication.Success200Code;
    static getGenericItemsSuccessCode = Communication.Success200Code;
    static generateComparedListsSuccessCode = Communication.Success200Code;

    static async ViewMarkets(token, name = null, phone = null, locationCountry = null, locationCity = null) {
        let nameKey = Defenitions.UserViewMarketsNameQuery;
        let phoneKey = Defenitions.UserViewMarketsPhoneQuery;
        let locationCountryKey = Defenitions.UserViewMarketsLocationCountryQuery;
        let locationCityKey = Defenitions.UserViewMarketsLocationCityQuery;
        let query = {};
        query[nameKey] = name;
        query[phoneKey] = phone;
        query[locationCountryKey] = locationCountry;
        query[locationCityKey] = locationCity;
        //console.log("req", query);
        let res = await Communication.postToServer(UserRoutes.viewMarketsRelativeAddress, query, true, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, JSON.parse(res.data.item2), res.status === UserRoutes.viewMarketsSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async GetAllLocations(token) {
        let res = await Communication.getFromServer(UserRoutes.getAllLocationsRelativeAddress, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, JSON.parse(res.data.item2), res.status === UserRoutes.getAllLocationsSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async GetMarketItems(token, marketId) {
        let res = await Communication.getFromServer(Helper.stringWithParamToFullString(
            UserRoutes.getMarketItemsRelativeAddress, { mid: marketId }),
            Helper.makeTokenHeader(token));
        let success = res.status === UserRoutes.getMarketItemsSuccessCode;
        const ret = new ServerMsg(res.status, res.data.item1, success ?
            JSON.parse(res.data.item2) : res.data.item2, success);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async GetGenericItems(token, itemName = null, itemBarcode = null, itemCategoryId = Defenitions.undefinedId) {
        let query = {};
        query["CategoryId"] = itemCategoryId;
        query["Name"] = itemName;
        query["Barcode"] = itemBarcode;
        let res = await Communication.postToServer(UserRoutes.getGenericItemsRelativeAddress, query, true, Helper.makeTokenHeader(token));
        let success = res.status === UserRoutes.getGenericItemsSuccessCode;
        const ret = new ServerMsg(res.status, res.data.item1, success ? JSON.parse(res.data.item2) : res.data.item2, success);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async GenerateComparedLists(token, country, city, barcodesLst) {
        let query = {};
        query["City"] = city;
        query["Country"] = country;
        query["BarcodesLst"] = barcodesLst;
        //console.log(query);
        let res = await Communication.postToServer(UserRoutes.generateComparedListsRelativeAddress, query, true, Helper.makeTokenHeader(token));
        let success = res.status === UserRoutes.generateComparedListsSuccessCode;
        const ret = new ServerMsg(res.status, res.data.item1, success ? JSON.parse(res.data.item2) : res.data.item2, success);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }
}

export class MarketRoutes {
    static relativeAddress = "/Market"
    static createMarketRelativeAddress = MarketRoutes.relativeAddress + "/CreateMarket";
    static setMarketImageRelativeAddress = MarketRoutes.relativeAddress + "/SetMarketImage?marketId={marketId}";
    static createCategoryRelativeAddress = MarketRoutes.relativeAddress + "/CreateCategory";
    static setCategoryImageRelativeAddress = MarketRoutes.relativeAddress + "/SetCategoryImage?categoryId={categoryId}";
    static viewOwnedMarketsRelativeAddress = MarketRoutes.relativeAddress + "/ViewOwnedMarkets";
    static getMarketImageRelativeAddress = MarketRoutes.relativeAddress + "/GetMarketImage?marketId={marketId}&randomSeed=";
    static getItemImageRelativeAddress = MarketRoutes.relativeAddress + "/GetItemImage?itemId={itemId}&randomSeed=";
    static getCategoriesRelativeAddress = MarketRoutes.relativeAddress + "/GetCategoris";
    static addItemRelativeAddress = MarketRoutes.relativeAddress + "/createItem?marketId={marketId}";
    static setItemImageRelativeAddress = MarketRoutes.relativeAddress + "/SetItemImage?itemId={itemId}";
    static getMarketItemsRelativeAddress = MarketRoutes.relativeAddress +
        '/GetMarketItems?marketId={marketId}';
    static deleteItemRelativeAddress = MarketRoutes.relativeAddress + "/deleteItem?marketId={marketId}&itemId={itemId}"
    static deleteMarketRelativeAddress = MarketRoutes.relativeAddress + "/deleteMarket?marketId={marketId}"
    static editItemRelativeAddress = MarketRoutes.relativeAddress + "/editItem?marketId={marketId}";
    static editMarketRelativeAddress = MarketRoutes.relativeAddress + "/editMarket";


    static CreatemarketSuccessCode = Communication.Success200Code;
    static setMarketImageSuccessCode = Communication.Success200Code;
    static viewOwnedMarketsSuccessCode = Communication.Success200Code;
    static viewOwnedMarketsSuccessCode = Communication.Success200Code;
    static getMarketImageSuccessCode = Communication.Success200Code;
    static getItemImageSuccessCode = Communication.Success200Code;
    static getCategoriesSuccessCode = Communication.Success200Code;
    static createCategorySuccessCode = Communication.Success200Code;
    static setCategoryImageSuccessCode = Communication.Success200Code;
    static additemSuccessCode = Communication.Success200Code;
    static setItemImageSuccessCode = Communication.Success200Code;
    static getMarketItemsSuccessCode = Communication.Success200Code;
    static deleteItemSuccessCode = Communication.Success200Code;
    static deleteMarketSuccessCode = Communication.Success200Code;
    static editItemSuccessCode = Communication.Success200Code;
    static editMarketSuccessCode = Communication.Success200Code;

    /**
     * 
     * @param {*} token 
     * @param {*} marketName 
     * @param {*} marketAddressCountry 
     * @param {*} marketAddressCity 
     * @param {*} marketAddressStreet 
     * @param {*} phoneNum 
     * @param {*} description 
     * @returns promise success or error with [ServerMsg]
     */
    static async CreateMarket(token, marketName, marketAddressCountry, marketAddressCity, marketAddressStreet,
        phoneNum, description) {
        let market = new Market(
            {
                addresscity: marketAddressCity,
                addresscountry: marketAddressCountry,
                addressstreet: marketAddressStreet,
                description: description,
                name: marketName,
                phonenum: phoneNum
            }
        );
        let res = await Communication.postToServer(MarketRoutes.createMarketRelativeAddress, market, true, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === MarketRoutes.CreatemarketSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async SetMarketImage(token, marketId, imageFormData) {
        let res = await Communication.postToServer(Helper.stringWithParamToFullString(MarketRoutes.setMarketImageRelativeAddress,
            { marketId: marketId })
            , imageFormData, false, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === MarketRoutes.setMarketImageSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async ViewOwnedMarkets(token) {
        let res = await Communication.getFromServer(MarketRoutes.viewOwnedMarketsRelativeAddress, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1,
            res.data.item2 !== null && res.data.item2 !== undefined ? JSON.parse(res.data.item2) : "", res.status === MarketRoutes.viewOwnedMarketsSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async GetCategories(token) {
        let res = await Communication.getFromServer(MarketRoutes.getCategoriesRelativeAddress, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1,
            res.data.item2 !== null && res.data.item2 !== undefined ? JSON.parse(res.data.item2) : "", res.status === MarketRoutes.getCategoriesSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async GetMarketImage(token, marketId) {
        /*let res = await Communication.getFromServer(Helper.stringWithParamToFullString(MarketRoutes.getMarketImageRelativeAddress,
            {marketId:marketId})*/
        let res = await Communication.getFromServerBlob(Helper.stringWithParamToFullString(MarketRoutes.getMarketImageRelativeAddress,
            { marketId: marketId }) + Helper.getRandomSeedForImage()
            , Helper.makeTokenHeader(token));
        let success = res.status === MarketRoutes.getMarketImageSuccessCode;
        const ret = new ServerMsg(res.status, res.data.item1,
            res, success);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async CreateCategory(token, name, description) {
        let category = new Category(
            {
                name: name,
                description: description
            }
        );
        let res = await Communication.postToServer(MarketRoutes.createCategoryRelativeAddress, category, true, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === MarketRoutes.createCategorySuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async SetCategoryImage(token, categoryId, imageFormData) {
        let res = await Communication.postToServer(Helper.stringWithParamToFullString(MarketRoutes.setCategoryImageRelativeAddress,
            { categoryId: categoryId })
            , imageFormData, false, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === MarketRoutes.setCategoryImageSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async CreateItem(token, marketId, name, description, barcode, price, currency, categoryId) {
        let item = new Item({
            categoryid: categoryId,
            marketid: marketId,
            name: name,
            description: description,
            barcodenum: barcode,
            price: price,
            currency: currency
        });
        let res = await Communication.postToServer(Helper.stringWithParamToFullString(MarketRoutes.addItemRelativeAddress,
            { marketId: marketId }), item, true, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === MarketRoutes.additemSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async SetItemImage(token, itemId, imageFormData) {
        let res = await Communication.postToServer(Helper.stringWithParamToFullString(MarketRoutes.setItemImageRelativeAddress,
            { itemId: itemId })
            , imageFormData, false, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === MarketRoutes.setItemImageSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async GetMarketItems(token, marketId) {
        let res = await Communication.getFromServer(Helper.stringWithParamToFullString(MarketRoutes.getMarketItemsRelativeAddress,
            { marketId: marketId }), Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1,
            res.data.item2 !== null && res.data.item2 !== undefined ? JSON.parse(res.data.item2) : "",
            (res.status === MarketRoutes.getMarketItemsSuccessCode || res.status === Communication.NotFound404Code));
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async GetItemImage(token, itemId) {
        let res = await Communication.getFromServerBlob(Helper.stringWithParamToFullString(MarketRoutes.getItemImageRelativeAddress,
            { itemId: itemId }) + Helper.getRandomSeedForImage()
            , Helper.makeTokenHeader(token));
        let success = res.status === MarketRoutes.getItemImageSuccessCode;
        const ret = new ServerMsg(res.status, res.data.item1,
            res, success);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async DeleteItem(token, marketId, itemId) {
        let res = await Communication.deleteFromServer(Helper.stringWithParamToFullString(MarketRoutes.deleteItemRelativeAddress,
            { marketId: marketId, itemId: itemId })
            , Helper.makeTokenHeader(token));
        let success = res.status === MarketRoutes.deleteItemSuccessCode;
        const ret = new ServerMsg(res.status, res.data.item1,
            res, success);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async EditItem(token, marketId, itemId, name, description, barcode, price, currency, categoryId) {
        let item = new Item({
            id: itemId,
            categoryid: categoryId,
            marketid: marketId,
            name: name,
            description: description,
            barcodenum: barcode,
            price: price,
            currency: currency
        });
        let res = await Communication.postToServer(Helper.stringWithParamToFullString(MarketRoutes.editItemRelativeAddress,
            { marketId: marketId }), item, true, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === MarketRoutes.editItemSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async DeleteMarket(token, marketId) {
        let res = await Communication.deleteFromServer(Helper.stringWithParamToFullString(MarketRoutes.deleteMarketRelativeAddress,
            { marketId: marketId })
            , Helper.makeTokenHeader(token));
        let success = res.status === MarketRoutes.deleteMarketSuccessCode;
        const ret = new ServerMsg(res.status, res.data.item1,
            res, success);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

    static async EditMarket(token, marketId, marketName, marketAddressCountry, marketAddressCity, marketAddressStreet,
        phoneNum, description) {
        let market = new Market(
            {
                id: marketId,
                addresscity: marketAddressCity,
                addresscountry: marketAddressCountry,
                addressstreet: marketAddressStreet,
                description: description,
                name: marketName,
                phonenum: phoneNum
            }
        );
        //console.log(market);
        let res = await Communication.postToServer(MarketRoutes.editMarketRelativeAddress, market, true, Helper.makeTokenHeader(token));
        const ret = new ServerMsg(res.status, res.data.item1, res.data.item2, res.status === MarketRoutes.editMarketSuccessCode);
        return ret.Success === true ? Promise.resolve(ret) : Promise.reject(ret);
    }

}