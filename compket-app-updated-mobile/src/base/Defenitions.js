import { createTheme } from '@mui/material/styles';
import { purple } from '@mui/material/colors';
export class Defenitions {
    static minPasswordLength = 8;
    static accountType = Object.freeze({
        Customer: 0,
        VerifiedCustomer: 1,
        MarketOwner: 2,
        SystemAdmin: 3
    });
    static fileType = Object.freeze({
        PngPicture: 0,
        JpegPicture: 1,
        JpgPicture: 2,
        NotAPicture: 3,
    });
    static currency = Object.freeze([
        "₪",
        "$",
        "€"
    ]);
    static UserViewMarketsNameQuery = "name";
    static UserViewMarketsPhoneQuery = "phone";
    static UserViewMarketsLocationCountryQuery = "locationCountry";
    static UserViewMarketsLocationCityQuery = "locationCity";
    static userIdKeyForLocalStorage = "user_id";
    static toastDuration = 2500;
    static accessTokenkeyForLocalStorage = "access_token";
    static mapAutoCompleteInputId = 'mapAutoCompleteInput';
    static marketIdkeyForLocalStorage = "market_id";
    static shoppingCartKeyForLocalStorage = "shopping_cart";
    static selectedLanguageKeyForLocalStorage = "selected_language";
    static accoutTypekeyForLocalStorage = "account_type";
    static undefinedId = -1;
    static tokenKeyForHeader = "accessToken";
    static undefinedErrCode = -1;
    static serverAddress = "https://192.168.0.105:5001";
    static routes = {
        default: {
            link: "/default",
            name: "defaultLandPage"
        },
        registerRelative: {
            link: "/register",
            name: "Register"
        },
        loginRelative: {
            link: "/login",
            name: "Login"
        },
        user: {
            link: "/user",
            name: "userLandPage"
        },
        homeRelative: {
            link: "/home",
            name: "Home"
        },
        viewMarketsRelative: {
            link: "/viewMarkets",
            name: "ViewMarkets"
        },
        logoutRelative: {
            link: "/logout",
            name: "Logout"
        },
        market: {
            link: "/market",
            name: "marketLandPage"
        },
        marketSelectionRelative: {
            link: "/select",
            name: "MarketSelection"
        },
        marketCreateionRelative: {
            link: "/create",
            name: "MarketCreation"
        },
        backToUserRelative: {
            link: "/back",
            name: "BacktoUser"
        },
        viewItemsRelative: {
            link: "/items",
            name: "ViewItems"
        },
        addItemRelative: {
            link: "/addItem",
            name: "AddItem"
        },
        addCategoryRelative: {
            link: "/addCategory",
            name: "AddCatgory"
        },
        userViewMarketsItemsRelative: {
            link: "/viewItems",
            name: "UserViewMarketItems"
        },
        userAddItemsToShoppingCartRelative: {
            link: "/addItems",
            name: "UserAddItemsToShoppingCart"
        },
        userViewShoppingCartRelative: {
            link: "/viewCartItems",
            name: "UserViewShoppingCart"
        },
        userGenerateCopmaredListRelative: {
            link: "/generateCompketList",
            name: "UserGenerateCompketList"
        },
        adminPanelRelative: {
            link: "/admin",
            name: "AdminPanel"
        }

    };
    static undefinedLength = 0;
    static numbersCodeMap = {
        Code: ["", Defenitions.undefinedLength, ""],
        Israel: ["+972", 9, "(0)541232288"],
        Germany: ["+49", 11, "(0)12345678987"]
    };

    static appName = "Compket";
    static maxTextFieldLength = 50;
    static height = 0;
    static width = 0;
    static ltrScreenDirection = false;
    static setScreenDirectionLtr() {
        this.ltrScreenDirection = true;
    }
    static setScreenDirectionRtl() {
        this.ltrScreenDirection = false;
    }
    static getTheme() {
        return createTheme({
            palette: Defenitions.palette,
            direction: Defenitions.ltrScreenDirection ? "ltr" : "rtl",

        });
    }
    static setWidthHeight(w, h) {
        this.height = h;
        this.width = w;
    }
    static palette = {
        primary: {
            // Purple and green play nicely together.
            main: purple[500],
        },
        secondary: {
            // This is green.A700 as hex.

            main: '#FFFFFF',
        },
        info: {
            main: '#FFFFFF',
        },
        // Used by `getContrastText()` to maximize the contrast between
        // the background and the text.
        contrastThreshold: 3,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: 0.2,
    }
}

export class UsedLanguage {
    static languageRef;
    static languageType = Object.freeze({
        EN: 0,
        HE: 1
    });
    static setLang(selectedLanguageType) {
        if (UsedLanguage.languageRef === null || UsedLanguage.languageRef === undefined) {
            if (selectedLanguageType === UsedLanguage.languageType.EN) {
                UsedLanguage.setLangEN();
            }
            else if (selectedLanguageType === UsedLanguage.languageType.HE) {
                UsedLanguage.setLangHE();
            }
        }
    }
    static setLangHE() {
        UsedLanguage.languageRef = new HebrewLanguages();
        Defenitions.setScreenDirectionRtl();
    }
    static setLangEN() {
        UsedLanguage.languageRef = new EnglishLanguages();
        Defenitions.setScreenDirectionLtr();
    }
}

export class Language {
    langCode = null;
    LoginTxt = null;
    FirstNameTxt = null;
    LastNameTxt = null;
    EmailTxt = null;
    PhoneNumTxt = null;
    PasswordTxt = null;
    RegisterTxt = null;
    SubmitTxt = null;
    MustChooseCountryCodeFirstTxt = null;
    MustNotUseZeroAtBeginningOfNumTxt = null;
    RegisterFieldsEmptyErrTxt = null;
    NetworkRequestInProgress = null;
    NetworkRequestFailed = null;
    NetworkRequestSucceeded = null;
    LogOutTxt = null;
    HomeTxt = null;
    MarketModeTxt = null;
    MarketSelectionTxt = null;
    MarketCreationTxt = null;
    BackToUserModeTxt = null;
    MarketNameTxt = null;
    MarketDescriptionTxt = null;
    FillOutTheFormTxt = null;
    EnterLocationTxt = null;
    TheSelectedLocationTxt = null;
    LocationNotSetTxt = null;
    ClearSelectionTxt = null;
    selectFileTxt = null;
    selectAFileTxt = null;
    selectAnImageTxt = null;
    stringPlacholderTxt = null;
    noOwnedMarketsTxt = null;
    viewItemsTxt = null;
    addCategoryTxt = null;
    categoryName = null;
    categoryDescription = null;
    mustBeAnAdminToAccessTxt = null;
    addItemTxt = null;
    itemNameTxt = null;
    itemDescriptionTxt = null;
    itemPriceTxt = null;
    currencyTxt = null;
    barcodeNumTxt = null;
    restartTheAppTxt = null;
    youCantUseThisFeatureTxt = null;
    torchTxt = null;
    scannerUnavailableTxt = null;
    noItemsFoundInThisSearchTxt = null;
    agreeTxt = null;
    disagreeTxt = null;
    deleteItemTxt = null;
    deleteItemBodyTxt = null;
    deleteTxt = null;
    editTxt = null;
    openSearchTxt = null;
    barcodeOrNameTxt = null;
    selectFileTxt = null;
    deleteMarketTxt = null;
    deleteMarketBodyTxt = null;
    confirmByTypingTxt = null;
    confirmTxt = null;
    setNewLocationTxt = null;
    viewMarketsTxt = null;
    countryTxt = null;
    cityTxt = null;
    marketNameOrPhoneTxt = null;
    noStoresAtThisMomentTxt = null;
    addItemToShoppingCartTxt = null;
    chooseCategoryFirstTxt = null;
    itemAddedSuccessfully = null;
    viewCartTxt = null;
    itemRemovedSuccessfulyTxt = null;
    amountofItems = null;
    generateCompketListTxt = null;
    youMustAddItemsToCartFirstTxt = null;
    listTotalTxt = null;
    allItemsFoundTxt = null;
    allItemsNotFoundTxt = null;
    missingItemsTxt = null;
    emptyCartTxt = null;
    adminPanelTxt = null;
    selectYourLanguageTxt = null;
    changesWillTakeAffectOnReloadTxt = null;
    noResultsAtmTxt = null;
    CustomerTxt = null;
    VerifiedCustomerTxt = null;
    MarketOwnerTxt = null;
    SystemAdminTxt = null;
    userTypeTxt = null;
    youCantDeleteAdminTxt = null;
    errCodesLang = Object.freeze({
        0: "GeneralServerError",
        1: "ErrUserExistsWithGivenName",
        2: "ErrUserExistsWithGivenEmail",
        3: "ErrUserExistsWithGivenPhone",
        4: "ErrUserInvalidEmail",
        5: "ErrUserInvalidPhone",
        6: "ErrUserStructureLacksContent",
        7: "ErrUserPasswordIsTooShort",
        8: "ErrCategoryExistsWithGivenName",
        9: "ErrCategoryDoesntExists",
        10: "ErrMarketExistsWithGivenNameAtAddressOrPhoneEmpty",
        11: "ErrUserIdDoesntExist",
        12: "ErrPreferenceExists",
        13: "ErrItemMarketIdDoesntExist",
        14: "ErrItemCategoryDoesntExist",
        15: "ErrItemStructureInvalid",
        16: "ErrItemDoesntExist",
        17: "ErrItemExistsAtThisMarket",
        18: "ErrWrongCredentials",
        19: "ErrCantSaveImage",
        20: "ErrMarketNotExistingWithThisId",
    });
}

export class HebrewLanguages extends Language {
    constructor() {
        super()
        this.langCode = "he";
        this.LoginTxt = "התחבר";
        this.FirstNameTxt = "שם פרטי";
        this.LastNameTxt = "שם משפחה";
        this.EmailTxt = "אי-מייל";
        this.PhoneNumTxt = "מספר פלאפון";
        this.PasswordTxt = "ססמא";
        this.RegisterTxt = "הרשם";
        this.SubmitTxt = "הגש";
        this.MustChooseCountryCodeFirstTxt = "עליך לבחור בקוד מדינה ";
        this.MustNotUseZeroAtBeginningOfNumTxt = "יש להזין מספר ללא 0 בתחילתו";
        this.RegisterFieldsEmptyErrTxt = " וודא כי תיבות הטקסט מלאות, אימייל תקין וססמא באורך לפחות " +
            Defenitions.minPasswordLength + " תווים";
        this.NetworkRequestInProgress = "בקשה בטיפול";
        this.NetworkRequestFailed = "🤯 בקשה נכשלה ";
        this.NetworkRequestSucceeded = "👌 הושלם בהצלחה ";
        this.LogOutTxt = "התנתק";
        this.HomeTxt = "בית";
        this.MarketModeTxt = "מצב חנות";
        this.MarketSelectionTxt = "בחירת חנות";
        this.MarketCreationTxt = "יצירת חנות";
        this.BackToUserModeTxt = "למצב משתמש";
        this.MarketNameTxt = "שם החנות";
        this.MarketDescriptionTxt = "תיאור החנות";
        this.FillOutTheFormTxt = "מלא את תוכן השדות";
        this.EnterLocationTxt = "הכנס מיקום";
        this.TheSelectedLocationTxt = "המיקום הנבחר הוא: ";
        this.LocationNotSetTxt = "מיקום לא נבחר עדיין";
        this.ClearSelectionTxt = "נקה את הבחירה";
        this.selectFileTxt = "בחר בתמונה על מנת להציגה";
        this.selectAFileTxt = "בחר בקובץ";
        this.selectAnImageTxt = "בחר בתמונה";
        this.stringPlacholderTxt = "מחרוזת";
        this.noOwnedMarketsTxt = "לא נמצאו חנויות בבעלותך";
        this.viewItemsTxt = "צפיה במוצרים";
        this.addCategoryTxt = "הוספת קטגוריה";
        this.categoryName = "שם הקטגוריה";
        this.categoryDescription = "תיאור הקטגוריה";
        this.mustBeAnAdminToAccessTxt = "עליך להיות בעל גישת מנהל מערכת על מנת לגשת לעמוד זה";
        this.addItemTxt = "הוסף מוצר";
        this.itemNameTxt = "שם המוצר";
        this.itemDescriptionTxt = "תיאור המוצר";
        this.itemPriceTxt = "מחיר המוצר";
        this.currencyTxt = "מטבע";
        this.barcodeNumTxt = "מספר ברקוד";
        this.restartTheAppTxt = "הפעל מחדש את התוכנה - פעולה זו חד פעמית";
        this.youCantUseThisFeatureTxt = "אינך יכול להשתמש במלוא יכולת התוכנה כעת";
        this.torchTxt = "פנס";
        this.scannerUnavailableTxt = "סורק אינו זמין כעת";
        this.noItemsFoundInThisSearchTxt = "לא נמצאו מוצרים בחיפוש הנוכחי";
        this.agreeTxt = "מסכים";
        this.disagreeTxt = "לא מסכים";
        this.deleteItemTxt = "למחוק את המוצר?";
        this.deleteItemBodyTxt = "פעולה זו בלתי הפיכה";
        this.deleteTxt = "מחק";
        this.editTxt = "ערוך";
        this.openSearchTxt = "חיפוש חופשי";
        this.barcodeOrNameTxt = "שם או ברקוד";
        this.selectFileTxt = "עיין בקבצים";
        this.deleteMarketTxt = "למחוק את החנות?";
        this.deleteMarketBodyTxt = "פעולה זו בלתי הפיכה!";
        this.confirmByTypingTxt = "אמת על ידי הקלדת: ";
        this.confirmTxt = "מאשר";
        this.setNewLocationTxt = "הגדר מיקום מחדש";
        this.viewMarketsTxt = "צפיה בחנויות";
        this.countryTxt = "מדינה";
        this.cityTxt = "עיר";
        this.marketNameOrPhoneTxt = "שם החנות או מספר הטלפון שלה";
        this.noStoresAtThisMomentTxt = "אין חנויות כרגע";
        this.addItemToShoppingCartTxt = "הוסף מוצר לעגלת הקניות";
        this.chooseCategoryFirstTxt = "ראשית יש לבחור בקטגוריה";
        this.itemAddedSuccessfully = "המוצר נוסף בהצלחה לרשימת הקניות שלך";
        this.viewCartTxt = "צפה בעגלת הקניות";
        this.itemRemovedSuccessfulyTxt = "המוצר הוסר";
        this.amountofItems = "כמות המוצרים: ";
        this.generateCompketListTxt = "צור רשימות השוואה";
        this.youMustAddItemsToCartFirstTxt = "ראשית, הוסף מוצרים לעגלת הקניות";
        this.listTotalPriceTxt = "מחיר עבור רשימת הקניות:";
        this.allItemsFoundTxt = "כל המוצרים נמצאו";
        this.allItemsNotFoundTxt = "לא כל המוצרים נמצאו";
        this.missingItemsTxt = "מוצרים חסרים:";
        this.emptyCartTxt = "נקה את עגלת הקניות";
        this.adminPanelTxt = "פאנל ניהול";
        this.selectYourLanguageTxt = "בחירת שפה";
        this.changesWillTakeAffectOnReloadTxt = "שינויים יושפעו בפתיחה הבאה של האתר";
        this.noResultsAtmTxt = "אין תוצאות";
        this.CustomerTxt = "לקוח";
        this.VerifiedCustomerTxt = "לקוח מאומת";
        this.MarketOwnerTxt = "מנהל חנות";
        this.SystemAdminTxt = "מנהל מערכת";
        this.userTypeTxt = "סוג משתמש";
        this.youCantDeleteAdminTxt = "ייתכן וניסית למחוק מנהל מערכת - פעולה זו לא נתמכת";
        this.errCodesLang = Object.freeze({
            0: "שגיאת שרת כללית",
            1: "משתמש קיים עם שם זה",
            2: "משתמש קיים עם אימייל זה",
            3: "משתמש קיים עם מספר פלאפון זה",
            4: "אימייל לא תקני",
            5: "מספר פלאפון לא תקני",
            6: "מבנה משתמש אינו מלא",
            7: "ססמא קצרה מדי",
            8: "קטגוריה קיימת עם שם זה",
            9: "קטגוריה אינה קיימת",
            10: "חנות זו כבר קיימת עם שם זהה בכתובת זו,מספר זהה, או מספר הטלפון ריק",
            11: "מזהה משתמש אינו קיים",
            12: "פרפרנס משתמש כבר קיים",
            13: "החנות שנבחרה עבור המוצר לא קיימת",
            14: "הקטגוריה שנבחרה עבור מוצר זה אינה קיימת",
            15: "מבנה מוצר אינו מלא",
            16: "מוצר לא קיים",
            17: "מוצר קיים עם שם/ברקוד בחנות זו",
            18: "פרטי התחברות לא נכונים",
            19: "לא ניתן לשמור את התמונה",
            20: "לא קיימת חנות עם מזהה נתון",
        });
    }

}

export class EnglishLanguages extends Language {
    constructor() {
        super()
        this.langCode = "en";
        this.LoginTxt = "Log in";
        this.FirstNameTxt = "First Name";
        this.LastNameTxt = "Last Name";
        this.EmailTxt = "Email";
        this.PhoneNumTxt = "Phone Number";
        this.PasswordTxt = "Password";
        this.RegisterTxt = "Register";
        this.SubmitTxt = "Submit";
        this.MustChooseCountryCodeFirstTxt = "You must choose a country code first!";
        this.MustNotUseZeroAtBeginningOfNumTxt = "Enter a number without the zero prefix!";
        this.RegisterFieldsEmptyErrTxt = "Make sure that the form is filled out, valid email and password with at least " +
            Defenitions.minPasswordLength + " chars";
        this.NetworkRequestInProgress = "Request in progress";
        this.NetworkRequestFailed = "🤯 Request failed ";
        this.NetworkRequestSucceeded = "👌 Request completed ";
        this.LogOutTxt = "Logout";
        this.HomeTxt = "Home";
        this.MarketModeTxt = "Market Mode";
        this.MarketSelectionTxt = "Market Selection";
        this.MarketCreationTxt = "Market Creation";
        this.BackToUserModeTxt = "User Mode";
        this.MarketNameTxt = "Market Name";
        this.MarketDescriptionTxt = "Market Description";
        this.FillOutTheFormTxt = "Fill out the form";
        this.EnterLocationTxt = "Enter location";
        this.TheSelectedLocationTxt = "The Selected location is: ";
        this.LocationNotSetTxt = "Location not set yet";
        this.ClearSelectionTxt = "Clear your selection";
        this.selectFileTxt = "Choose an image to see a preview";
        this.selectAFileTxt = "Choose a file";
        this.selectAnImageTxt = "Choose an image";
        this.stringPlacholderTxt = "String";
        this.noOwnedMarketsTxt = "We couldn't find any markets under your ownership";
        this.viewItemsTxt = "View Items";
        this.addCategoryTxt = "Add Category";
        this.categoryName = "Category Name";
        this.categoryDescription = "Category description";
        this.mustBeAnAdminToAccessTxt = "You cant access this page without system admin privilege";
        this.addItemTxt = "Add an item";
        this.itemNameTxt = "Item's name";
        this.itemDescriptionTxt = "Item's description";
        this.itemPriceTxt = "Item's price";
        this.currencyTxt = "Currency";
        this.barcodeNumTxt = "Barcode Number";
        this.restartTheAppTxt = "Please restart the app - this action is a one time action only.";
        this.youCantUseThisFeatureTxt = "You can't use the full features of the app now.";
        this.torchTxt = "Torch";
        this.scannerUnavailableTxt = "The scanner is unavailable at this time";
        this.noItemsFoundInThisSearchTxt = "No items found under the current search";
        this.agreeTxt = "Agree";
        this.disagreeTxt = "Disagree";
        this.deleteItemTxt = "Delete the item?";
        this.deleteItemBodyTxt = "This action is irreversible";
        this.deleteTxt = "Delete";
        this.editTxt = "Edit";
        this.openSearchTxt = "Open search";
        this.barcodeOrNameTxt = "Name or Barcode";
        this.selectFileTxt = "Browse File";
        this.deleteMarketTxt = "Delete the store?";
        this.deleteMarketBodyTxt = "This action is irreversible!";
        this.confirmByTypingTxt = "Confirm by typing: ";
        this.confirmTxt = "confirm";
        this.setNewLocationTxt = "Set new location";
        this.viewMarketsTxt = "View Markets";
        this.countryTxt = "Country";
        this.cityTxt = "City";
        this.marketNameOrPhoneTxt = "Market name or phone number";
        this.noStoresAtThisMomentTxt = "No stores ath this moment";
        this.addItemToShoppingCartTxt = "Add item to shopping cart";
        this.chooseCategoryFirstTxt = "First, select a category";
        this.itemAddedSuccessfully = "The item has been added successfully.";
        this.viewCartTxt = "View shopping cart";
        this.itemRemovedSuccessfulyTxt = "Item removed";
        this.amountofItems = "Amount of items: ";
        this.generateCompketListTxt = "Generate comparison lists";
        this.youMustAddItemsToCartFirstTxt = "First, add items to your shopping cart.";
        this.listTotalPriceTxt = "Price for the shopping list:";
        this.allItemsFoundTxt = "All the items were found";
        this.allItemsNotFoundTxt = "Not all the items were found";
        this.missingItemsTxt = "Missing items:";
        this.emptyCartTxt = "Empty the shopping cart";
        this.adminPanelTxt = "Admin Panel";
        this.selectYourLanguageTxt = "Language Selection";
        this.changesWillTakeAffectOnReloadTxt = "Changes will take affect on next reload";
        this.noResultsAtmTxt = "No results";
        this.CustomerTxt = "Customer";
        this.VerifiedCustomerTxt = "Verified Customer";
        this.MarketOwnerTxt = "Market Owner";
        this.SystemAdminTxt = "System Admin";
        this.userTypeTxt = "User Type";
        this.youCantDeleteAdminTxt = "Did you try to delete a system admin? this type of action is not supported.";
        this.errCodesLang = Object.freeze({
            0: "General Server Error",
            1: "User Exists With GivenName",
            2: "User Exists With Given Email",
            3: "User Exists With Given Phone",
            4: "User Entered Invalid Email",
            5: "User Invalid Phone",
            6: "User Structure Lacks Content",
            7: "User Password Is Too Short",
            8: "Category Exists With Given Name",
            9: "CategoryDoesntExists",
            10: "Market Exists With Given Name at this address Or with that phone or the provided Phone is Empty",
            11: "User Id Doesnt Exist",
            12: "Preference Exists",
            13: "Selected Market Id Doesnt Exist for the Item",
            14: "Category Doesnt Exist for the Item",
            15: "Item Structure Invalid",
            16: "Item Doesnt Exist",
            17: "Item Exists At This Market with given name/barcode",
            18: "Wrong Credentials",
            19: "Unable to save the image",
            20: "Market doesnt exist with given id",
        });
    }
}