import { Defenitions } from "./Defenitions";
import { toast } from 'react-toastify';
import { UsedLanguage } from "./Defenitions";
import Category from "../models/Category";
import { MarketRoutes } from "./Communication";
import React from "react";
import {
  useLocation
} from "react-router-dom";

export class Helper {
  static isPasswordValid(password) {
    return (password !== "" && password.length >= Defenitions.minPasswordLength);
  }

  static makeTokenHeader(token) {
    let header = {};
    header[Defenitions.tokenKeyForHeader] = token;
    return header;
  }

  static validateEmail(email) {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  }
  /**
   * 
   * @param {*} file 
   * @returns true if the file is an image
   */
  static isFileTypeAnImage(file) {
    return (file !== null && file !== undefined && (file.type === "image/png" || file.type === "image/jpeg" || file.type === "image/jpg"));
  }
  /**
   * Turns a parameteraized string to full string
   * @param {string} string the string with params fo e.g "/test?myParam={myParam}"
   * @param {*} obj the map with param values for e.g {myParam:"123"}
   * @returns full string
   */
  static stringWithParamToFullString(string, obj) {
    var s = string;
    for (var prop in obj) {
      s = s.replace(new RegExp('{' + prop + '}', 'g'), obj[prop]);
    }
    return s;
  }
  static objectToJsonMapWithLowerCaseKeys(obj) {
    if (obj == null)
      return null;
    var key, keys = Object.keys(obj);
    var n = keys.length;
    var newObj = {}
    while (n--) {
      key = keys[n];
      newObj[key.toLowerCase()] = obj[key];
    }
    return newObj;
  }
  static async strToSha512(str) {
    return crypto.subtle.digest("SHA-512", new TextEncoder("utf-8").encode(str)).then(buf => {
      return Array.prototype.map.call(new Uint8Array(buf), x => (('00' + x.toString(16)).slice(-2))).join('');
    });
  }

  /**
   * Method is used by providing a network promise(from Communication.js)
   * @param {*} promise 
   * @returns the toast promise with data from the network result
   */
  static async createToastPromise(promise) {
    return toast.promise(
      promise,
      {
        pending: UsedLanguage.languageRef.NetworkRequestInProgress,
        success: UsedLanguage.languageRef.NetworkRequestSucceeded,
        error: {
          render({ data }) {
            // When the promise reject, data will contains the error
            return data.ServerErrCode !== undefined && data.ServerErrCode !== Defenitions.undefinedErrCode
              ? UsedLanguage.languageRef.NetworkRequestFailed +
              UsedLanguage.languageRef.errCodesLang[data.ServerErrCode] : UsedLanguage.languageRef.NetworkRequestFailed;
          }
        }
      }
      , { autoClose: Defenitions.toastDuration })
  }

  static createToast(txt, customId = null, type = 'info') {
    toast(txt, {
      autoClose: Defenitions.toastDuration,
      toastId: customId !== null ? customId : Helper.getRandomSeedForImage(), type: type
    });
  }

  static saveToLocalStorage(key, val) {
    localStorage.setItem(key, val);
  }
  static getFromLocalStorage(key) {
    return localStorage.getItem(key);
  }
  static removeFromLocalStorage(key) {
    localStorage.removeItem(key);
  }
  static saveShoppingCartToStorage(shoppingCartMap) {
    Helper.saveToLocalStorage(Defenitions.shoppingCartKeyForLocalStorage,
      JSON.stringify(shoppingCartMap));
  }

  static getShoppingCartFromStorage() {
    return JSON.parse(Helper.getFromLocalStorage(Defenitions.shoppingCartKeyForLocalStorage));
  }

  /**
   * if null is passed the token is removed.
   * @param {*} token 
   */
  static saveAccessTokenToStorage(token) {
    if (token === null)
      Helper.removeFromLocalStorage(Defenitions.accessTokenkeyForLocalStorage);
    else
      Helper.saveToLocalStorage(Defenitions.accessTokenkeyForLocalStorage, token);
  }
  /**
   * 
   * @returns null if not saved
   */
  static getAccessTokenFromStorage() {
    return Helper.getFromLocalStorage(Defenitions.accessTokenkeyForLocalStorage);
  }
  /**
   * if null is passed the token is removed.
   * @param {*} token 
   */
  static saveUserIdToStorage(userId) {
    if (userId === null)
      Helper.removeFromLocalStorage(Defenitions.userIdKeyForLocalStorage);
    else
      Helper.saveToLocalStorage(Defenitions.userIdKeyForLocalStorage, userId);
  }
  /**
   * 
   * @returns null if not saved
   */
  static getUserIdFromStorage() {
    return parseInt(Helper.getFromLocalStorage(Defenitions.userIdKeyForLocalStorage));
  }

  static saveSelectedLanguageToStorage(selectedLanguage) {
    if (selectedLanguage === null)
      Helper.removeFromLocalStorage(Defenitions.selectedLanguageKeyForLocalStorage);
    else
      Helper.saveToLocalStorage(Defenitions.selectedLanguageKeyForLocalStorage, selectedLanguage);
  }
  /**
   * 
   * @returns EN if not saved
   */
  static getSelectedLanguageFromStorage() {
    let lang = Helper.getFromLocalStorage(Defenitions.selectedLanguageKeyForLocalStorage);

    return lang !== null ? parseInt(lang) : UsedLanguage.languageType.EN;
  }

  /**
   * if we are given 0, we will get 'EN'
   * @param {} languageValue 
   * @returns 
   */
  static getLanguageKeyByValue(languageValue) {
    for (const [key, value] of Object.entries(UsedLanguage.languageType)) {
      if (value === languageValue)
        return key;
    }
    return null;
  }

  /**
   * if we are given 'EN' we will get 0
   * @param {*} languageKey 
   */
  static getLanguageValueByKey(languageKey) {
    for (const [key, value] of Object.entries(UsedLanguage.languageType)) {
      if (key === languageKey)
        return value;
    }
    return null;
  }

  /**
   * if null is passed the market id is removed.
   * @param {*} mid market id 
   */
  static saveSelectedMarketToStorage(mid) {
    if (mid === null)
      Helper.removeFromLocalStorage(Defenitions.marketIdkeyForLocalStorage);
    else
      Helper.saveToLocalStorage(Defenitions.marketIdkeyForLocalStorage, mid);
  }
  /**
   * 
   * @returns null if not saved
   */
  static getSelectedMarketFromStorage() {
    return Helper.getFromLocalStorage(Defenitions.marketIdkeyForLocalStorage);
  }
  /**
   * if null is passed the account type is removed.
   * @param {number} accountType 
   */
  static saveSAccountTypeToStorage(accountType) {
    if (accountType === null)
      Helper.removeFromLocalStorage(Defenitions.accoutTypekeyForLocalStorage);
    else
      Helper.saveToLocalStorage(Defenitions.accoutTypekeyForLocalStorage, accountType);
  }
  /**
   * 
   * @returns null if not saved
   */
  static getAccountTypeFromStorage() {
    return Helper.getFromLocalStorage(Defenitions.accoutTypekeyForLocalStorage);
  }

  /**
   * 
   * @returns true if the current account is an admin
   */
  static isSystemAdmin() {
    return parseInt(Helper.getAccountTypeFromStorage()) === Defenitions.accountType.SystemAdmin;
  }

  /**
   * turns an array of categories to array of thier names
   * @param {*} categories should look like [{name:.., desc:..},{..}]
   */
  static categoriesToCategoriesNamesArray(categories) {
    let categoryLst = []
    categories.forEach((category) => {
      categoryLst.push(new Category(category));
    });
    return categoryLst.map((option) => option.Name)
  }

  static locationsToLocationsCountryNamesArray(locations) {
    let countryLst = [""]
    for (const [key, value] of Object.entries(locations)) {
      countryLst.push(key);
    }
    return countryLst;
  }

  static locationsToLocationsCitiesNamesArray(locations, countryName) {
    for (const [key, value] of Object.entries(locations)) {
      if (key === countryName) {
        let ret = value.slice();
        ret.push("");
        return ret;
      }
    }
    return [""];
  }

  /**
   * Gets value like $ and returns the index of it and he vlaue like {index:1, value:'$'}
   * @param {string} value 
   */
  static selectedCurrencyValueToIndexAndValue(value) {
    let i = Defenitions.currency.findIndex((val) => {
      return val === value
    });
    return { index: i, value: value }
  }
  /**
   
   * @param {string} value 
   */
  static selectedCatgoryValueToIdAndValue(categoriesMap, value) {
    try {
      let i = categoriesMap.findIndex((category) => {
        return category.name === value
      });
      return { id: categoriesMap[i].id, value: value }
    }
    catch { return null; }
  }

  /**
   * Method gets categoryId and categorylst and retrieves the name
   * @param {*} categoryId 
   * @param {*} categoryLst 
   * @returns 
   */
  static getCategoryNameFromId(categoryId, categoryLst) {
    let name = '';
    for (var i = 0; i < categoryLst.length; i++) {
      if (categoryLst[i].id === categoryId) {
        name = categoryLst[i].name;
        break;
      }
    }
    return name;
  }

  static getRandomSeedForImage() {
    return performance.now().toString();
  }

  static useQuery() {
    const { search } = useLocation();

    return React.useMemo(() => new URLSearchParams(search), [search]);
  }

  static phoneNumberToCodeAndNumber(phoneNumber) {
    let code1 = phoneNumber.substring(0, 2);
    let code2 = phoneNumber.substring(0, 3);
    let code3 = phoneNumber.substring(0, 4);
    let ret = {};
    let found = false;
    let number = null;
    for (const [key, value] of Object.entries(Defenitions.numbersCodeMap)) {
      //console.log(key, value);
      if (value[0] === code1) {
        found = true;
        number = phoneNumber.substring(2);
      }
      else if (value[0] === code2) {
        found = true;
        number = phoneNumber.substring(3);
      }
      else if (value[0] === code3) {
        found = true;
        number = phoneNumber.substring(4);
      }
      if (found) {
        ret['code'] = value[0];
        ret['maxLength'] = value[1];
        ret['number'] = number;
        break;
      }
    }
    //console.log(ret);
    return ret;
  }

  static userAccountTypeToString(accountTypeIndex)
  {
    if(accountTypeIndex === Defenitions.accountType.Customer)
      return UsedLanguage.languageRef.CustomerTxt;
    else if(accountTypeIndex === Defenitions.accountType.VerifiedCustomer)
      return UsedLanguage.languageRef.VerifiedCustomerTxt;
    else if(accountTypeIndex === Defenitions.accountType.MarketOwner)
      return UsedLanguage.languageRef.MarketOwnerTxt;
    else if(accountTypeIndex === Defenitions.accountType.SystemAdmin)
      return UsedLanguage.languageRef.SystemAdminTxt;
  }
  
  static isUserTypeIndexAdmin(accountTypeIndex)
  {
    return Defenitions.accountType.SystemAdmin === accountTypeIndex;
  }

  static getUserAccountTypeKeyByIndexValue(index)
  {
    for (const [key, value] of Object.entries(Defenitions.accountType)) {
      if (value === index) {
        return key;
      }
    }
    return null;
  }

  static getUserAccountTypeValueByKey(typeKey)
  {
    for (const [key, value] of Object.entries(Defenitions.accountType)) {
      if (key === typeKey) {
        return value;
      }
    }
    return null;
  }
}