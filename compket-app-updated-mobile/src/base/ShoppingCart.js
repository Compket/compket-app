// static shopping cart data manipulation class

/**
 * the cart is saved in shared local storage
 * looks like this:
 * {<itemBarcode1>:{amount:<amount of item>,data: <item_data>}, ...}
 */

import { Helper } from "./Helper";

export default class ShoppingCart {
    static addItemToShoppingCart(item) {
        item.Price = 0;
        let cart = ShoppingCart.getShoppingCart();
        if (item.BarcodeNum.toString() in cart) {
            cart[item.BarcodeNum.toString()].amount += 1;
        }
        else {
            cart[item.BarcodeNum.toString()] = { amount: 1, data: item };
        }
        Helper.saveShoppingCartToStorage(cart);
    }

    static isCartEmpty() {
        return ShoppingCart.getCartSize() === 0;
    }

    /**
     * method returns list of items as barcodes, 
     * and an amount of each item.
     * e.g: if we have x2 tomato, x1 apple:
     * [{barcode:"131365456", amount:2}, ...]
     */
    static getListOfItemsAsBarcodes() {
        let lst = [];
        let cart = ShoppingCart.getShoppingCart();
        for (const [key, value] of Object.entries(cart)) {
            lst.push({
                Barcode: key,
                Amount: value.amount
            });
        }
        return lst;
    }

    static removeItemFromShoppingCart(itemBarcode) {
        let cart = ShoppingCart.getShoppingCart();
        if (itemBarcode.toString() in cart) {
            let amount = cart[itemBarcode.toString()].amount;
            amount--;
            cart[itemBarcode.toString()].amount = amount;
            if (amount === 0)
                delete cart[itemBarcode.toString()];
            Helper.saveShoppingCartToStorage(cart);
        }
    }

    static getCartSize() {
        let size = 0;
        let cart = ShoppingCart.getShoppingCart();
        for (const [key, value] of Object.entries(cart)) {
            size += value.amount;
        }
        return size;
    }

    static countItemInShoppingCart(itemBarcode) {
        let cart = ShoppingCart.getShoppingCart();
        if (itemBarcode.toString() in cart) {
            return cart[itemBarcode.toString()].amount;
        }
    }

    static getShoppingCart() {
        let cart = Helper.getShoppingCartFromStorage();
        return cart !== null ? cart : {};
    }

    static getCartItemsAsArray() {
        let arr = [];
        let cart = ShoppingCart.getShoppingCart();
        for (const [key, value] of Object.entries(cart)) {
            //console.log(`${key}: ${value}`);
            arr.push(value.data);
        }
        return arr;
    }

    static emptyCart() {
        let cart = ShoppingCart.getShoppingCart();
        for (const [key, value] of Object.entries(cart)) {
            delete cart[key];
        }
        Helper.saveShoppingCartToStorage(cart);
    }

    static getCartItemDataByBarcode(barcode) {
        let cart = ShoppingCart.getShoppingCart();
        let data = null;
        if (barcode.toString() in cart) {
            return cart[barcode.toString()].data;
        }
        return data;
    }

    static getCartItemsFiltered(categoryId, name = null, barcode = null) {
        let arr = [];
        let cart = ShoppingCart.getShoppingCart();
        for (const [key, value] of Object.entries(cart)) {
            if ((categoryId === null || categoryId === ""
                || value.data.CategoryId === categoryId) &&
                (name === null || name === "" ||
                    (value.data.Name.includes(name)))
                &&
                (barcode === null || barcode === "" ||
                    (value.data.BarcodeNum.includes(barcode)))) {
                arr.push(value.data);
            }
        }
        return arr;
    }
}
