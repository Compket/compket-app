﻿import { Defenitions } from '../base/Defenitions';
import Model from './Model'

export default class Picture extends Model {
    constructor({ id = Defenitions.undefinedId, name = null, extension = 0 } =
        {
            id: Defenitions.undefinedId, name: null, extension: 0
        }) {
        super(id);
        this.Name = name;
        this.Extension = extension;//a file type fom Defenitions
    }
}