﻿import { Defenitions } from '../base/Defenitions';
import Model from './Model'

export default class Item extends Model {
    constructor({ id = Defenitions.undefinedId, name = null,
        description = null, pictureid = Defenitions.undefinedId, price = 0,
        currency = 0, marketid = Defenitions.undefinedId, barcodenum = null,
        categoryid = Defenitions.undefinedId } =
        {
            id: Defenitions.undefinedId, name: null,
            description: null, pictureid: Defenitions.undefinedId, price: 0,
            currency: 0, marketid: Defenitions.undefinedId, barcodenum: null,
            categoryId: Defenitions.undefinedId
        }) {
        super({id});
        this.Name = name;
        this.Description = description;
        this.PictureId = pictureid;
        this.Price = price;
        this.Currency = currency;//enum in defenitions
        this.MarketId = marketid;
        this.BarcodeNum = barcodenum;
        this.CategoryId = categoryid;
    }
}

