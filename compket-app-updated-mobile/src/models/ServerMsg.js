export default class ServerMsg{
    constructor(httpMessageCode, serverErrCode, data, success){
        this.HttpMessageCode = httpMessageCode;
        this.ServerErrCode = serverErrCode;
        this.Data = data;
        this.Success = success;
    }
}