﻿import { Defenitions } from '../base/Defenitions';
import Model from './Model'

export default class Market extends Model {
    constructor({ id = Defenitions.undefinedId, name = null, description = null,
        pictureid = Defenitions.undefinedId, ownerid = Defenitions.undefinedId,
        addresscountry = null, addresscity = null, addressstreet = null, phonenum = null } =
        {
            id: Defenitions.undefinedId, name: null, description: null,
            pictureid: Defenitions.undefinedId, ownerid: Defenitions.undefinedId,
            addresscountry: null, addresscity: null, addressstreet : null, phonenum: null
        }) {
        super({id});
        this.Name = name;
        this.Description = description;
        this.PictureId = pictureid;
        this.OwnerId = ownerid;
        this.AddressCountry = addresscountry;
        this.AddressCity = addresscity;
        this.AddressStreet = addressstreet;
        this.PhoneNum = phonenum;
    }
}