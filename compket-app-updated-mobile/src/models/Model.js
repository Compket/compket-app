﻿import { Defenitions } from "../base/Defenitions";

export default class Model {
    constructor({id = Defenitions.undefinedId} = {id :Defenitions.undefinedId}){
        this.ID = id;
    }
}