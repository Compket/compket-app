﻿import { Defenitions } from '../base/Defenitions';
import Model from './Model'

export default class Preference extends Model {
    constructor({ id = Defenitions.undefinedId, userid = Defenitions.undefinedId,
        gotwheels = false } =
        {
            id: Defenitions.undefinedId, userid: Defenitions.undefinedId,
            gotwheels: false
        }) {
        super(id);
        this.UserId = userid;
        this.GotWheels = gotwheels;
    }
}