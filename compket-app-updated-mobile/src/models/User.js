﻿import { Defenitions } from '../base/Defenitions';
import Model from './Model'

export default class User extends Model {

    constructor({ id = Defenitions.undefinedId,
        email = null, fisrtname = null, lastname = null,
        password = null, salt = null, accounttype = 0, pictureid = Defenitions.undefinedId,
        phonenum = null } =
        {
            id: Defenitions.undefinedId,
            email: null, fisrtname: null, lastname: null,
            password: null, salt: null, accounttype: 0, pictureid: Defenitions.undefinedId,
            phonenum: null
        }) {
        super(id);
        this.Email = email;
        this.FirstName = fisrtname;
        this.LastName = lastname;
        this.Password = password;
        this.Salt = salt;
        this.AccountType = accounttype;
        this.PictureId = pictureid;
        this.PhoneNum = phonenum;
    }
}