﻿import { Defenitions } from '../base/Defenitions';
import Model from './Model'

export default class AccessToken extends Model {
    constructor({id = Defenitions.undefinedId, userid = Defenitions.undefinedId,
        token = null, prevtoken = null, renewaldate = null} =
        {id:Defenitions.undefinedId, userid :Defenitions.undefinedId,
            token : null, prevtoken : null, renewaldate : null}) {
                 
        super({id});
        this.UserId = userid;
        this.Token = token;
        this.PrevToken = prevtoken;
        this.RenewalDate = renewaldate;
    }
}
