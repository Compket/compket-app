﻿import { Defenitions } from '../base/Defenitions';
import Model from './Model'

export default class Category extends Model {
    constructor({ id = Defenitions.undefinedId, name = null, description = null,
        pictureid = Defenitions.undefinedId } =
        {
            id :Defenitions.undefinedId, name : null, description : null,
            pictureid : Defenitions.undefinedId
        }) {
        super(id);
        this.Name = name;
        this.Description = description;
        this.PictureId = pictureid;
    }
}
