import ModalUnstyled from '@mui/base/ModalUnstyled';
import { styled } from '@mui/system';
import PropTypes from 'prop-types'
import { Defenitions } from '../base/Defenitions';
import CloseIcon from '@mui/icons-material/Close';
import { IconButton } from '@mui/material';
import { Paper } from '@mui/material';

const StyledModal = styled(ModalUnstyled)`
  position: fixed;
  z-index:1000;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Backdrop = styled('div')`
  z-index: -1;
  position: fixed;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const PopupModal = (props) => {
    return (
        <StyledModal
            open={props.open}
            onClick={props.onClick}
            onClose={props.onClose}
            BackdropComponent={Backdrop}
        >
            <div style={{ marginBottom: 10, overflow: 'hidden', backgroundColor: 'white', width: Defenitions.width * 0.8, height: Defenitions.height * 0.8 }}>
                <div style={{ display: 'flex', maxHeight: Defenitions.height * 0.1, overflow: '-moz-hidden-unscrollable', justifyContent: 'start', alignContent: 'start'}}>
                <IconButton onClick={() => { props.onClose() }} style={{
                    marginRight: 10, marginLeft: 10, marginTop: 10
                }}>
                    <CloseIcon fontSize='large' />
                </IconButton>
                </div>
                <div style={{ display: 'flex', maxHeight: Defenitions.height * 0.7, overflow: 'auto', justifyContent: 'center', alignContent: 'center', paddingBottom:1 }}>
                    {props.bodyComponent}
                </div>
            </div>

        </StyledModal>
    )
}

export default PopupModal

PopupModal.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onClick: PropTypes.func,
    bodyComponent: PropTypes.element.isRequired,
}
PopupModal.defaultProps = {
    onClick: () => { }
};