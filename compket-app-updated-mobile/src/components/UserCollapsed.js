import { useState } from 'react';
import PropTypes from 'prop-types'
import { IconButton } from '@mui/material';
import { Paper } from '@mui/material';
import { Grid } from '@mui/material'
import PhotoIcon from '@mui/icons-material/Photo';
import { Defenitions, UsedLanguage } from '../base/Defenitions';
import { Typography } from '@mui/material';
import { Helper } from '../base/Helper';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import UserTypeSelection from '../components/UserTypeSelection';
import { AdminRoutes } from '../base/Communication';
import YesNoDialog from '../components/YesNoDialog';

const UserCollapsed = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    let width = props.width;
    let height = Defenitions.height * 0.18;
    return (
        <Paper elevation={4} style={{ width: width, height: height, alignContent: "center", justifyContent: "center", justifyItems: "center" }}>
            <div style={{ width: width, height: height, display: 'inline-flex' }}>
                <Grid component='main'
                    key="MarketCollapsedViewMainGrid"
                    container
                    m={0}
                    ml={0}
                    p={0}
                    pl={1}
                    overflow="clip"
                    spacing={0}
                    width={props.adminView ? width * 0.8 : width}
                    height={height}
                    justifyContent="start"
                    alignContent="center"
                    direction={"row"}
                    alignItems="center"
                >
                    <Grid
                        key="anotherGrid"
                        container
                        minHeight={height}
                        item xs={8}
                        sm={3}
                        m={0}
                        mt={0}
                        p={0}
                        overflow="hidden"
                        spacing={0}
                        height={height}
                        justifyContent="center"
                        alignContent="center"
                        direction={"column"}
                        alignItems="center"
                    >
                        <Grid item
                            key="NameGrid"
                            display="flex"
                            overflow={"hidden"}
                            alignItems="center"
                            justifyContent="center">
                            <Typography variant="body1" style={
                                {
                                    color: Defenitions.palette.primary.main,
                                    textAlign: 'center',
                                }
                            }>
                                {props.userFirstName + " " + props.userLastName}
                            </Typography>
                        </Grid>
                        <Grid item
                            key="emailGrid"
                            display="flex"
                            overflow={"hidden"}
                            alignItems="center"
                            justifyContent="center">
                            <Typography variant="body2" style={
                                {
                                    color: Defenitions.palette.primary.main,
                                    textAlign: 'center',
                                }
                            }>
                                <a href={"mailto:" + props.userEmail}>{props.userEmail}</a>
                            </Typography>
                        </Grid>
                        <Grid item
                            key="phoneGrid"
                            display="flex"
                            overflow={"auto"}
                            alignItems="center"
                            justifyContent="center">
                            <Typography variant="body2" style={
                                {
                                    color: Defenitions.palette.primary.main,
                                    textAlign: 'center',
                                    direction: 'ltr'
                                }
                            }>
                                <a href={"tel:" + props.userPhoneNumber}>{props.userPhoneNumber}</a>
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid
                        key="anotherGridExtra"
                        container
                        minHeight={height}
                        item xs={4}
                        sm={6}
                        m={0}
                        mt={1}
                        p={0}
                        overflow="hidden"
                        spacing={0}
                        height={height}
                        justifyContent="center"
                        alignContent="start"
                        direction={"column"}
                        alignItems="center"
                    >

                        <Grid item
                            key="typeGrid"
                            display="flex"
                            overflow={"hidden"}
                            alignItems="center"
                            justifyContent="center">
                            <UserTypeSelection selectedTypeIndex={props.userAccountType}
                                onChange={async (data) => {
                                    //console.log("setting type for:" + props.userId + "type:" + data);
                                    if (data !== null) {
                                        await AdminRoutes.SetUserAccessLevel(Helper.getAccessTokenFromStorage(),
                                            props.userId, data).then((res) => {
                                                //console.log(res);
                                            }, (err) => { 
                                                console.log(err);
                                            });
                                    }
                                }}
                                width={width * 0.25}
                                userId={props.userId}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                {
                    props.adminView ?
                        <div style={{ width: width * 0.2, display: 'inline-flex', justifyContent: 'center', alignContent: 'center', paddingBlock: '.95%' }}>
                            <IconButton
                                height='initial'
                                onClick={() => {
                                    //console.log("deleting" + props.userId)
                                    setIsOpen(true);
                                }}>
                                <DeleteForeverIcon />
                            </IconButton> </div>
                        : <></>
                }
                <YesNoDialog
                    open={isOpen}
                    title={UsedLanguage.languageRef.deleteTxt}
                    body={UsedLanguage.languageRef.deleteMarketBodyTxt}
                    confirmByTxt={true}
                    onResult={async (data) => {
                        setIsOpen(false);
                        if (data !== null && data === true) {
                            await Helper.createToastPromise(AdminRoutes.DeleteUser(Helper.getAccessTokenFromStorage(),
                                props.userId)).then((res) => {
                                    //console.log(res);
                                    props.onAction();
                                }, (err) => {
                                    console.log(err);
                                    Helper.createToast(UsedLanguage.languageRef.youCantDeleteAdminTxt,
                                        null, "info");
                                });
                        }
                    }}
                />
            </div>
        </Paper>
    );
};

UserCollapsed.propTypes = {
    adminView: PropTypes.bool.isRequired,
    userId: PropTypes.number,
    userEmail: PropTypes.string,
    userFirstName: PropTypes.string,
    userLastName: PropTypes.string,
    userAccountType: PropTypes.number,
    userPhoneNumber: PropTypes.string,
    width: PropTypes.number.isRequired,
    onAction: PropTypes.func,

}
UserCollapsed.defaultProps = {
    userId: Defenitions.undefinedId,
    userEmail: "<user@email.net>",
    userFirstName: "<firstName>",
    userLastName: "<lastName>",
    userAccountType: Defenitions.accountType.Customer,
    userPhoneNumber: "<phoneNumber>",
    onAction: () => { }
};
export default UserCollapsed;
