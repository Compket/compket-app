import { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Defenitions, UsedLanguage } from '../base/Defenitions'
import { Grid } from '@mui/material'
import PhotoIcon from '@mui/icons-material/Photo';
import { Typography } from '@mui/material';
import { Button } from '@mui/material'
import { MarketRoutes } from '../base/Communication';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Helper } from '../base/Helper';
import QrCode2Icon from '@mui/icons-material/QrCode2';
import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import YesNoDialog from './YesNoDialog';
import AddItem from '../pages/market/AddItem';
import Item from '../models/Item';
import PopupModal from './PopupModal';
import ItemCollapsedAccordionSummary from './ItemCollapsedAccordionSummary';

const ItemExpandableComponent = props => {
    const [src, setSrc] = useState(null);
    const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    const [openEdit, setOpenEdit] = useState(false);
    const [expanded, setExpanded] = useState(false);

    const width = props.width !== null ? props.width : Defenitions.width * 0.9;
    const height = Defenitions.height * 0.1;

    const editCloseHandler = async () => { setOpenEdit(false); (reloadfunc()).then(() => { props.onEdit() }); };

    const reloadfunc = async () => {
        await MarketRoutes.GetItemImage(Helper.getAccessTokenFromStorage(), props.itemId).then((res) => {
            if (res.Success) {
                setSrc(URL.createObjectURL(res.Data.data));
            }
        }).catch((err) => console.log("image not found", err));
    }

    useEffect(async () => {
        await reloadfunc();
    }, []);
    return (
        <div>
            <Accordion elevation={4} style={{ width: width }} expanded={expanded} onChange={() => { setExpanded(!expanded) }}>
                <ItemCollapsedAccordionSummary
                    barcodeNumber={props.barcodeNumber}
                    imageSrc={src}
                    key='summary'
                    currency={props.currency}
                    name={props.name}
                    price={props.price}
                    width={width}
                    height={height}
                />

                < AccordionDetails sx={{ p: 0 }
                }>
                    <Card sx={{ pb: 1, width: width, height: 'auto', alignContent: 'center', justifyContent: 'center' }}>
                        <CardContent>
                            <Typography variant="h6" textAlign={'center'} color={Defenitions.palette.primary.main}>
                                {props.name}
                            </Typography>
                            <Typography variant="body1" textAlign={'center'} color={Defenitions.palette.primary.main}>
                                {props.categoryName}
                            </Typography>
                            <Typography variant="body2" textAlign={'left'} color={Defenitions.palette.primary.main}>
                                {!props.isGeneric ? props.description : ""}
                            </Typography>
                        </CardContent>
                        {src === null || src === undefined ?
                            <></>
                            :
                            <CardMedia
                                onClick={() => { setOpenModal(true) }}
                                component="img"
                                style={{
                                    display: 'inline-flex', width: '100%',
                                    p: 2,
                                    m: 2,
                                    height: '35vh', objectFit: 'scale-down'
                                }}
                                image={src}
                            />
                        }
                        {props.isMarketOwner ?
                            <CardActions>
                                <Button size="small" onClick={() => { setOpenEdit(true) }}>
                                    {UsedLanguage.languageRef.editTxt}
                                </Button>
                                <Button size="small" onClick={() => { setDeleteDialogOpen(true) }}>
                                    {UsedLanguage.languageRef.deleteTxt}
                                </Button>
                            </CardActions>
                            :
                            <></>
                        }
                    </Card>
                </AccordionDetails >
            </Accordion >
            <PopupModal
                open={openModal}
                onClick={() => { setOpenModal(false) }}
                onClose={() => { setOpenModal(false) }}
                bodyComponent={(<img src={src} style={{ objectFit: 'scale-down', maxWidth: '100%', maxHeight: '100%', padding: 20 }}
                />)}
            />
            <PopupModal
                open={openEdit}
                onClose={editCloseHandler}
                bodyComponent={(<AddItem editItemMode={true} itemImgSrc={src !== null && src !== undefined ? src : null}
                    editDoneFunc={editCloseHandler}
                    itemModel={new Item({
                        id: props.itemId, name: props.name,
                        description: props.description, price: props.price,
                        currency: props.currency, marketid: props.marketId, barcodenum: props.barcodeNumber,
                        categoryId: props.categoryId
                    })} />)}
            />
            <YesNoDialog key={'dialog' + props.itemId}
                title={UsedLanguage.languageRef.deleteItemTxt}
                body={UsedLanguage.languageRef.deleteItemBodyTxt}
                open={deleteDialogOpen}
                onResult={async (data) => {
                    setDeleteDialogOpen(false);
                    if (data === true) {
                        await Helper.createToastPromise(MarketRoutes.DeleteItem(Helper.getAccessTokenFromStorage(),
                            Helper.getSelectedMarketFromStorage(), props.itemId)).then(
                                (res) => {
                                    props.onDelete();//to tell the parent we successfuly deleted.
                                },
                                (err) => { console.log(err) });
                    }
                }} />
        </div >
    )
}

ItemExpandableComponent.propTypes = {
    itemId: PropTypes.number.isRequired,
    barcodeNumber: PropTypes.string,
    categoryName: PropTypes.string,
    currency: PropTypes.number,
    description: PropTypes.string,
    marketId: PropTypes.number,
    name: PropTypes.string,
    price: PropTypes.number,
    onDelete: PropTypes.func.isRequired,//this is used to notify parent of successful deletion!
    onEdit: PropTypes.func.isRequired,//this is used to notify parent on finish edit!
    isMarketOwner: PropTypes.bool,
    isGeneric: PropTypes.bool,
    width: PropTypes.number
}
ItemExpandableComponent.defaultProps = {
    itemId: null,
    barcodeNumber: "<barcode>",
    categoryName: "<category>",
    currency: 0,
    description: "<description>",
    marketId: null,
    name: "<name>",
    price: 0.0,
    isMarketOwner: true,
    isGeneric: false,
    width: null
    //onClick: (() => { }),
};
export default ItemExpandableComponent

