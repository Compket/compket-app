import React from 'react';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types'
import { styled } from '@mui/material/styles';
import MarketCollapsedView from '../components/MarketCollapsedView'
import { Defenitions, UsedLanguage } from '../base/Defenitions';
import { List, ListItem } from '@mui/material';
import Item from '../models/Item';
import ItemExpandableComponent from './ItemExpandableComponent';
import { Helper } from '../base/Helper';
import ShoppingCart from '../base/ShoppingCart';



const GeneratedItemsList = (props) => {
    //console.log(props.listData);
    let market = props.listData.market;
    let listData = props.listData;
    let innerWidth = 0.9 * props.width;
    return (<div>
        <MuiAccordion style={{ width: props.width }} disableGutters elevation={1} square  >
            <MuiAccordionSummary
                style={{ width: props.width, justifyContent: 'center' }}
                expandIcon={<ArrowForwardIosSharpIcon
                    style={{ marginInline: 5 }}
                    sx={{ fontSize: '0.9rem' }} />}
            >
                <MarketCollapsedView
                    description={market.description}
                    onDeleteOrEdit={null}
                    width={innerWidth}
                    linkTo=''
                    marketOwnerMode={false}
                    mid={market.id} key={market.id} addressCity={market.addresscity} addressCountry={market.addresscountry}
                    addressStreet={market.addressstreet} name={market.name} phoneNumber={market.phonenum} />
            </MuiAccordionSummary>
            <MuiAccordionDetails>
                <div style={{ display: 'grid', justifyContent: 'center' }}>
                    <Typography variant='h6'
                        textAlign={'center'}
                        color={Defenitions.palette.primary.main}>
                        {listData.allItemsFound ?
                            UsedLanguage.languageRef.allItemsFoundTxt :
                            UsedLanguage.languageRef.allItemsNotFoundTxt
                        }
                    </Typography>
                    <Typography variant='h6'
                        textAlign={'center'}
                        color={Defenitions.palette.primary.main}>
                        {UsedLanguage.languageRef.listTotalPriceTxt + " " +
                            listData.totalPrice.toFixed(2)}
                    </Typography>
                    <List>
                        {
                            listData.foundItems.map((currData, index) => {
                                //console.log(currData);
                                let item = new Item(currData.item);
                                let categoryName = Helper.getCategoryNameFromId(item.CategoryId, props.categoryLst);
                                let borderSpacingPx = 5;
                                let lstItem = (
                                    <ListItem key={'lstItem' + index}
                                        style={{ justifyContent: "center", maxWidth: Defenitions.width }}>
                                        <div style={{ display: 'table', borderSpacing: borderSpacingPx, verticalAlign: 'middle' }}>
                                            <div style={{ display: 'table-cell', verticalAlign: 'middle' }}>
                                                <Typography variant="h6" textAlign={'center'} style={{ color: Defenitions.palette.primary.main }}>
                                                    {"X" + currData.amount}
                                                </Typography>
                                            </div>
                                            <div style={{ display: 'table-cell', verticalAlign: 'middle' }}>
                                                <ItemExpandableComponent itemId={item.ID} price={item.Price} name={item.Name}
                                                    onDelete={() => { }}
                                                    width={innerWidth * 0.95}
                                                    onEdit={() => { }}
                                                    isGeneric={false}
                                                    isMarketOwner={false}
                                                    categoryName={categoryName}
                                                    barcodeNumber={item.BarcodeNum}
                                                    description={item.Description} categoryId={item.CategoryId} currency={item.Currency}
                                                    marketId={item.MarketId} />
                                            </div>
                                        </div>
                                    </ListItem>);
                                return lstItem;
                            })
                        }
                        <ListItem key={'lstItemTxtitem'}
                            style={{ justifyContent: "center", maxWidth: Defenitions.width }}>
                            <Typography variant='h6'
                                textAlign={'center'}
                                color={Defenitions.palette.primary.main}>
                                {listData.allItemsFound ?
                                    "" :
                                    UsedLanguage.languageRef.missingItemsTxt
                                }
                            </Typography>
                        </ListItem>
                        {
                            listData.allItemsFound ? <></> :
                                (

                                    listData.missingItems.map((currData, index) => {
                                        //console.log(currData);
                                        let item = currData;
                                        let categoryName = Helper.getCategoryNameFromId(item.CategoryId, props.categoryLst);
                                        let borderSpacingPx = 5;
                                        let lstItem = (
                                            <ListItem key={'lstItem' + index}
                                                style={{ justifyContent: "center", maxWidth: Defenitions.width }}>
                                                <div style={{ display: 'table', borderSpacing: borderSpacingPx, verticalAlign: 'middle' }}>
                                                    <div style={{ display: 'table-cell', verticalAlign: 'middle' }}>
                                                        <Typography variant="h6" textAlign={'center'} style={{ color: Defenitions.palette.primary.main }}>
                                                            {"X" + ShoppingCart.countItemInShoppingCart(currData.BarcodeNum)}
                                                        </Typography>
                                                    </div>
                                                    <div style={{ display: 'table-cell', verticalAlign: 'middle' }}>
                                                        <ItemExpandableComponent itemId={item.ID} price={item.Price} name={item.Name}
                                                            onDelete={() => { }}
                                                            width={innerWidth * 0.95}
                                                            onEdit={() => { }}
                                                            isGeneric={true}
                                                            isMarketOwner={false}
                                                            categoryName={categoryName}
                                                            barcodeNumber={item.BarcodeNum}
                                                            description={item.Description} categoryId={item.CategoryId} currency={item.Currency}
                                                            marketId={item.MarketId} />
                                                    </div>
                                                </div>
                                            </ListItem>);
                                        return lstItem;
                                    }))
                        }
                    </List>
                </div>
            </MuiAccordionDetails>
        </MuiAccordion>
    </div >);
};


GeneratedItemsList.propTypes = {
    listData: PropTypes.object.isRequired,
    width: PropTypes.number.isRequired,
    categoryLst: PropTypes.array.isRequired

}
GeneratedItemsList.defaultProps = {
    listData: null,
};

export default GeneratedItemsList;
