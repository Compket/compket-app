// MyGoogleMaps.js
import React, { Component } from 'react';

import GoogleMapReact from 'google-map-react';

import styled from 'styled-components';
import PropTypes from 'prop-types';
import AutoComplete from '../components/AutoComplete';
import Marker from './Marker';
import { UsedLanguage } from '../base/Defenitions';

const Wrapper = styled.main`
  width: 100%;
  height: 100%;
`;

class MyGoogleMap extends Component {


    state = {
        mapApiLoaded: false,
        mapInstance: null,
        mapApi: null,
        geoCoder: null,
        places: [],
        center: [32.08, 34.78],
        lat: 32.08,
        lng: 34.78,
        zoom: 14,
        address: '',
        draggable: true,
    };

    componentDidMount() {
        this.setCurrentLocation();
    }


    onMarkerInteraction = (childKey, childProps, mouse) => {
        this.setState({
            draggable: false,
            lat: mouse.lat,
            lng: mouse.lng
        });
    }
    onMarkerInteractionMouseUp = (childKey, childProps, mouse) => {
        this.setState({ draggable: true });
        this._generateAddress();
    }

    _onChange = ({ center, zoom }) => {
        this.setState({
            center: center,
            zoom: zoom,
        });

    }

    _onClick = (value) => {
        this.setState({
            lat: value.lat,
            lng: value.lng
        });
    }

    apiHasLoaded = (map, maps) => {
        this.setState({
            mapApiLoaded: true,
            mapInstance: map,
            mapApi: maps,
        });

        this._generateAddress();
    };

    _handleChanged(place) {
        try{
            //console.log(place.address_components)
            let street = place.address_components[place.address_components.length === 6 ? 1 : 0].long_name;
            street += place.address_components.length === 6 ?
                place.address_components[0].long_name : " ";
            this.props.onLocationSelected(place.geometry.location.lat(),
                place.geometry.location.lng(),
                place.address_components[place.address_components.length === 6 ? 5 : 4].long_name,
                place.address_components[place.address_components.length === 6 ? 2 : 1].long_name,
                street
            );
        }
        catch(err){console.log(err)}
    }

    addPlace = (place) => {
        this.setState({
            places: [place],
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
        });

        //this._generateAddress()
        if (this.props.onLocationSelected != null) {
            this._handleChanged(place);
        }
    };

    _generateAddress() {
        const {
            mapApi
        } = this.state;

        const geocoder = new mapApi.Geocoder;

        geocoder.geocode({ 'location': { lat: this.state.lat, lng: this.state.lng } }, (results, status) => {
            if (status === 'OK') {
                if (results[0]) {
                    this.zoom = 12;
                    this.setState({
                        address: results[0].formatted_address,
                        lat: results[0].geometry.location.lat(),
                        lng: results[0].geometry.location.lng()
                    });
                    if (this.props.onLocationSelected != null) {
                        this._handleChanged(results[0]);
                    }
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }

        });
    }

    // Get Current Location Coordinates
    setCurrentLocation() {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.setState({
                    center: [position.coords.latitude, position.coords.longitude],
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                }, ()=>{
                    this._generateAddress();
                });
            }, () => {
                this.setState({
                    center: [32.08, 34.78],
                    lat: 32.08,
                    lng: 34.78
                });
            });
        }
    }

    render() {
        const {
            places, mapApiLoaded, mapInstance, mapApi,
        } = this.state;


        return (
            <Wrapper>
                {mapApiLoaded && (
                    <div>
                        <AutoComplete map={mapInstance} mapApi={mapApi} addplace={this.addPlace} />
                    </div>
                )}

                <GoogleMapReact
                    center={this.state.center}
                    zoom={this.state.zoom}
                    draggable={this.state.draggable}
                    onChange={this._onChange}
                    onChildMouseDown={this.onMarkerInteraction}
                    onChildMouseUp={this.onMarkerInteractionMouseUp}
                    onChildMouseMove={this.onMarkerInteraction}
                    onChildClick={() => {}}
                    onClick={this._onClick}
                    bootstrapURLKeys={{
                        key: 'AIzaSyBFL-cEIFOzq6kMUGLpf9yGcDHovZjgWsk',
                        libraries: ['places', 'geometry'],
                        language: UsedLanguage.languageRef.langCode
                    }}
                    yesIWantToUseGoogleMapApiInternals
                    onGoogleApiLoaded={({ map, maps }) => this.apiHasLoaded(map, maps)}
                >

                    <Marker
                        text={this.state.address}
                        lat={this.state.lat}
                        lng={this.state.lng}
                    />


                </GoogleMapReact>

            </Wrapper >
        );
    }
}

export default MyGoogleMap;

MyGoogleMap.defaultProps = {
    onLocationSelected: null,
};

MyGoogleMap.propTypes = {
    onLocationSelected: PropTypes.func,
};
