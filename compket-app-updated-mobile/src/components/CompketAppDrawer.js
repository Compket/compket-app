import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Grid from '@mui/material/Grid';
import { createTheme } from '@material-ui/core/styles';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import 'bootstrap/dist/css/bootstrap.min.css';
import { MuiThemeProvider } from '@material-ui/core/styles';
import BodyDirectionHandler from './BodyDirectionHandler';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import { Defenitions } from '../base/Defenitions';
import { AppBar } from '@mui/material';
import { Box } from '@mui/material';
import { Link } from 'react-router-dom';

const drawerWidth = 240;
let theme = Defenitions.getTheme()
const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-start'
}));


export default function CompketAppDrawer(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const MenuBtn = () => {
    return (
      <div className="col" style={{ "display": "flex" }}>
        <IconButton onClick={handleDrawerToggle} style={{
          justifyContent:
            "flex-start"
        }}

          color="inherit" aria-label="menu" sx={{ mr: 0 }}>
          <MenuIcon />
        </IconButton>
      </div>
    )
  }
  const LinkBtn = () => {
    return (
      <div className="col" style={{
        display: "flex", justifyContent:
          "flex-end",
        alignItems: "center"
      }}>
        <Link to={props.buttonLink} style={{ textDecoration: 'none', color: "inherit" }}>
          <Button style={{textTransform: "none"}}
            color="secondary">{props.buttonText}</Button>
        </Link>
      </div>
    )
  };
  const Lst = () => {
    return props.drawerItems
  };
  const drawer = (
    <div>
      <DrawerHeader>
        <IconButton onClick={handleDrawerToggle}>
          {Defenitions.ltrScreenDirection ? <ChevronLeftIcon /> : <ChevronRightIcon />}
        </IconButton>
      </DrawerHeader>
      <Divider />
      <div onClick={handleDrawerToggle}>
        <Lst />
      </div>
    </div>
  );
  const container = window !== undefined ? () => window().document.body : undefined;
  console.log("ltr:", Defenitions.ltrScreenDirection);
  const styleOptions2 = ({ backgroundColor: theme.palette.info.main, minHeight: '100%' });
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="static" color="primary" style={{ minHeight: '5vh' }}>
        <Toolbar variant="dense" style={{
          display: "flex",
          backgroundColor: "primary",
          justifyContent: "space-around"
        }}>

          <Grid container justifyContent="space-evenly" spacing={0}>
            <Grid key="menuBtnG" item xs={4} container
              direction="column"
              alignItems="flex-start"
              justifyContent="flex-start">
              <MenuBtn />
            </Grid>
            <Grid key="nameG" item xs={4} container
              direction="column"
              alignItems="center"
              justifyContent="center">
              <Grid key="nameGG" item xs={4} container
                direction="row"
                alignItems="center"
                justifyContent="center">
                <Typography variant="h6" color="inherit" component="div" style={{
                  verticalAlign: "middle",
                  "flex": 1
                }} >
                  {Defenitions.appName}
                </Typography>
              </Grid>
            </Grid>
            <Grid key="linkBtnG" item xs={4} container
              direction="column"
              alignItems="flex-end"
              justifyContent="flex-end">
              <LinkBtn />
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <BodyDirectionHandler fromKey="compketAppDrawerTemporary" bodyContent={(
        <Drawer
          variant="temporary"
          anchor="top"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          <div style={styleOptions2}>
            {drawer}
          </div>
        </Drawer>
      )} />
      <BodyDirectionHandler fromKey="compketAppDrawerPersistant" bodyContent={(
        <Drawer
          variant="persistent"
          sx={{
            display: { xs: 'none', sm: 'block' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
          anchor="top"
          open={mobileOpen}
          onClose={handleDrawerToggle}
        >
          <div style={styleOptions2}>
            {drawer}
          </div>
        </Drawer>
      )} />
    </Box>
  );
}

CompketAppDrawer.propTypes = {
  buttonEnabled: PropTypes.bool.isRequired,
  buttonText: PropTypes.string,
  buttonLink: PropTypes.string,
  drawerItems: PropTypes.element
};

CompketAppDrawer.defaultProps = {
  buttonEnabled: true,
  buttonText: null,
  buttonLink: "/",
  drawerItems: <div />
}
