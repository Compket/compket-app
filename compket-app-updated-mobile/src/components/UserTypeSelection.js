import React from 'react';
import Box from '@mui/material/Box';
import PropTypes from 'prop-types'
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import { Defenitions, UsedLanguage } from '../base/Defenitions';
import { Helper } from '../base/Helper';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { makeStyles } from '@mui/styles';
const useStyles = makeStyles({
    select: {
        '&:before': {
            borderColor: Defenitions.palette.primary.main,
        },
        '&:after': {
            borderColor: Defenitions.palette.primary.main,
        },
        '&:not(.Mui-disabled):hover::before': {
            borderColor: Defenitions.palette.primary.main,
        },
    },
    icon: {
        fill: Defenitions.palette.primary.main,
    },
    root: {
        color: Defenitions.palette.primary.main,
    },
})

const UserTypeSelection = (props) => {
    const classes = useStyles()
    const [type, setType] = React.useState(Helper.getUserAccountTypeKeyByIndexValue(props.selectedTypeIndex));
    const [currIndex, setIndex] = React.useState(props.selectedTypeIndex);
    const handleChange = (event) => {
        setType(event.target.value);
        let index = Helper.getUserAccountTypeValueByKey(event.target.value);
        props.onChange(index);
        setIndex(index);
    };

    const getOptions = () => {
        let arrRet = [];
        for (const [key, value] of Object.entries(Defenitions.accountType)) {
            let translatedKey = Helper.userAccountTypeToString(value);
            arrRet.push(
                (<MenuItem key={key} value={key}>
                    {translatedKey}
                </MenuItem>)
            );
        }

        return arrRet;
    }
    let disabled = Helper.isUserTypeIndexAdmin(currIndex) &&
        props.userId === Helper.getUserIdFromStorage();
    //console.log(disabled, props.userId, Helper.getUserIdFromStorage());
    //IconComponent: (props) => (<ArrowDropDownIcon props={props} color="primary" />) 
    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off"
        >
            <div>
                <TextField
                    id="outlined-select-currency"
                    style={{ width: props.width }}
                    select
                    disabled={disabled}
                    SelectProps={{
                        className: classes.select, inputProps: {
                            classes: {
                                icon: classes.icon,
                                root: classes.root,
                            },
                        }
                    }}
                    focused
                    variant="standard"
                    label={UsedLanguage.languageRef.userTypeTxt}
                    value={type}
                    onChange={handleChange}
                >
                    {
                        getOptions()
                    }
                </TextField>
            </div>
        </Box>
    );
};

UserTypeSelection.propTypes = {
    selectedTypeIndex: PropTypes.number.isRequired,
    userId: PropTypes.number.isRequired,
    onChange: PropTypes.func,
    width: PropTypes.number,

}
UserTypeSelection.defaultProps = {
    onChange: () => { },
    width: 120
};
export default UserTypeSelection;
