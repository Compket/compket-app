import React from 'react'
import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider } from "@emotion/react";
import createCache from "@emotion/cache";
import { Defenitions } from '../base/Defenitions';
import { ThemeProvider } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const cacheLtr = createCache({
    key: "muiltr"
});

const cacheRtl = createCache({
    key: "muirtl",
    stylisPlugins: [rtlPlugin]
});

const BodyDirectionHandler = (props) => {
    const [isRtl, setIsRtl] = React.useState(!Defenitions.ltrScreenDirection);
    const [value, setValue] = React.useState("initial value");
    React.useLayoutEffect(() => {
        document.body.setAttribute("dir", isRtl ? "rtl" : "ltr");
    }, [isRtl]);
    const Content = () => {
        return (<div key="bodyDirectionHandlerDivContentWrapper">{props.bodyContent}</div>);
    };
    const theme = Defenitions.getTheme();
    return (
        <CacheProvider key={props.fromKey + "cacheProvider"} value={isRtl ? cacheRtl : cacheLtr}>
            <ThemeProvider key={props.fromKey + "themeProvider"} theme={theme}>
                {Content()}
            </ThemeProvider>
        </CacheProvider>
    )
}
BodyDirectionHandler.propTypes = {
    bodyContent: PropTypes.element.isRequired,
    fromKey: PropTypes.string.isRequired
};

BodyDirectionHandler.defaultProps = {
    bodyContent: <div />,
    fromKey: null
}
export default BodyDirectionHandler
