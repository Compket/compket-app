import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import { Defenitions, UsedLanguage } from '../base/Defenitions';
import { makeStyles } from '@mui/styles';
import { TextField } from '@mui/material';
import { MenuItem } from '@mui/material';
import { Helper } from '../base/Helper';

const CurrencySelection = (props) => {
    const [currency, setCurrency] = useState(props.defaultVal);
    const useStyles = makeStyles({
        icon: {
            color: Defenitions.palette.primary.main,
        },
    });
    useEffect(()=>props.onChange(Helper.selectedCurrencyValueToIndexAndValue(currency)),[])
    return (
        <TextField
            id="filled-select-currency"
            select
            required
            label={UsedLanguage.languageRef.currencyTxt}
            value={currency}
            style={{ alignContent: 'end' }}
            SelectProps={{
                classes: { icon: useStyles().icon },
            }}
            sx={{width:props.width}}
            onChange={(event) => { setCurrency(event.target.value); 
                props.onChange(Helper.selectedCurrencyValueToIndexAndValue(event.target.value));}}
            focused
            variant="standard"
        >
            {Defenitions.currency.map((option, index) => {
                return (
                    <MenuItem key={index} value={option}>
                        {option}
                    </MenuItem>
                )
            })}
        </TextField>
    )
}

export default CurrencySelection

CurrencySelection.propTypes = {
    onChange: PropTypes.func,//returns the selected index and value
    width: PropTypes.number.isRequired,
    defaultVal: PropTypes.string,
}
CurrencySelection.defaultProps = {
    onChange: (() => { }),
    defaultVal: Defenitions.currency[0]
};