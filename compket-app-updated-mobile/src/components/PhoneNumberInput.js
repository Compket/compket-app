import NativeSelect from '@mui/material/NativeSelect';
import { TextField } from '@mui/material'
import React, { Component } from 'react'
import { createStyles, makeStyles } from '@mui/styles';
import { Defenitions, UsedLanguage } from '../base/Defenitions';
import PropTypes from 'prop-types';
import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import Button from '@mui/material/Button';
import CloseIcon from '@mui/icons-material/Close';
import { Helper } from '../base/Helper';

const theme = Defenitions.getTheme();
const useStyles = makeStyles(() =>
    createStyles({
        select: {
            '&:before': {
                borderBottom: "2px solid" + theme.palette.primary.main,
            },
            '&:after': {
                borderBottom: "2px solid" + theme.palette.primary.main,
            },
        },
        icon: {
            fill: theme.palette.primary.main,
        },
    }),
);

function withMyHook(Component) {
    return function WrappedComponent(props) {
        const myHookValue = useStyles();
        return <Component key={props.specialKey + "innerInput"} {...props} myHookValue={myHookValue} />;
    }
}

withMyHook.propTypes = {
    specialKey: PropTypes.string.isRequired
}

const undefinedLength = 0;

const selectionMap = Defenitions.numbersCodeMap;
class PhoneNumberInput extends Component {
    static selfRef;
    selections;
    Selections() {
        let components = [];
        for (const [key, value] of Object.entries(selectionMap)) {
            components.push(<option key={"selction" + key} value={value[0]}>{key}</option>)
        }
        return components;
    }
    constructor(props) {
        super(props);
        this.state = {
            openErr: false, open: false, codeValue: "",
            numValue: "", value: "", maxLength: undefinedLength
        };
        this.defaultCode = "";
        this.defaultPhoneValue = "";
        if(this.props.defaultPhoneNumber !== null)
        {
            let res = Helper.phoneNumberToCodeAndNumber(this.props.defaultPhoneNumber);
            this.defaultCode = res.code;
            this.defaultPhoneValue = res.number;
            this.state.codeValue = this.defaultCode;
            this.state.numValue = this.defaultPhoneValue;
            this.state.maxLength = res.maxLength;
            this.state.value = this.props.defaultPhoneNumber;
            //console.log(this.defaultCode, this.defaultPhoneValue, this.state.maxLength);
        }
        this.handleCodeChange = this.handleCodeChange.bind(this);
        this.handleNumChange = this.handleNumChange.bind(this);
        // This binding is necessary to make `this` work in the callback    this.handleClick = this.handleClick.bind(this);  }
        this.selections = this.Selections();
    }
    getTemplateByCode(selectionCode) {
        let templateCode = "";
        for (const [key, value] of Object.entries(selectionMap)) {
            if (value[0] === selectionCode) {
                templateCode = value[2]
                break;
            }
        }
        return templateCode;
    }
    getLengthByCode(selectedCode) {
        let length = undefinedLength;
        for (const [key, value] of Object.entries(selectionMap)) {
            if (value[0] === selectedCode) {
                length = value[1]
                break;
            }
        }
        return length;
    }
    handleCodeChange(e) {
        let codeVal = e.target.value;
        let maxLength = this.getLengthByCode(codeVal);
        let numValue = "";
        this.setState((state) => ({
            codeValue: codeVal,
            maxLength: maxLength,
            numValue: numValue, value: codeVal
        }), () => {
            if (this.props.onChangeEvent != null)
                this.props.onChangeEvent(this.state);
        });
    }
    handleNumChange(e) {
        if (e.target.value.startsWith("0")) {
            this.setState({
                numValue: ""
            }, () => { this.setOpenErr(true); });
        }
        else {
            let numValue = e.target.value;
            numValue = e.target.value.replace(/[^0-9]/g, '');
            if(e.target.value.length > this.state.maxLength)
            {
                numValue = numValue.substring(0, this.state.maxLength);
            }
            if (PhoneNumberInput.selfRef.state.openErr)
                PhoneNumberInput.selfRef.setOpenErr(false);
            this.setState({
                codeValue: this.state.codeValue,
                maxLength: this.state.maxLength,
                numValue: numValue, value: this.state.codeValue + numValue
            }, () => {
                if (this.props.onChangeEvent != null)
                    this.props.onChangeEvent(this.state);
            });
        }
    }
    handleNumOnFocus(e) {
        if (PhoneNumberInput.selfRef.state.openErr)
            PhoneNumberInput.selfRef.setOpenErr(false);
        if (PhoneNumberInput.selfRef.state.maxLength === 0)
            PhoneNumberInput.selfRef.setOpen(true);
    }
    handleSelectOnFocus(e) {
        if (PhoneNumberInput.selfRef.state.open)
            PhoneNumberInput.selfRef.setOpen(false);
    }
    setOpen(e) {
        this.setState((state) => ({ open: e }));
    }
    setOpenErr(e) {
        this.setState((state) => ({ openErr: e }));
    }
    SelectComp() {
        const classes = this.props.myHookValue;

        /*defaultValue={this.props.codeValue != null ? this.props.codeValue : this.state.codeValue}*/
        return (<NativeSelect
            color="primary"
            key="selectComp"
            onChange={this.handleCodeChange}
            defaultValue={this.defaultCode}
            className={classes.select}
            onFocus={this.handleSelectOnFocus}
            inputProps={{
                name: 'code',
                id: 'uncontrolled-native',
                classes: {
                    icon: classes.icon,
                    border: classes.border
                },
            }}
        >
            {this.selections}
        </NativeSelect>);
    };
    NumComp() {
        return (<TextField label={UsedLanguage.languageRef.PhoneNumTxt}
            variant="standard"
            required
            key="numComp"
            inputProps={{
                maxLength: this.state.maxLength,
                type:"number"
            }}
            value={this.state.numValue}
            color="primary"
            dir="LTR"
            placeholder={this.getTemplateByCode(this.state.codeValue)}
            onFocus={this.handleNumOnFocus}
            onChange={this.handleNumChange}
            style={{ marginLeft: "1%" }}
            focused />);
    };
    render() {
        PhoneNumberInput.selfRef = this;
        return (
            <div key="numInputDiv">
                <Collapse key="collapseTop" in={this.state.open}>
                    <Alert
                        severity="error"
                        action={
                            <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    this.setOpen(false);
                                }}
                            >
                                <CloseIcon fontSize="inherit" />
                            </IconButton>
                        }
                        sx={{ mb: 2 }}
                    >
                        {UsedLanguage.languageRef.MustChooseCountryCodeFirstTxt}
                    </Alert>
                </Collapse>
                <Collapse key="collapseSecond" in={this.state.openErr}>
                    <Alert
                        severity="error"
                        action={
                            <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    this.setOpenErr(false);
                                }}
                            >
                                <CloseIcon fontSize="inherit" />
                            </IconButton>
                        }
                        sx={{ mb: 2 }}
                    >
                        {UsedLanguage.languageRef.MustNotUseZeroAtBeginningOfNumTxt}
                    </Alert>
                </Collapse>
                <div key="phoneNumInputInnerDiv" style={{ justifyContent: "center", "display": "flex", "flexDirection": "row" }}>
                    {this.props.directionLTR ? this.SelectComp() : this.NumComp()}
                    {this.props.directionLTR ? this.NumComp() : this.SelectComp()}
                </div>
            </div>
        );
    }
}


PhoneNumberInput.propTypes = {
    directionLTR: PropTypes.bool.isRequired,
    onChangeEvent: PropTypes.func,
    defaultPhoneNumber : PropTypes.string,
};

PhoneNumberInput.defaultProps = {
    directionLTR: true,//so that if we provide component with false(means that we are in rtl)
    onChangeEvent: null,
    defaultPhoneNumber : null,
}

export default withMyHook(PhoneNumberInput);