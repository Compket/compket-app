import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@mui/material';
import { UsedLanguage } from '../base/Defenitions';
import { Grid } from '@mui/material';
import { Helper } from '../base/Helper';
import InsertPhotoIcon from '@mui/icons-material/InsertPhoto';
import { Typography } from '@mui/material';
import { Defenitions } from '../base/Defenitions';
const FileSelection = (props) => {

	const [selectedFile, setSelectedFile] = useState();
	const [firstStart, setFirstStart] = useState(true);

	const [isFilePicked, setIsFilePicked] = useState(false);

	const [image, setImage] = useState("");


	const changeHandler = (event) => {
		_clearSelection();
		let sFile = event.target.files[0];
		/*console.log(event.target);
		console.log(event.target.files);
		console.log(event.target.files[0]);
		debugger*/
		setSelectedFile(sFile);
		setIsFilePicked(true);
		setFirstStart(false);
		if(sFile.lastModifiedDate === null || sFile.lastModifiedDate === undefined)
			sFile.lastModifiedDate = new Date(sFile.lastModified);
		if (event.target.files && event.target.files[0] && Helper.isFileTypeAnImage(sFile)) {
			setImage(URL.createObjectURL(event.target.files[0]));
			/*console.log(event.target.files[0]);
			console.log(URL.createObjectURL(event.target.files[0]));*/
		}
		//console.log(sFile);
		if (props.handlerParentFunc !== null)
			props.handlerParentFunc(sFile);
	};

	const _clearSelection = () => {
		setSelectedFile(null);
		setIsFilePicked(false);
		setImage(null);
		if (props.handlerParentFunc !== null)
			props.handlerParentFunc(null);
	}

	const handleBtnClick = () => {
		_clearSelection();
		document.getElementById('selectedFile').value = null;
	}

	const fileDetails = () => {
		return isFilePicked ? (<div>

			<p>Filename: {selectedFile.name}</p>
			<p>Filetype: {selectedFile.type}</p>
			<p>Size in bytes: {selectedFile.size}</p>
			<p>
				lastModifiedDate:{' '}
				{selectedFile.lastModifiedDate?.toLocaleDateString()}
			</p>
		</div>)
			:
			<p>{UsedLanguage.languageRef.selectFileTxt}</p>
	};


	/**https://www.pluralsight.com/guides/uploading-files-with-reactjs */

	const myRef = useRef(null)
	useEffect(() => {
		if(!firstStart && selectedFile !== "" && selectedFile !== null)
		{
			myRef.current.scrollIntoView({
				block: "end",
				inline: "end",
				behavior: "auto",
				alignToTop: true
			  });
		}
		if(image === '' && props.existingSrc !== null)
		{
			setImage(props.existingSrc);
		}
	}, [selectedFile]);
	return (

		<Grid component='main'
			key="fileSelectionGrid"
			container
			columns={1}
			spacing={0}
			justifyContent="space-evenly"
			alignItems="top"
		>
			<Grid item xs={4} container
                mt={1}
                ml={1}
                mr={1}
                direction="column"
                key="titleSelectionGrid"
                alignItems="center"
                justifyContent="center">
                <Typography variant="h6" component="h6" style={
                        {
                            color: Defenitions.palette.primary.main,
                        }
                    }>
                    {UsedLanguage.languageRef.selectAnImageTxt }
                </Typography>
            </Grid>
			<Grid item xs={4} container
				mt={1}
				key="imageGrid"
				direction="column"
				alignItems="center"
				height="25vh"
				justifyContent="center">
				{props.componentMentForImage ? 
				(image !== "" && image !== undefined && image !== null ?
					<img id="target" src={image} style={{
					maxWidth: '80%',
					maxHeight: '100%'
				}} /> :<InsertPhotoIcon fontSize='large'/>) 
				: fileDetails()}
			</Grid>
			<Grid item xs={4} container
				mt={1}
				ml={1}
				mr={1}
				key="inputFileGrid"
				direction="column"
				alignItems="center"
				maxHeight='10vh'
				justifyContent="center">
				<input type="file" id="selectedFile" name="file" onChange={changeHandler} style={{display: 'none'}} />
			</Grid>
			<Grid item xs={4} container
				mt={1}
				key="actualBtnGrid"
				direction="column"
				alignItems="center"
				maxHeight='10vh'
				justifyContent="center">
				<Button color="primary"  key="actualBtn" variant="contained" value="Browse..." onClick={()=>{document.getElementById('selectedFile').click();}}>{UsedLanguage.languageRef.selectFileTxt}</Button>
			</Grid>
			<Grid item xs={4} container
				mt={1}
				key="clearGrid"
				direction="column"
				alignItems="center"
				maxHeight='10vh'
				justifyContent="center">
				<Button color="primary" key="clearBtn" variant="contained" onClick={handleBtnClick}>{UsedLanguage.languageRef.ClearSelectionTxt}</Button>
			</Grid>
			<Grid>
				<div ref={myRef}/>
			</Grid>
		</Grid >

	);
}

export default FileSelection;

FileSelection.defaultProps = {
	handlerParentFunc: null,
	componentMentForImage: false,
	existingSrc: null
};

FileSelection.propTypes = {
	handlerParentFunc: PropTypes.func.isRequired,
	componentMentForImage: PropTypes.bool,
	existingSrc: PropTypes.string
};

//todo add support https://cordova.apache.org/docs/en/10.x/reference/cordova-plugin-file/