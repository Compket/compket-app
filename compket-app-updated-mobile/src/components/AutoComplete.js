// Autocomplete.js
import React, { Component } from 'react';
import styled from 'styled-components';
import { Defenitions, UsedLanguage } from '../base/Defenitions';

const Wrapper = styled.div`
  position: relative;
  z-index: 1600
  align-items: center;
  justify-content: center;
  width: 100%;
  padding: 20px;
  text-align:center;
`;

class AutoComplete extends Component {
    constructor(props) {
        super(props);
        this.clearSearchBox = this.clearSearchBox.bind(this);
    }

    componentDidMount({ map, mapApi } = this.props) {
        const options = {
            // restrict your search to a specific type of result
            types: ['address'],
            // restrict your search to a specific country, or an array of countries
            // componentRestrictions: { country: ['gb', 'us'] },

        };
        this.autoComplete = new mapApi.places.Autocomplete(
            this.searchInput,
            options,
        );
        this.autoComplete.addListener('place_changed', this.onPlaceChanged);
        this.autoComplete.bindTo('bounds', map);
    }

    componentWillUnmount({ mapApi } = this.props) {
        mapApi.event.clearInstanceListeners(this.searchInput);
    }

    onPlaceChanged = ({ map, addplace } = this.props) => {
        const place = this.autoComplete.getPlace();

        if (!place.geometry) return;
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        addplace(place);
        //console.log(place);
        this.searchInput.blur();
    };

    clearSearchBox() {
        this.searchInput.value = '';
    }

    render() {
        return (
            <Wrapper>
                <input
                    id={Defenitions.mapAutoCompleteInputId}
                    className="search-input"
                    //style={{zIndex:999}}
                    ref={(ref) => {
                        this.searchInput = ref;
                    }}
                    style={
                        {
                            border: "2px solid " + Defenitions.palette.primary.main,
                            outline: "none !important",
                            //display:'none'
                        }
                    }
                    type="text"
                    onFocus={this.clearSearchBox}
                    placeholder={UsedLanguage.languageRef.EnterLocationTxt}
                />
            </Wrapper>
        );
    }
}

export default AutoComplete;