import React, {useState, useEffect} from 'react'
import { MarketRoutes } from '../base/Communication';
import { Helper } from '../base/Helper';
import { Defenitions, UsedLanguage} from '../base/Defenitions';
import { TextField } from '@mui/material';
import { Autocomplete } from '@mui/material';
import PropTypes from 'prop-types'; 
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

const CategoryAutoComplete = (props) => {
    const [options, setOptions] = useState([]);
    const [rawCategories, setRaw] = useState([]);
    const [value, setValue] = useState('');
    const [index, setIndex] = useState([]);
    const [inputValue, setInputValue] = useState('');

    useEffect(async () => {
        MarketRoutes.GetCategories(Helper.getAccessTokenFromStorage()).then((res)=>{
            if (res.Success) {
                setRaw(res.Data);
                setOptions(Helper.categoriesToCategoriesNamesArray(res.Data));
            }
        }).catch((err)=>{
            console.log(err);
        });
    }, []);
    return (
        <Autocomplete
            id="nameAuto"
            options={options}
            disableClearable={!props.clearable}
            onChange={(event, newValue) => {
                setValue(newValue);
                setIndex(Helper.selectedCatgoryValueToIdAndValue(rawCategories, newValue));
                if(!props.isAdmin)
                    props.onChange(Helper.selectedCatgoryValueToIdAndValue(rawCategories, newValue));
            }}
            popupIcon={<ArrowDropDownIcon color='primary'/>}
            clearIcon=""
            sx={{ width: props.width}}
            onInputChange={(event, newInputValue) => {
                if(newInputValue === '' || newInputValue === undefined || newInputValue === null)
                    props.onChange(null);
                setInputValue(newInputValue);//the relevent value
                if(props.isAdmin)
                    props.onChange(newInputValue);
            }}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label={UsedLanguage.languageRef.categoryName}
                    variant="standard"
                    focused
                    required
                    InputProps={{
                        ...params.InputProps,
                        type: 'search',
                    }}
                />
            )}
        />
    )
}

export default CategoryAutoComplete

CategoryAutoComplete.propTypes = {
    width: PropTypes.number.isRequired,
    onChange: PropTypes.func,
    isAdmin: PropTypes.bool.isRequired,
    clearable : PropTypes.bool,
}
CategoryAutoComplete.defaultProps = {
    onChange: (() => { }),
    clearable: false,
    
};