import React, { useEffect } from 'react';
import { useState } from 'react';
import SearchIcon from '@mui/icons-material/Search';
import { InputAdornment } from '@mui/material';
import { TextField } from '@mui/material';
import PropTypes from 'prop-types';

const SearchBar = (props) => {
    const [value, setValue] = useState(props.value !== null ? props.value : "")
    return <TextField variant='standard' focused
        placeholder={props.placeholder}
        value={props.value !== null ? props.value : value}
        label={props.label}
        type={props.type}
        onChange={(e) => {
            setValue(e.target.value);
            props.onChange(e.target.value);
        }}
        sx={{ width: props.width }}
        InputProps={{
            endAdornment: (
                <InputAdornment position="start">
                    <SearchIcon color='primary' />
                </InputAdornment>
            ),
        }}
    />;
};

export default SearchBar;
SearchBar.propTypes = {
    width: PropTypes.number.isRequired,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,// if we want value to be controlled, default is uncontrolled
    type: PropTypes.string,
}
SearchBar.defaultProps = {
    onChange: (() => { }),
    placeholder: null,
    label: null,
    value: null,
    type: 'search'

};