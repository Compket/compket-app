import React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import { UsedLanguage } from '../base/Defenitions';
import { Helper } from '../base/Helper';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { makeStyles } from '@mui/styles';
import { Defenitions } from '../base/Defenitions';

const LanguageSelector = () => {
    const useStyles = makeStyles({
        icon: {
            color: Defenitions.palette.primary.main,
        },
    });
    const [lang, setLang] = React.useState(Helper.getLanguageKeyByValue(Helper.getSelectedLanguageFromStorage()));
    //console.log(lang);
    const handleChange = (event) => {
        setLang(event.target.value);
        //Helper.createToast(UsedLanguage.languageRef.changesWillTakeAffectOnReloadTxt);
        Helper.saveSelectedLanguageToStorage(Helper.getLanguageValueByKey(event.target.value));
        let loc = location;
        if(loc !== null && loc !== undefined){
            loc.reload();
        }
    };

    const getOptions = () => {
        let arrRet = [];
        for (const [key, value] of Object.entries(UsedLanguage.languageType)) {
            arrRet.push(
                (<MenuItem key={key} value={key}>
                    {key}
                </MenuItem>)
            );
        }

        return arrRet;
    }
    return (
        <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off"
        >
            <div>
                <TextField
                    id="outlined-select-currency"
                    select
                    SelectProps={{
                        classes: { icon: useStyles().icon },
                    }}
                    focused
                    variant="standard"
                    label={UsedLanguage.languageRef.selectYourLanguageTxt}
                    value={lang}
                    onChange={handleChange}
                >
                    {
                        getOptions()
                    }
                </TextField>
            </div>
        </Box>
    );
};

export default LanguageSelector;
