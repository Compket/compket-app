import React from 'react'
import { Grid } from '@mui/material'
import { AccordionSummary } from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import PhotoIcon from '@mui/icons-material/Photo';
import QrCode2Icon from '@mui/icons-material/QrCode2';
import PropTypes from 'prop-types';
import { Typography } from '@mui/material';
import { Defenitions } from '../base/Defenitions';


const ItemCollapsedAccordionSummary = (props) => {
    let width = props.width;
    let height = props.height;
    return (
        <AccordionSummary
            style={{ width: width, height: height, overflow: 'hidden', p: 0, m: 0 }}
            expandIcon={<ExpandMoreIcon />}
            key='itemAccordion'
        >
            <Grid component='main'
                key="MarketCollapsedViewMainGrid"
                container
                m={0}
                p={0}
                overflow="hidden"
                spacing={0}
                /*width={width}
                */
                height={height}
                justifyContent="center"
                alignContent="center"
                direction={"row"}
                alignItems="center"
            >
                <Grid item xs={2}
                    key="pictureGrid"
                    display="flex"
                    alignItems="center"
                    alignContent="center"
                    justifyContent="center">
                    {props.imageSrc === null || props.imageSrc === undefined ? <PhotoIcon fontSize='large' /> :
                        <img src={props.imageSrc}
                            style={{ maxWidth: '100%'}}
                            height={"45ph"} />}
                </Grid>
                <Grid item xs={3}
                    key="NameGrid"
                    display="flex"
                    overflow={"clip"}
                    alignItems="center"
                    justifyContent="center">
                    <Typography variant="h6" style={
                        {
                            textAlign: 'center',
                            color: Defenitions.palette.primary.main,
                        }
                    }>
                        {props.name}
                    </Typography>
                </Grid>
                <Grid item xs={7}
                    key="dataGrid"
                    display="flex"
                    alignItems="center"
                    justifyContent="center">
                    <Grid
                        container
                        overflow="clip"
                        display="flex"
                        spacing={0}
                        pl={1}
                        justifyContent="start"
                        direction={"column"}
                        alignItems="start"
                    >
                        <Grid item xs={1}>
                            <Typography variant="subtitle2" textAlign={"left"}
                                sx={{ fontWeight: 'bold' }}
                                style={
                                    {
                                        color: Defenitions.palette.primary.main,
                                    }
                                }>
                                {props.price > 0 ? props.price.toFixed(2) + Defenitions.currency[props.currency] : ""}
                            </Typography>
                        </Grid>
                        <Grid item container direction={'row'} xs={1}
                            overflow="clip"
                            display="flex"
                            spacing={0}
                            justifyContent="start"
                            alignItems="center">
                            <Grid item >
                                <Typography variant="subtitle2" dir="ltr" textAlign={"left"} style={
                                    {
                                        color: Defenitions.palette.primary.main,
                                    }
                                }>
                                    {props.barcodeNumber}
                                </Typography>
                            </Grid>
                            <Grid item justifyContent="start" alignItems='start'
                                alignContent='start'>
                                <QrCode2Icon fontSize='medium' />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </AccordionSummary>
    )
}

export default ItemCollapsedAccordionSummary

ItemCollapsedAccordionSummary.propTypes = {
    imageSrc: PropTypes.string,
    name: PropTypes.string.isRequired,
    barcodeNumber: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    width: PropTypes.number,
    height: PropTypes.number,
    currency: PropTypes.number.isRequired,
}
ItemCollapsedAccordionSummary.defaultProps = {
    imageSrc: null,
    width: 200,
    height: 100
};
