import React, { useState, useEffect } from 'react'
import { UserRoutes } from '../base/Communication';
import { Helper } from '../base/Helper';
import { UsedLanguage } from '../base/Defenitions';
import { TextField } from '@mui/material';
import { Autocomplete } from '@mui/material';
import PropTypes from 'prop-types';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

const LocationsAutoComplete = (props) => {
    const [optionsCountry, setOptionsCountry] = useState([]);
    const [optionsCity, setOptionsCity] = useState([]);
    const [rawLocations, setRaw] = useState([]);
    const [selectedCountry, setCountry] = useState('');
    const [selectedCity, setCity] = useState('');
    const [open, setOpen] = useState(false);

    useEffect(async () => {
        UserRoutes.GetAllLocations(Helper.getAccessTokenFromStorage()).then((res) => {
            if (res.Success) {
                setRaw(res.Data);
                setOptionsCountry(Helper.locationsToLocationsCountryNamesArray(res.Data));
            }
        }).catch((err) => {
            console.log(err);
        });
    }, []);
    useEffect(() => {
        props.onChange([selectedCountry, null]);
        setOptionsCity(Helper.locationsToLocationsCitiesNamesArray(rawLocations, selectedCountry));
     }, [selectedCountry]);
    useEffect(() => {
        props.onChange([selectedCountry, selectedCity]);
     }, [selectedCity]);
    return (
        <div style={{ display: "table", borderSpacing: 10 }}>
            <div style={{ display: "table-cell" }}>
                <Autocomplete
                    id="countryAuto"
                    options={optionsCountry}
                    disableClearable={!props.clearable}
                    onChange={(event, newValue) => {
                        
                    }}
                    popupIcon={<ArrowDropDownIcon color="primary"/>}
                    clearIcon=""
                    sx={{ width: (props.width / 2  - 5)}}
                    onInputChange={(event, newInputValue) => {
                        if (newInputValue === '' || newInputValue === undefined || newInputValue === null)
                            props.onChange(null);
                        setCity('');
                        setCountry(newInputValue);
                        if(newInputValue !== '')
                            setOpen(true);
                    }}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            label={UsedLanguage.languageRef.countryTxt}
                            variant="standard"
                            focused
                            InputProps={{
                                ...params.InputProps,
                                type: 'search',
                            }}
                        />
                    )}
                />
            </div>
            <div style={{ display: "table-cell"}}>
                <Autocomplete
                    id="cityAuto"
                    value={selectedCity}
                    open={open}
                    popupIcon={<ArrowDropDownIcon color="primary"/>}
                    onOpen={()=>{
                        setOpen(true);
                    }}
                    onClose={()=>{
                        setOpen(false);
                    }}
                    options={optionsCity}
                    disableClearable={!props.clearable}
                    onChange={(event, newValue) => {
                        
                    }}
                    clearIcon=""
                    sx={{ width: (props.width / 2  - 5)}}
                    onInputChange={(event, newInputValue) => {
                        if (newInputValue === '' || newInputValue === undefined || newInputValue === null)
                            props.onChange(null);
                        setCity(newInputValue);
                    }}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            label={UsedLanguage.languageRef.cityTxt}
                            variant="standard"
                            focused
                            InputProps={{
                                ...params.InputProps,
                                type: 'search',
                            }}
                        />
                    )}
                />
            </div>
        </div>
    )
}

export default LocationsAutoComplete

LocationsAutoComplete.propTypes = {
    width: PropTypes.number.isRequired,
    onChange: PropTypes.func,
    //isAdmin: PropTypes.bool.isRequired,
    clearable: PropTypes.bool,
}
LocationsAutoComplete.defaultProps = {
    onChange: (() => { }),
    clearable: false,

};