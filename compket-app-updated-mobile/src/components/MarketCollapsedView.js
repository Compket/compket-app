import { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Defenitions, UsedLanguage } from '../base/Defenitions'
import { Grid } from '@mui/material'
import PhotoIcon from '@mui/icons-material/Photo';
import { Typography } from '@mui/material';
import { Paper } from '@mui/material';
import { Button } from '@mui/material'
import { MarketRoutes } from '../base/Communication';
import { Helper } from '../base/Helper';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { IconButton } from '@mui/material';
import { Link } from 'react-router-dom';
import YesNoDialog from './YesNoDialog';
import PopupModal from './PopupModal';
import MarketCreation from '../pages/market/MarketCreation';

const MarketCollapsedView = props => {
    const [src, setSrc] = useState(null);
    const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
    const [openEdit, setOpenEdit] = useState(false);

    const width = props.width === null ? Defenitions.width * 0.9 : props.width;
    const height = Defenitions.height * 0.1;

    useEffect(async () => {
        await MarketRoutes.GetMarketImage(Helper.getAccessTokenFromStorage(), props.mid).then((res) => {
            if (res.Success)
                setSrc(URL.createObjectURL(res.Data.data));
        }).catch((err) => console.log("image not found", err));
    }, []);
    return (
        <Paper elevation={4} style={{ width: width, height: height, alignContent: "center", justifyContent: "center", justifyItems: "center" }}>
            <div style={{ width: width, height: height, display: 'inline-flex' }}>
                <Button color='inherit' style={{ width: props.marketOwnerMode ? width * 0.7 : width, height: height }}>
                    <Link to={props.linkTo}
                        key={"market" + props.mid} style={{ textDecoration: 'none', color: "inherit" }}
                        onClick={props.onClick}>
                        <Grid component='main'
                            key="MarketCollapsedViewMainGrid"
                            container
                            m={0}
                            ml={1}
                            p={0}
                            overflow="hidden"
                            spacing={0}
                            width={props.marketOwnerMode ? width * 0.7 : width}
                            height={height}
                            justifyContent="center"
                            alignContent="center"
                            direction={"row"}
                            alignItems="center"
                        >
                            <Grid item xs={2}
                                key="pictureGrid"
                                display="flex"
                                alignItems="center"
                                alignContent="center"
                                justifyContent="center">
                                {src === null || src === undefined ? <PhotoIcon fontSize='large' /> : <img src={src} width={"45pw"}
                                    height={"45ph"} />}
                            </Grid>
                            <Grid item xs={4}
                                key="NameGrid"
                                display="flex"
                                overflow={"hidden"}
                                alignItems="center"
                                justifyContent="center">
                                <Typography variant="h6" style={
                                    {
                                        color: Defenitions.palette.primary.main,
                                        textAlign: 'center',
                                        textTransform: 'none'
                                    }
                                }>
                                    {props.name}
                                </Typography>
                            </Grid>
                            <Grid item xs={6}
                                key="dataGrid"
                                display="flex"
                                alignItems="center"
                                justifyContent="center">
                                <Grid
                                    container
                                    overflow="clip"
                                    display="flex"
                                    spacing={0}
                                    justifyContent="start"
                                    direction={"column"}
                                    alignItems="start"
                                >
                                    <Grid item xs={1}>
                                        <Typography variant="body2" textAlign={"left"} style={
                                            {
                                                color: Defenitions.palette.primary.main,
                                                textTransform: 'none'
                                            }
                                        }>
                                            {props.addressCountry + " " + props.addressCity + " "
                                                + props.addressStreet}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={1}>
                                        <Typography variant="body2" dir="ltr" textAlign={"left"} style={
                                            {
                                                color: Defenitions.palette.primary.main,
                                            }
                                        }>
                                            {props.phoneNumber}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Link>
                </Button>
                {props.marketOwnerMode ?
                    <div style={{ width: width * 0.3, display: 'inline-flex', justifyContent: 'center', alignContent: 'center' }}>
                        <IconButton style={{width:'auto'}} onClick={() => { setOpenEdit(true) }}>
                            <EditIcon fontSize='medium' style={{ width:'auto' }} />
                        </IconButton>
                        <IconButton style={{width:'auto'}}/*style={{ overflow: 'auto', display: 'table' }}*/ onClick={() => { setDeleteDialogOpen(true) }}>
                            <DeleteForeverIcon fontSize='medium' style={{ width:'auto' }}/*style={{ display: 'table-row' }}*/ />
                        </IconButton>
                    </div>
                    :
                    <></>
                }
                <YesNoDialog key={'dialog' + props.mid}
                    title={UsedLanguage.languageRef.deleteMarketTxt}
                    body={UsedLanguage.languageRef.deleteMarketBodyTxt}
                    confirmByTxt={true}
                    open={deleteDialogOpen}
                    onResult={async (data) => {
                        setDeleteDialogOpen(false);
                        if (data === true) {
                            await Helper.createToastPromise(MarketRoutes.DeleteMarket(Helper.getAccessTokenFromStorage(),
                                props.mid)).then(
                                    (res) => {
                                        props.onDeleteOrEdit();//to tell the parent we successfuly deleted.
                                    },
                                    (err) => { console.log(err) });
                        }
                    }} />
                <PopupModal
                    open={openEdit}
                    onClose={() => { props.onDeleteOrEdit() }}
                    bodyComponent={<MarketCreation
                        marketEditMode={true}
                        addressCity={props.addressCity}
                        addressCountry={props.addressCountry}
                        addressStreet={props.addressStreet}
                        name={props.name}
                        mid={props.mid}
                        description={props.description}
                        onDeleteOrEdit={props.onDeleteOrEdit}
                        phoneNumber={props.phoneNumber}
                        imgSrc={src}
                    />}
                />
            </div>
        </Paper>
    )
}

MarketCollapsedView.propTypes = {
    mid: PropTypes.any,
    name: PropTypes.string.isRequired,
    addressCountry: PropTypes.string.isRequired,
    addressCity: PropTypes.string.isRequired,
    addressStreet: PropTypes.string,
    phoneNumber: PropTypes.string.isRequired,
    linkTo: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    description: PropTypes.string,
    marketOwnerMode: PropTypes.bool,
    onDeleteOrEdit: PropTypes.func,
    width: PropTypes.number,
}
MarketCollapsedView.defaultProps = {
    mid: null,
    name: null,
    description: null,
    addressCountry: null,
    addressCity: null,
    addressStreet: null,
    phoneNumber: null,
    onClick: (() => { }),
    onDeleteOrEdit: (() => { }),
    marketOwnerMode: false,
    width: null
};
export default MarketCollapsedView
