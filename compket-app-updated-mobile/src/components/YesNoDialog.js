import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types'
import { useState } from 'react';
import { UsedLanguage } from '../base/Defenitions';
import { TextField } from '@mui/material';

const YesNoDialog = (props) => {
    const [confirmVal, setConfrimVal] = useState('');
    const resetField = ()=>{
        setConfrimVal('');//just resetting the textfield.
    }
    return (
        <Dialog
            open={props.open}
            onClose={() => { resetField();props.onResult(false); }}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title" style={{ display: 'flex', justifyContent: 'center' }}>
                {props.title}
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description" style={{ textAlign: 'center', display: 'flex', justifyContent: 'center' }}>
                    {props.body}
                    <br />
                    {props.confirmByTxt ?
                        UsedLanguage.languageRef.confirmByTypingTxt + UsedLanguage.languageRef.confirmTxt
                        : ''
                    }
                </DialogContentText>
                {props.confirmByTxt ?
                    <TextField style={{ display: 'flex', justifyContent: 'center' }}
                    key='confirxField' variant='standard' value={confirmVal} onChange={(e)=>{setConfrimVal(e.target.value)}} />
                    :
                    <></>
                }
            </DialogContent>
            <DialogActions style={{ display: 'flex', justifyContent: 'center' }}>
                <Button onClick={() => { resetField();props.onResult(false); }}>
                    {UsedLanguage.languageRef.disagreeTxt}
                </Button>
                <Button onClick={() => {
                    if(props.confirmByTxt && confirmVal !== UsedLanguage.languageRef.confirmTxt)
                    {
                        return;
                    }
                    resetField();
                    props.onResult(true);
                }}
                >{UsedLanguage.languageRef.agreeTxt}</Button>
            </DialogActions>
        </Dialog>
    )
}

export default YesNoDialog

YesNoDialog.propTypes = {
    title: PropTypes.string,
    body: PropTypes.string,
    open: PropTypes.bool.isRequired,
    onResult: PropTypes.func.isRequired,
    confirmByTxt: PropTypes.bool
}
YesNoDialog.defaultProps = {
    title: "<title>",
    body: "<body>",
    confirmByTxt: false,
};