# Compket-App

React Application Repositroy

## Getting started
Compket (Compare Market) is a Proof Of Concept for a software that allows market owners and customers 
to view products and compare their prices.
Features a shopping cart of generic market items that can be compared between multiple 
different markets. 
Developed using React.js, MySQL Server, RESTful .NET Server, GIT workflow.

To see a working example you may watch the provided video demos.
